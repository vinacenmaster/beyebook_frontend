import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/services';
import { CrmContractService, CrmContractDetailService } from '@app/core/services/crm/contract.service';
import { CrmSalesOpportunityService, CrmActivityBusinessService } from '@app/core/services/crm/sale-opportunity.service';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { CrmStatisticBoadService } from '@app/core/services/crm/dashboard.service';
import { CrmTnsStatisticBoardModel } from '@app/core/models/crm/statisticsboard.model';

@Component({
  selector: 'sa-statistic-board',
  templateUrl: './statistic-board.component.html',
  styleUrls: ['./statistic-board.component.css']
})
export class StatisticBoardComponent extends BasePage implements OnInit {

  userLogin: any;
  year: number = 0;
  statisticBoard : any = {};
  constructor(
    private router: Router,
    public userService: AuthService,
    private crmStatisticBoadService: CrmStatisticBoadService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.year = new Date().getFullYear();
    this.crmStatisticBoadService.getStatisticBoad(this.userLogin.company_id,this.year).then(data =>{
      this.statisticBoard = data;
      // console.log(this.statisticBoard)
    })
  }
}
