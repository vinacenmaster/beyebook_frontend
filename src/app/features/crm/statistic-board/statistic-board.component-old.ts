// import { Component, OnInit } from '@angular/core';
// import { BasePage } from '@app/core/common/base-page';
// import { Router } from '@angular/router';
// import { AuthService } from '@app/core/services';
// import { CrmContractService, CrmContractDetailService } from '@app/core/services/crm/contract.service';
// import { CrmSalesOpportunityService, CrmActivityBusinessService } from '@app/core/services/crm/sale-opportunity.service';
// import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
// import { TraderService } from '@app/core/services/features.services/trader-master.service';
// import { ContactorService } from '@app/core/services/features.services/contactor-master.service';

// @Component({
//   selector: 'sa-statistic-board',
//   templateUrl: './statistic-board.component.html',
//   styleUrls: ['./statistic-board.component.css']
// })
// export class StatisticBoardComponent extends BasePage implements OnInit {

//   userLogin: any;
//   year: number = 0;
//   profitByAM_Total: any[] = [];
//   salesOpt_Total: number = 0;
//   expectedSales_Total: number = 0;
//   fixedFinancial_Total: number = 0;
//   salesByCustomer_Total: number = 0;
//   costByCustomer_Total: number = 0;
//   salesByContactor_Total: number = 0;
//   constructor(
//     private router: Router,
//     public userService: AuthService,
//     private crmContractService: CrmContractService,
//     private crmSalesOpportunityService: CrmSalesOpportunityService,
//     private crmActivityBusinessService: CrmActivityBusinessService,
//     private crmContractDetailService: CrmContractDetailService,
//     private salesOrderService: SalesOrderService,
//     private traderService: TraderService,
//     private contactorService: ContactorService,
//   ) {
//     super(userService);
//   }

//   ngOnInit() {
//     this.userLogin = this.userService.getUserInfo();
//     this.year = new Date().getFullYear();
//     this.getlistExpected()
//     this.getlistFixedFinancial()
//     Promise.all([this.getlistContract(), this.getListOrder(), this.getCustomer(),this.getContactor(), this.getlistSalesOpt()])
//     .then(([contracts, orders, customers,contactors,salesOpts]) => {
//       this.profitByAM_Total = contracts
//       this.salesOpt_Total = salesOpts.filter(x => x.sale_activitys).length
//       this.getlistSalesByCustomer(contracts, orders, customers)
//       this.getlistCostByCustomer(contracts, orders, customers)
//       this.getlistSalesByContactor(contracts, orders,contactors,salesOpts)
//     })
//   }

//   getlistContract() {
//     return this.crmContractService.ListFromAMStatistics(this.userLogin.company_id, this.year, 0, '')
//   }
//   getListOrder() {
//     return this.salesOrderService.ShortList(this.userLogin.company_id)
//   }
//   getCustomer() {
//     return this.traderService.ShortList(this.userLogin.company_id)
//   }
//   getContactor() {
//     return this.contactorService.ShortList(this.userLogin.company_id)
//   }
//   getlistSalesOpt() {
//     return this.crmSalesOpportunityService.ListFromSalesOptStatistics(this.userLogin.company_id,this.year)
//   }
//   getlistExpected() {
//     this.crmActivityBusinessService.ShortList(this.userLogin.company_id, this.year)
//       .then(data => { return this.expectedSales_Total = data.activity.length })
//   }
//   groupFunction(data, type) {
//     var groups = data.reduce(function (r, o) {
//       var m: any;
//       switch (type) {
//         case 'contract':
//           m = o.contract_id
//           break;
//         case 'customer':
//           m = o.customer_id
//           break;
//         case 'contactor':
//           m = o.contactor_id
//           break;
//         default:
//           break;
//       }
//       if (!r[m])
//         r[m] = { group: m };
//       return r;
//     }, {});
//     return Object.keys(groups).map(function (k) { return groups[k]; });
//   }
//   getlistFixedFinancial() {
//     this.crmContractDetailService.ShortList(this.userLogin.company_id, 0, 0, this.year)
//       .then(data => {
//         this.fixedFinancial_Total = this.groupFunction(data, 'contract').length
//       })
//   }
//   getlistSalesByCustomer(contracts, orders, customers) {
//     let _arr = contracts.filter(x => x.src_order_type == true);
//     for (let i = 0; i < _arr.length; i++) {
//       for (let j = 0; j < orders.length; j++) {
//         if (_arr[i].src_order_id == orders[j].order_id) {
//           for (let k = 0; k < customers.length; k++) {
//             if (orders[j].customer_contactor_id == customers[k].trader_id) {
//               _arr[i].customer_id = customers[k].trader_id;
//             }
//           }
//         }
//       }
//     }
//     this.salesByCustomer_Total = this.groupFunction(_arr, 'customer').length
//   }
//   getlistCostByCustomer(contracts, orders, customers) {
//     let _arr = contracts.filter(x => x.src_order_type == false);
//     for (let i = 0; i < _arr.length; i++) {
//       for (let j = 0; j < orders.length; j++) {
//         if (_arr[i].src_order_id == orders[j].order_id) {
//           for (let k = 0; k < customers.length; k++) {
//             if (orders[j].customer_contactor_id == customers[k].trader_id) {
//               _arr[i].customer_id = customers[k].trader_id;
//             }
//           }
//         }
//       }
//     }
//     this.costByCustomer_Total = this.groupFunction(_arr, 'customer').length
//   }
//   getlistSalesByContactor(contracts, orders,contactors,salesOpts) {
//     let _arr = contracts.filter(x => x.src_order_type == true);
//     for (let i = 0; i < _arr.length; i++) {
//       for (let j = 0; j < orders.length; j++) {
//         if (_arr[i].src_order_id == orders[j].order_id) {
//           for (let k = 0; k < salesOpts.length; k++) {
//             if (orders[j].salesopt_id == salesOpts[k].salesopt_id) {
//               for (let m = 0; m < contactors.length; m++) {
//                 if (salesOpts[k].customer_contactor_id == contactors[m].contactor_id) {
//                   _arr[i].contactor_id = contactors[m].contactor_id;
//                 }
//               }
//             }
//           }
//         }
//       }
//     }
//     this.salesByContactor_Total = this.groupFunction(_arr, 'contactor').length
//   }
// }
