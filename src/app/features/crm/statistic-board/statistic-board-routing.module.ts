import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatisticBoardComponent } from './statistic-board.component';

const routes: Routes = [{
  path: '',
  component: StatisticBoardComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticBoardRoutingModule { }
