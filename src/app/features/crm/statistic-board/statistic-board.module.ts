import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatisticBoardRoutingModule } from './statistic-board-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { StatisticBoardComponent } from './statistic-board.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    StatisticBoardRoutingModule
  ],
  declarations: [StatisticBoardComponent]
})
export class StatisticBoardModule { }
