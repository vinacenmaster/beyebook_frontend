import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CrmContractModel } from '@app/core/models/crm/contract.model';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService } from '@app/core/services';
import { ProgramList } from '@app/core/common/static.enum';
import { CrmContractService } from '@app/core/services/crm/contract.service';
import { BsModalService } from 'ngx-bootstrap';
import { Router } from '@angular/router';
import { GetOrderModel } from '@app/core/models/crm/get-order.model';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { CostOrderModel } from '@app/core/models/crm/cost-order.model';

@Component({
  selector: 'sa-contract-popup',
  templateUrl: './contract-popup.component.html',
  styleUrls: ['../contract-master/contract-master.component.css']
})
export class ContractPopupComponent extends BasePage implements OnInit {
  @Output() childCall = new EventEmitter();
  @Input() Id :number;
  @Input() JSContractInfo :string;
  @Input() orderInfo : any = {};

  contractInfo:CrmContractModel;
  userLogin : any;
  customer: any[] = [];
  validationOptions:any={}
  constructor(
    private notification: NotificationService,
    private crmContractService: CrmContractService,
    public userService: AuthService,
    private modalService: BsModalService,
    private router: Router,
    private traderService: TraderService,

  ) {
    super(userService);
  }

  ngOnInit() {
    this.checkPermission(ProgramList.Contract_Magt.valueOf());
    this.userLogin = this.userService.getUserInfo();
    this.contractInfo = this.crmContractService.resetModel();
    this.contractInfo = this.crmContractService.getModel();
    this.contractInfo.company_id = this.userLogin.company_id;
    $('.start-ymd-datepicker').datepicker({
      dateFormat: 'yy-mm-dd',
      onSelect: (selectedDate) => {
        this.contractInfo.contract_start_ymd = selectedDate;
      }
    });
    $('.end-ymd-datepicker').datepicker({
      dateFormat: 'yy-mm-dd',
      onSelect: (selectedDate) => {
        this.contractInfo.contract_end_ymd = selectedDate;
      }
    });
    $('.order-ymd-datepicker').datepicker({
      dateFormat: 'yy-mm-dd',
      onSelect: (selectedDate) => {
        this.contractInfo.order_date = selectedDate;
      }
    });
    
    if (this.Id == 0) {
      this.contractInfo.contract_start_ymd = new Date().toISOString().split('T')[0];
      this.contractInfo.contract_end_ymd = new Date().toISOString().split('T')[0];
      if(this.orderInfo.order_type){// true = order | false = cost order
        this.contractInfo.src_order_id = this.orderInfo.order_id
        this.contractInfo.order_nm =  this.orderInfo.order_nm
        this.contractInfo.order_date = this.orderInfo.order_ymd
        this.contractInfo.order_amt = this.orderInfo.total_atm
      }else{
        this.contractInfo.src_order_id = this.orderInfo.cost_order_id
        this.contractInfo.order_nm =  this.orderInfo.cost_order_nm
        this.contractInfo.order_date = this.orderInfo.cost_order_ymd
        this.contractInfo.order_amt = this.orderInfo.cost_order_atm
      }
        this.contractInfo.src_order_type =this.orderInfo.order_type ;
        this.contractInfo.order_account = this.orderInfo.customer_contactor_nm,
        this.contractInfo.contract_nm = this.contractInfo.order_nm;
        this.contractInfo.contract_amt = this.contractInfo.order_amt;
    }else{
      if (this.JSContractInfo) {
        this.contractInfo = JSON.parse(this.JSContractInfo)
      }
    }
  }
  sharingToSelected(data){
    this.contractInfo.sharing_to=data;
  }
  ClosePopup(){
    this.modalService.hide(1);
  }
  onReset(){

  }
  onSubmit(){
    this.crmContractService.InsertContract(this.contractInfo).then(data => {
      if (data.error) {
        if (data.error.code === 403) {
          this.modalService.hide(1);
          this.router.navigate(["/error/error403"]);
        }
        this.childCall.emit(false);
        this.notification.showMessage("error", data.error.message);
      } else {
        this.notification.showMessage("success", data.message);
        this.modalService.hide(1);
        this.childCall.emit(true);
      }
    });
  }

  onDelete(){
    this.notification.confirmDialog(
      "Deleting this item ?",
      `Deleting an item will move it to the <span class='warning-emphasize'>trash</span>.<br />
      Deleted items <span class='warning-emphasize'>can</span> be <span class='warning-emphasize'>recovered from the Recycle Bin within 30 days</span>.<br />
      Do you want to continue?`,
      x => {
        if (x) {
          this.crmContractService.DeleteContract(this.contractInfo).then(data => {
            if (data.error) {
              if (data.error.code === 403) {
                this.modalService.hide(1);
                this.router.navigate(["/error/error403"]);
              }
              this.notification.showMessage("error", data.error.message);
            } else {
              this.notification.showMessage("success", data.message);
              this.modalService.hide(1);
              this.router.navigate(['/crm-contract']);
            }
          })
        }
      }
    );
  }
}
