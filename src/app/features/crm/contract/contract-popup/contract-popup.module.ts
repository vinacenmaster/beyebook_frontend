import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { ContractPopupComponent } from './contract-popup.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    NgxCurrencyModule,
  ],
  exports:[
    ContractPopupComponent
  ],
  declarations: [ContractPopupComponent]
})
export class ContractPopupModule { }
