import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractMasterRoutingModule } from './contract-master-routing.module';
import { ContractMasterComponent } from './contract-master.component';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { CommonSharedModule } from '@app/shared/common/common-shared.module';
import { ContractPopupModule } from '../contract-popup/contract-popup.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SmartadminDatatableModule,
    ContractMasterRoutingModule,
    CommonSharedModule,
    ContractPopupModule,
  ],
  declarations: [ContractMasterComponent]
})
export class ContractMasterModule { }
