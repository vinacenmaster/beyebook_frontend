import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContractMasterComponent } from './contract-master.component';

const routes: Routes = [{
  path: '',
  component: ContractMasterComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContractMasterRoutingModule { }
