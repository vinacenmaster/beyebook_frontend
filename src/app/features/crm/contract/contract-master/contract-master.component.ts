import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AuthService, UserMasterService, NotificationService, ProgramService } from '@app/core/services';
import { BasePage } from '@app/core/common/base-page';
import { CrmContractService } from '@app/core/services/crm/contract.service';
import { ProgramList } from '@app/core/common/static.enum';
import { CommonFunction } from '@app/core/common/common-function';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { CrmContractModel } from '@app/core/models/crm/contract.model';
import { CostOrderService } from '@app/core/services/crm/cost-order.service';
import { config } from '@app/core/smartadmin.config';
import { ProgramModel } from '@app/core/models/program.model';

@Component({
  selector: 'sa-contract-master',
  templateUrl: './contract-master.component.html',
  styleUrls: ['./contract-master.component.css']
})
export class ContractMasterComponent extends BasePage implements OnInit {
  @ViewChild("popupContract") popupContract;
  contractInfo:any;
  userLogin : any;
  modalRef: BsModalRef;
  Id :number = 0;
  options: any;
  listOrder : any[] = [];
  listCostOrder : any[] = [];
  customer: any[] = [];
  contractList:CrmContractModel[] = [];
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    private modalService: BsModalService,
    private notification: NotificationService,
    private crmContractService: CrmContractService,
    public router: Router,
    public salesOrderService: SalesOrderService,
    public costOrderService: CostOrderService,
    private traderService: TraderService,
    private programService: ProgramService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.checkPermission(ProgramList.Contract_Magt.valueOf());
    this.userLogin = this.userService.getUserInfo();
    this.initDatatable();
    
  }
  getListOrder(){
    return this.salesOrderService.getList(this.userLogin.company_id)
  }
  getListCostOrder(){
    return this.costOrderService.getList(this.userLogin.company_id)
  }
  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id)
  }
  initDatatable() {
    this.options = {
      dom: "Bfrtip",
      ajax: (data, callback, settings) => {
        this.crmContractService.ListContract(this.userLogin.company_id).then(data => {
          Promise.all([this.getListOrder(),this.getListCostOrder(), this.getCustomer()]).then(([ListOrder,ListCostOrder,ListCustomer]) => {
            this.bindEventPoppup();
            this.listOrder = ListOrder;
            this.listCostOrder = ListCostOrder;
            this.customer = ListCustomer;
            callback({
              aaData: data
            });
          });
        })
      },
      columns: [
        { data: "contract_start_ymd", className: "", width: "70px" },
        { data: "contract_end_ymd", className: "", width: "70px" },
        { data: (data) =>{
          if (data.src_order_type) {
            return `<a class="goto" data-contractId = "${data.contract_id}"><span class='label label-info'>Order</span> ${data.contract_nm}</a>`
          }else{
            return `<a class="goto" data-contractId = "${data.contract_id}"><span class='label label-danger'>Order</span> ${data.contract_nm}</a>`
          }
          
        }, className: "", width: "" },
        { data: (data) =>{
          var result = '';
          if (data.src_order_type) 
          {
            this.listOrder.forEach(order => {
              if (order.order_id == data.src_order_id) {
                this.customer.forEach(cus => {
                  if (cus.trader_id == order.customer_contactor_id) {
                    result = cus.trader_local_nm
                  }
                });
              }
            });
            return result;
          }else{
            this.listCostOrder.forEach(cost => {
              if (cost.cost_order_id == data.src_order_id) {
                this.customer.forEach(cus => {
                  if (cus.trader_id == cost.customer_contactor_id) {
                    result = cus.trader_local_nm
                  }
                });
              }
            });
            return result;
          };
        }, className: "", width: "200px" },
        { data: (data) =>{
          if (data.src_order_type) return `${CommonFunction.FormatMoney(data.contract_amt)}`;
          else return '';
        }, className: "right", width: "80px" },
        { data: (data) =>{
          if (!data.src_order_type && data.contract_amt!=0) return `-${CommonFunction.FormatMoney(data.contract_amt)}`;
          else return '';
        }, className: "right", width: "80px" },
        { data: (data) =>{
          if (data.contract_details.length > 0) {
            let sum = 0;
            data.contract_details.forEach(item => {
              if (item.amt_actual_ymd !="") {
                sum += item.issue_amt
              }
            });
            return `${CommonFunction.FormatMoney(sum)}`;
          }else return '0';
        }, className: "right", width: "80px" },
        { data: (data) =>{
          if (data.contract_details.length > 0) {
            let count = 0;
            data.contract_details.forEach(item => {
              if (item.amt_actual_ymd !="") {
                count++;
              }
            });
            return `${count}/${data.contract_details.length}`;
          }else return '0/0';
        }, className: "center", width: "50px" },
        { data: (data) =>{
          if (data.contract_details.length > 0) {
            let result = '';
            data.contract_details.forEach(item => {
              if (item.amt_actual_ymd !="") {
                result= item.issue_ymd;
              }
            });
            return result;
          }else return '';
        }, className: "", width: "80px" },
        { data: (data) =>{
          if (data.contract_details.length > 0) {
            return data.contract_details[0].created_time;
          }else return '';
        }, className: "", width: "80px" },
      ],
      scrollY: 310,
      scrollX: true,
      paging: true,
      pageLength: 25,
      buttons: [
        {
          text: '<i class="fa fa-refresh" title="Refresh"></i>',
          action: (e, dt, node, config) => {
            dt.ajax.reload();
          }
        },
        "copy",
        "excel",
        "pdf",
        "print"
      ]
    };
  }
  private reloadDatatable() {
    $(".contractTbl")
      .DataTable()
      .ajax.reload();
  }
  public search() {
    let name = $('#txtKeyword').val().toLowerCase();
    var tbl = $('.contractTbl').DataTable(); 
    tbl.search(name).draw() ;
  }
  checkKeycode(event){
    if (event.keyCode === 13) {
      this.search();
    }
  }
  bindEventPoppup() {
    let vm = this;
    setTimeout(() => {
      $('.goto').click(function () {
        let id = $(this).attr('data-contractId');
        var menu_url:string;
        var menu_id:number;
        var menu_name:string;
        menu_url = "/crm-contract-detail/" + id;
        menu_id = 9006;
        menu_name = "Contract detail";
        
        if (!vm.programService.openedPrograms.filter(x => x.id == menu_id).length && vm.programService.openedPrograms.length == config.maxOpenedPrograms) {
          vm.notification.smartMessageBox({
            title: "Notification",
            content: `Maximum ${config.maxOpenedPrograms} programs can be opened!`,
            buttons: '[OK]'
          });
          return;
        }
        $('.center-loading').show();
        vm.programService.addOpenedPrograms(new ProgramModel(menu_id, menu_name, menu_url),true);

      });
    }, 500);
  }
}
