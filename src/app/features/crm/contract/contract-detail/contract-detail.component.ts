import { Component, OnInit, ViewChild } from '@angular/core';
import { CrmContractDetailModel, CrmContractModel } from '@app/core/models/crm/contract.model';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService, UserMasterService } from '@app/core/services';
import { CrmContractService, CrmContractDetailService } from '@app/core/services/crm/contract.service';
import { ProgramList } from '@app/core/common/static.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { SalesOrderService, SalesOrderDetailService } from '@app/core/services/crm/sales-order.service';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { CrmMasServiceItemService } from '@app/core/services/crm/setting-item.service';
import { CostOrderService, CostOrderDetailService } from '@app/core/services/crm/cost-order.service';

@Component({
  selector: 'sa-contract-detail',
  templateUrl: './contract-detail.component.html',
  styleUrls: ['./contract-detail.component.css']
})
export class ContractDetailComponent extends BasePage implements OnInit {
  @ViewChild("popupContract") popupContract;
  contractInfo: CrmContractModel;
  userLogin: any;
  inputNumberClone: number = 1;
  priceBulk: string = '';
  arrays: any = [];
  contractDetail_BK: CrmContractModel;
  modalRef: BsModalRef;
  customer: any[] = [];
  user: any[] = [];
  history: any[] = [];
  listServiceItem: any[] = [];
  constructor(
    private notification: NotificationService,
    private crmContractService: CrmContractService,
    private crmContractDetailService: CrmContractDetailService,
    public userService: AuthService,
    private route: ActivatedRoute,
    public router: Router,
    private modalService: BsModalService,
    private traderService: TraderService,
    public salesOrderService: SalesOrderService,
    public costOrderService: CostOrderService,
    public salesOrderDetailService: SalesOrderDetailService,
    public costOrderDetailService: CostOrderDetailService,
    public crmMasServiceItemService: CrmMasServiceItemService,
    private crmSalesOpportunityService: CrmSalesOpportunityService,
    public userMasterService: UserMasterService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.checkPermission(ProgramList.Contract_Magt.valueOf());
    this.userLogin = this.userService.getUserInfo();
    this.contractInfo = this.crmContractService.resetModel();
    this.contractInfo = this.crmContractService.getModel();
    this.contractInfo.company_id = this.userLogin.company_id;
    this.contractInfo.contract_id = parseInt(this.route.snapshot.paramMap.get("id"));
    Promise.all([this.getCustomer(), this.getSytemUser(), this.getListService(), this.getContractDetail(this.contractInfo.contract_id)])
      .then(([customers, users, services, contractDetails]) => {
        this.customer = customers;
        this.user = users;
        this.listServiceItem = services;
        this.contractInfo = contractDetails;
        this.getOrderById(this.userLogin.company_id, this.contractInfo.src_order_type, this.contractInfo.src_order_id).then(data => {
          this.contractInfo.order_date = this.contractInfo.src_order_type ? data.order_ymd : data.cost_order_ymd
          this.contractInfo.order_amt = this.contractInfo.src_order_type ? data.total_atm : data.cost_order_atm
          let cus = this.customer.find(x => x.trader_id == data.customer_contactor_id)
          if (cus) this.contractInfo.order_account = cus.trader_local_nm;

          this.getSaleOptById(data.salesopt_id).then(data => {
            this.contractInfo.order_nm = data.salesopt_nm;
            let us = this.user.find(x => x.user_id == data.admin_id)
            if (us) this.contractInfo.sales_admin = us.user_nm;
            let cus = this.customer.find(x => x.trader_id == data.customer_id)
            if (cus) this.contractInfo.sales_customer = cus.trader_local_nm;
          })
        });
        this.getListOrderDetail(this.userLogin.company_id, this.contractInfo.src_order_type, this.contractInfo.src_order_id).then(data => {
          data.forEach(dt => {
            this.listServiceItem.forEach(se => {
              if (dt.crm_item_id == se.crm_item_id)
                dt.crm_item_nm = se.crm_item_nm;
            });
          });
          this.history = data;
        })
        this.contractInfo.contract_details.map((ctDetail, index) => {
          this.issueConfirm(ctDetail.issue_yn, index);
          if (ctDetail.amt_actual_ymd)
            ctDetail.checkDepositDate = true;
        });
        this.contractDetail_BK = JSON.parse(JSON.stringify(this.contractInfo));
      })
  }
  async getOrderById(companyId, orderType, orderId) {
    return await orderType ? this.salesOrderService.getDetail(companyId, orderId) : this.costOrderService.getDetail(companyId, orderId)
  }
  async getListOrderDetail(companyId, orderType, orderId) {
    return await orderType ? this.salesOrderDetailService.getListByOrderId(companyId, orderId) : this.costOrderDetailService.getListByOrderId(companyId, orderId)
  }
  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id)
  }
  getSytemUser() {
    return this.userMasterService.listUsers()
  }
  getListService() {
    return this.crmMasServiceItemService.getList(this.userLogin.company_id, null)
  }
  getSaleOptById(salesId) {
    return this.crmSalesOpportunityService.getDetail(salesId)
  }
  getContractDetail(contractId) {
    return this.crmContractService.getDetail(this.userLogin.company_id, contractId, true)
  }
  addClone() {
    for (let i = 0; i < this.inputNumberClone; i++) {
      this.contractInfo.contract_details.push({
        company_id: this.contractInfo.company_id,
        contract_id: this.contractInfo.contract_id,
        trans_seq: 0,
        issue_ymd: '',
        amt_eta_ymd: '',
        issue_yn: false,
        issue_amt: 0,
        amt_actual_ymd: '',
        del_yn: false,
        use_yn: true,
        remark: '',
        creator: '',
        changer: '',
        created_time: '',
        changed_time: '',
        color: 'false',
        checkDepositDate: false,
        issue_month: 0,
        salesopt_id: 0
      })
    }
    this.inputNumberClone = 1;
  }
  batchDate() {
    if (this.contractInfo.contract_details.length > 0) {
      let first_date = this.contractInfo.contract_details[0].issue_ymd;
      if (first_date == '') {
        this.notification.showError('Please select Date of issue first row')
        return;
      }
      var tmp_date = first_date;
      for (let i = 0; i < this.contractInfo.contract_details.length; i++) {
        var parse_date = Date.parse(tmp_date).addMonths(i);
        var new_date = parse_date.toString('yyyy-MM-dd');
        this.contractInfo.contract_details[i].issue_ymd = new_date;
      }
    }
  }
  batchAmount() {
    if (this.contractInfo.contract_details.length > 0) {
      this.contractInfo.contract_details.forEach(dt => {
        dt.issue_amt = this.contractInfo.contract_amt / this.contractInfo.contract_details.length;
      });
    }
  }
  applyBulk() {
    if (this.contractInfo.contract_details.length > 0) {
      if (this.priceBulk == '') {
        this.notification.showError('Please enter the value')
        return;
      }
      this.contractInfo.contract_details.forEach(dt => {
        dt.issue_amt = parseInt(this.priceBulk);
      });
      this.priceBulk = '';
    }
  }
  issueConfirm(status, index) {
    if (this.contractInfo.contract_details.length > 0) {
      this.contractInfo.contract_details[index].issue_yn = status;
      this.contractInfo.contract_details[index].color = status.toString();
    }
  }
  checkDepositDate(item, index) {
    if (this.contractInfo.contract_details.length > 0) {
      if (!item) this.contractInfo.contract_details[index].amt_actual_ymd = '';
    }
  }
  refreshDetail(trans_seq, index) {
    if (trans_seq > 0) {
      this.contractInfo.contract_details[index] = JSON.parse(JSON.stringify(this.contractDetail_BK.contract_details[index]));
    }
  }
  removeDetail(item, index) {
    if (this.contractInfo.contract_details.length > 0) {
      if (item.trans_seq == 0) {
        this.contractInfo.contract_details.splice(index, 1);
        return;
      }
      this.notification.confirmDialog(
        "Deleting this item ?",
        `Deleting an item will move it to the <span class='warning-emphasize'>trash</span>.<br />
        Deleted items <span class='warning-emphasize'>can</span> be <span class='warning-emphasize'>recovered from the Recycle Bin within 30 days</span>.<br />
        Do you want to continue?`,
        x => {
          if (x) {
            this.crmContractDetailService.DeleteContractDetail(item).then(data => {
              if (data.error) {
                if (data.error.code === 403) {
                  this.router.navigate(["/error/error403"]);
                }
                this.notification.showMessage("error", data.error.message);
              } else {
                this.notification.showMessage("success", data.message);
                this.getContractDetail(this.contractInfo.contract_id)
              }
            })
          }
        }
      );
    }
  }
  saveContractDetail() {
    if (this.contractInfo.contract_details.length > 0) {
      let dt = this.contractInfo.contract_details;
      for (let i = 0; i < dt.length; i++) {
        if (!dt[i].issue_ymd) {
          this.notification.showMessage("error", "Date of issue not null");
          return false;
        }
      }
      this.crmContractDetailService.InsertContractDetail(this.contractInfo).then(data => {
        if (data.error) {
          if (data.error.code === 403) {
            this.router.navigate(["/error/error403"]);
          }
          this.notification.showMessage("error", data.error.message);
        } else {
          this.notification.showMessage("success", data.message);
          this.getContractDetail(this.contractInfo.contract_id)
        }
      });
    }
  }
  openEditDetailPopup() {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.modalRef = this.modalService.show(this.popupContract, config);
  }
  closePopup() {
    this.modalRef && this.modalRef.hide();
  }
  reloadData() {
    this.getContractDetail(this.contractInfo.contract_id)
  }
}
