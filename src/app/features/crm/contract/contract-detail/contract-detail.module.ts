import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContractDetailRoutingModule } from './contract-detail-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { ContractPopupModule } from '../contract-popup/contract-popup.module';
import { ContractDetailComponent } from './contract-detail.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
   CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SmartadminDatatableModule,
    ContractDetailRoutingModule,
    ContractPopupModule,
    NgxCurrencyModule,
  ],
  declarations: [ContractDetailComponent]
})
export class ContractDetailModule { }
