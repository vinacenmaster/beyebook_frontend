import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExpectedSalesStatisticsComponent } from './expected-sales-statistics.component';

const routes: Routes = [{
  path: '',
  component: ExpectedSalesStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExpectedSalesStatisticsRoutingModule { }
