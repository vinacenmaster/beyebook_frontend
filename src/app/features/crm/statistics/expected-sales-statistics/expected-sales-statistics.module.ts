import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExpectedSalesStatisticsRoutingModule } from './expected-sales-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { ExpectedSalesStatisticsComponent } from './expected-sales-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    ExpectedSalesStatisticsRoutingModule
  ],
  declarations: [ExpectedSalesStatisticsComponent]
})
export class ExpectedSalesStatisticsModule { }
