import { Component, OnInit } from '@angular/core';

// @ts-ignore
import * as am4core from "@amcharts/amcharts4/core";
// @ts-ignore
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CommonFunction } from '@app/core/common/common-function';
import { CrmActivityBusinessService } from '@app/core/services/crm/sale-opportunity.service';
import { element } from 'protractor';
import { Category } from '@app/core/common/static.enum';
@Component({
  selector: 'sa-expected-sales-statistics',
  templateUrl: './expected-sales-statistics.component.html',
  styleUrls: ['../statistics-common.css']
})
export class ExpectedSalesStatisticsComponent extends BasePage implements OnInit {
  userLogin: any;
  years: any[] = [];
  year: any;
  yearBK: number = 0;
  users: any[] = [];
  userSelected: any = {};
  userSelectedBK: any = {};
  activityStatus: any[] = [];
  progressList: any[] = [];
  progress: number = 0;
  progressBK: number = 0;
  possibilityList: any[] = [];
  possibility: number = 0;
  possibilityBK: number = 0;
  searchKey: string = '';
  searchKeyBK: string = '';
  listDataTable: any[] = [];
  sumArray: any[] = [];
  sumAll: number = 0;
  sumExpectedAmount: number = 0;
  listData: any[] = [];
  listDataBK: any[] = [];
  isFilter: boolean = false;

  ShowStatus1LoadingCenter: boolean = true;
  ShowStatus2LoadingCenter: boolean = true;
  ShowStatisticLoadingCenter: boolean = true;
  ShowDataTableLoadingCenter: boolean = true;
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public generalMasterService: GeneralMasterService,
    public crmActivityBusinessService: CrmActivityBusinessService,
  ) {
    super(userService);
  }
  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.userSelected = { user_id: 0, user_nm: 'AM All' };

    let process = [
      { text: 'Progress All', val: 0 },
      { text: 'over 10', val: 9 },
      { text: '20% or more', val: 19 },
      { text: 'More than 40%', val: 39 },
      { text: 'More than 60%', val: 59 },
      { text: 'More than 80%', val: 79 },
      { text: '100%', val: 99 },
    ];
    this.progressList = process;
    this.possibilityList = process;
    this.year = new Date().getFullYear();
    this.progress = 0;
    this.possibility = 0;
    Promise.all([this.getAdminList(), this.getActivitiStatus()]).then(([user, activityStatus]) => {
      this.users = user;
      this.activityStatus = activityStatus;
      this.getListData(this.year)
    })
  }
  getAdminList() {
    return this.userMasterService.listUsers()
  }
  getActivitiStatus() {
    return this.generalMasterService.listGeneralByCate(Category.ActivityStatus.valueOf())
  }
  showAllloading() {
    this.ShowStatus1LoadingCenter = true,
      this.ShowStatus2LoadingCenter = true,
      this.ShowStatisticLoadingCenter = true;
    this.ShowDataTableLoadingCenter = true;
  }
  hideAllloading() {
    this.ShowStatus1LoadingCenter = false,
      this.ShowStatus2LoadingCenter = false,
      this.ShowStatisticLoadingCenter = false;
    this.ShowDataTableLoadingCenter = false;
  }
  getListData(year) {
    this.crmActivityBusinessService.ShortList(this.userLogin.company_id, year).then(data => {
      this.listData = Object.assign({}, data);
      if (data.activity.length > 0 || data.contractdetail.length > 0) {
        this.CompareData(data);
      } else return [this.resetData(), this.hideAllloading(), this.inItExpectedStatistics(null)];
    })
  }
  CompareData(data) {
    data.activity.map((e, i) => {
      let status_temp = this.activityStatus.find(x => x.gen_cd == e.sales_status_gen_cd)
      if (status_temp) e.progress = status_temp.gen_nm.split('-')[0]
      let user_temp = this.users.find(x => x.user_id == e.adminid)
      if (user_temp) e.admin_nm = user_temp.user_nm

      e.details = [];
      for (let j = 1; j < 13; j++) {
        e.details.push({ 'month': j > 9 ? j : '0' + j, 'value': 0, 'salesopt_id': e.salesopt_id })
      }
      let detail_temp = data.contractdetail.find(x => x.salesopt_id == e.salesopt_id)
      if (detail_temp) {
        e.expect_amt = detail_temp.contract_amt;
        e.contracted = true;
      }
      return e;
    })
    var groups = data.contractdetail.reduce(function (r, o) {
      var _month = o.issue_ymd.split(('-'))[1];
      var m = o.salesopt_id;
      if (r[m]) {
        for (let i = 0; i < r[m].length; i++) {
          if (r[m][i].month == _month) {
            r[m][i].value = o.issue_amt
            r[m][i].salesopt_id = o.salesopt_id;
            break;
          }
        }
      } else {
        r[m] = [];
        for (let i = 1; i < 13; i++) {
          r[m].push({ month: i > 9 ? i : '0' + i, value: 0, salesopt_id: o.salesopt_id })
          if (r[m][i - 1].month == _month)
            r[m][i - 1] = { month: _month, value: o.issue_amt, salesopt_id: o.salesopt_id }
        }
      }
      return r;
    }, {});
    var result = Object.keys(groups).map(function (k) { return groups[k]; });

    data.activity.map((e, i) => {
      let temp = e.details.find(element => element.month == e.activity_fin_ymd.split(('-'))[1] && element.salesopt_id == e.salesopt_id)
      if (temp) {
        e.details[i] = { 'month': temp.month, 'value': e.expect_amt, salesopt_id: e.salesopt_id }
        let temp2 = result.find(m => m[0].salesopt_id == e.salesopt_id)
        if (temp2)
          e.details = temp2;
      }
      e.sum = e.details.reduce((sum, data) => {
        return sum + +data.value;
      }, 0);
      return e;
    });
    this.resetData()
    if (this.isFilter) {
      data.activity = this.filter(data.activity)
    }
    if (data.activity.length > 0) {
      data.activity.map((e, i) => {
        this.SumFunction(e)
        if (i == data.activity.length - 1) {
          this.hideAllloading();
          this.inItExpectedStatistics(this.sumArray)
        }
        return e;
      })
    } else {
      this.hideAllloading();
      this.inItExpectedStatistics(null)
    }
    this.listDataTable = data.activity;

  }
  SumFunction(data) {
    data.details.map((e, i) => {
      if (!this.sumArray[i])
        this.sumArray[i] = { month: e.month, value: +e.value }
      else
        this.sumArray[i].value += +e.value
    });
    this.sumAll += +data.sum;
    this.sumExpectedAmount += +data.expect_amt
  }
  resetData() {
    this.listDataTable = [];
    this.sumArray = [];
    this.sumAll = 0;
    this.sumExpectedAmount = 0;
  }
  inItExpectedStatistics(data) {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("ExpectedSalesStatistics", am4charts.XYChart);
    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "month";
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.cellStartLocation = 0.1
    categoryAxis.renderer.cellEndLocation = 0.9
    chart.zoomOutButton.disabled = true;
    function createLine(value) {
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "value";
      series.dataFields.categoryX = "month";
      series.columns.template.tooltipText = "[bold]Month[/]: {categoryX}\n [bold]{valueY}[/]";
      series.columns.template.fillOpacity = 1;
      series.data = value;
      var columnTemplate = series.columns.template;
      columnTemplate.width = am4core.percent(85);
      chart.data = value;
    };
    chart.colors.baseColor = am4core.color('#2185d0')
    if (data)
      createLine(data);
    $("#ExpectedSalesStatistics").find("[aria-labelledby]").hide();
  }

  filter(data) {
    let result = data;
    if (this.progress > 0) {
      result = result.filter(x => +x.progress.split('%')[0] > +this.progress);
    }
    if (this.possibility > 0) {
      result = result.filter(x => +x.possibility > +this.possibility);
    }
    if (this.userSelected.user_id > 0) {
      result = result.filter(x => x.adminid == this.userSelected.user_id);
    }
    if (this.searchKey) {
      result = result.filter(x => x.salesoptnm.toLowerCase().includes(this.searchKey.toLowerCase()));
    }
    return result;
  }
  onYearChange() {
    if (this.year !== this.yearBK) {
      this.showAllloading();
      this.yearBK = this.year
      this.getListData(this.year)
    }
  }
  onFilterChange(user?: any) {
    this.listDataBK = [];
    if (user)
      this.userSelected = { user_id: user.user_id, user_nm: user.user_nm };
    if (this.progress !== this.progressBK
      || this.possibility !== this.possibilityBK
      || this.userSelected !== this.userSelectedBK
      || this.searchKey.toLowerCase() !== this.searchKeyBK.toLowerCase()) {
      this.showAllloading();
      this.progressBK = this.progress;
      this.possibilityBK = this.possibility;
      this.userSelectedBK = this.userSelected;
      this.searchKeyBK = this.searchKey;
      this.isFilter = true;
      Object.assign(this.listDataBK, this.listData);
      this.CompareData(this.listDataBK)
    }
  }
  checkKeycode(event) {
    if (event.keyCode === 13) {
      this.onFilterChange();
    }
  }
}
