// import { Component, OnInit } from '@angular/core';
// import { CommonFunction } from '@app/core/common/common-function';
// import { AuthService, UserMasterService } from '@app/core/services';
// import { BasePage } from '@app/core/common/base-page';
// import { CrmContractService } from '@app/core/services/crm/contract.service';
// import { CrmContractModel } from '@app/core/models/crm/contract.model';
// import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
// import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
// import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
// import { Category } from '@app/core/common/static.enum';
// // @ts-ignore
// import * as am4core from "@amcharts/amcharts4/core";
// // @ts-ignore
// import * as am4charts from "@amcharts/amcharts4/charts";
// import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// import { CostOrderService } from '@app/core/services/crm/cost-order.service';
// @Component({
//   selector: 'sa-am-statistics',
//   templateUrl: './am-statistics.component.html',
//   styleUrls: ['../statistics-common.css']
// })
// export class AmStatisticsComponent extends BasePage implements OnInit {
//   userLogin: any;
//   years: any[] = [];
//   year: any;
//   yearBK: number = 0;
//   months: any[] = [];
//   month: any;
//   monthBK: number = 0;
//   users: any[] = [];
//   admin_selected: any = {};
//   listContract: CrmContractModel[] = [];
//   listContractBK: CrmContractModel[] = [];
//   contractName: string = '';
//   listOrder: any[] = [];
//   listCostOrder: any[] = [];
//   listSalesOpt: any[] = [];
//   saleType: any[] = [];
//   ShowStatus1LoadingCenter: boolean = true;
//   ShowStatus2LoadingCenter: boolean = true;
//   ShowStatus3LoadingCenter: boolean = true;
//   ShowStatus4LoadingCenter: boolean = true;
//   ShowStatisticLoadingCenter: boolean = true;
//   sumContractOrder: number = 0;
//   sumContractCostOrder: number = 0;
//   contractPercent: number = 0;

//   statbyMonthArr: any[] = [];
//   constructor(
//     public userService: AuthService,
//     public userMasterService: UserMasterService,
//     private crmContractService: CrmContractService,
//     private crmSalesOpportunityService: CrmSalesOpportunityService,
//     public salesOrderService: SalesOrderService,
//     public costOrderService: CostOrderService,
//     public generalMasterService: GeneralMasterService,
//   ) {
//     super(userService);
//   }

//   ngOnInit() {
//     this.userLogin = this.userService.getUserInfo();
//     this.years = CommonFunction.generateYear(-10, 0).reverse();
//     this.genMonth();
//     this.admin_selected = { id: 0, nm: 'AM All' };
//     this.year = new Date().getFullYear();
//     this.month = 0;

//     Promise.all([this.getAdminList(), this.getSaleType(), this.getListOrder(),this.getListCostOrder(), this.getListSaleOpt()]).then(([admin, saleType, Order,CostOrder, SalesOpt]) => {
//       this.users = admin;
//       this.saleType = saleType;
//       this.listOrder = Order;
//       this.listCostOrder = CostOrder;
//       this.listSalesOpt = SalesOpt;
//       this.getlistContract(admin, saleType, Order,CostOrder, SalesOpt, this.year, this.month);
//     });
//   }
//   genMonth() {
//     let m1 = 0;
//     let m2 = 12;
//     while (m2 > m1) {
//       m1++;
//       this.months.push({ val: m1, text: `Month: ${m1 > 9 ? m1 : '0' + m1}` })
//     }
//   }
//   getAdminList() {
//     return this.userMasterService.listUsers()
//   }
//   getSaleType() {
//     return this.generalMasterService.listGeneralByCate(Category.SalesOptType.valueOf())
//   }
//   getListOrder() {
//     return this.salesOrderService.ShortList(this.userLogin.company_id)
//   }
//   getListCostOrder() {
//     return this.costOrderService.ShortList(this.userLogin.company_id)
//   }
//   getListSaleOpt() {
//     return this.crmSalesOpportunityService.ShortList(this.userLogin.company_id)
//   }


//   getlistContract(listUser, listSaleType, listOrder,listCostOrder, listSaleOpt, year: number = 0, month: number = 0, contractName: string = '') {
//     this.crmContractService.ListFromAMStatistics(this.userLogin.company_id, year, month, contractName).then(data => {
//       this.addMoreInfo(data, listUser, listSaleType, listOrder,listCostOrder, listSaleOpt);
//     });
//   }

//   addMoreInfo(listContract, listUser, listSaleType, listOrder,listCostOrder, listSaleOpt) {
//     if (listContract.length <= 0) {
//       return [this.listContract = [],
//       this.listContractBK = [],
//       this.sumContractOrder = 0,
//       this.sumContractCostOrder = 0,
//       this.contractPercent = 0,
//       this.intItAmStatistics(),
//       this.hideAllloading()];
//     }
//     listContract.forEach((dt, index) => {
//       if (dt.src_order_type) {
//         listOrder.forEach(order => {
//           if (dt.src_order_id === order.order_id) {
//             listSaleOpt.forEach(sales => {
//               if (sales.salesopt_id === order.salesopt_id) {
//                 dt.salesopt_id = sales.salesopt_id;
//                 dt.order_nm = sales.salesopt_nm;
//                 listUser.forEach(us => {
//                   if (us.user_id == sales.admin_id)
//                     dt.sales_admin = us.user_nm;
//                 });
//                 listSaleType.forEach(type => {
//                   if (type.gen_cd == sales.salesopt_type_gen_cd)
//                     dt.salesopt_type_nm = type.gen_nm;
//                 });
//               }
//             });
//           }
//         });
//       }else{
//         listCostOrder.forEach(order => {
//           if (dt.src_order_id === order.cost_order_id) {
//             listSaleOpt.forEach(sales => {
//               if (sales.salesopt_id === order.salesopt_id) {
//                 dt.salesopt_id = sales.salesopt_id;
//                 dt.order_nm = sales.salesopt_nm;
//                 listUser.forEach(us => {
//                   if (us.user_id == sales.admin_id)
//                     dt.sales_admin = us.user_nm;
//                 });
//                 listSaleType.forEach(type => {
//                   if (type.gen_cd == sales.salesopt_type_gen_cd)
//                     dt.salesopt_type_nm = type.gen_nm;
//                 });
//               }
//             });
//           }
//         });
//       }
      
//       if (index == listContract.length - 1) {
//         this.listContract = listContract;
//         console.table(this.listContract)
//         this.listContractBK = listContract;
//         this.ShowStatus1LoadingCenter = false;
//         this.intItAmStatistics();
//         if (this.admin_selected.id != 0) {
//           this.onAMChange(this.admin_selected.id, this.admin_selected.nm);
//         } else {
//           this.totalContract(this.listContract);
//         }
//       }
//     });
//   }
//   totalContract(listContract) {
//     this.sumContractOrder = 0;
//     let cost = 0;
//     this.sumContractCostOrder = 0;
//     this.contractPercent = 0;
//     if (listContract.length <= 0) return this.hideAllloading();
//     listContract.forEach((list, index) => {
//       if (list.src_order_type) {
//         this.sumContractOrder += list.contract_amt;
//       } else {
//         cost += list.contract_amt;
//       }
//       if (index == listContract.length - 1) {
//         this.sumContractCostOrder = this.sumContractOrder + cost;
//         this.contractPercent = this.sumContractOrder <= 0 ? 0 : parseFloat((this.sumContractCostOrder / this.sumContractOrder * 100).toFixed(1));
//         this.hideAllloading();
//       }
//     });
//   }
//   showAllloading(){
//     this.ShowStatus1LoadingCenter = true,
//     this.ShowStatus2LoadingCenter = true,
//     this.ShowStatus3LoadingCenter = true,
//     this.ShowStatus4LoadingCenter = true;
//     this.ShowStatisticLoadingCenter = true;
//   }
//   hideAllloading(){
//     this.ShowStatus1LoadingCenter = false,
//     this.ShowStatus2LoadingCenter = false,
//     this.ShowStatus3LoadingCenter = false,
//     this.ShowStatus4LoadingCenter = false;
//     this.ShowStatisticLoadingCenter = false;
//   }
//   onDateChange(type) {
//     this.showAllloading();
//     if (this.year != this.yearBK && type === 'year') {
//       this.yearBK = this.year;
//       return this.getlistContract(this.users, this.saleType, this.listOrder,this.listCostOrder, this.listSalesOpt, this.year, this.month, this.contractName)
//     }
//     if (this.month != this.monthBK && type === 'month') {
//       this.monthBK = this.month;
//       return this.getlistContract(this.users, this.saleType, this.listOrder,this.listCostOrder, this.listSalesOpt, this.year, this.month, this.contractName)
//     }
//   }
//   onAMChange(userId, userName) {
//     this.showAllloading();
//     if (userName === 'all') return [this.admin_selected = { id: 0, nm: 'AM All' }, this.listContract = this.listContractBK, this.totalContract(this.listContract),this.intItAmStatistics()];
//     this.admin_selected.id = userId;
//     this.admin_selected.nm = userName;
//     this.listContract = this.listContractBK.filter(x => x.sales_admin == userName);
//     this.totalContract(this.listContract);
//     this.intItAmStatistics();
//   }


//   intItAmStatistics() {
//     am4core.useTheme(am4themes_animated);
//     var chart = am4core.create("AMstatistics", am4charts.XYChart);
//     // Create axes
//     var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
//     categoryAxis.dataFields.category = "group";
//     // categoryAxis.renderer.grid.template.location = 0;
//     // categoryAxis.renderer.grid.template.disabled = true;
//     categoryAxis.renderer.minGridDistance = 30;
//     categoryAxis.renderer.cellStartLocation = 0.1
//     categoryAxis.renderer.cellEndLocation = 0.9
//     chart.legend = new am4charts.Legend();
//     chart.zoomOutButton.disabled=true;
//     function createLine(data,name) {
//       var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
//       // valueAxis.renderer.grid.template.disabled = true;
//       // Create series
//       var series = chart.series.push(new am4charts.ColumnSeries());
//       series.dataFields.valueY = "data";
//       series.dataFields.categoryX = "group";
//       // series.fill = am4core.color(color);
//       // series.stroke = am4core.color(color);
//       series.name = name;
//       series.columns.template.tooltipText = "Month: {categoryX}\n [bold]{label}: {valueY}[/]";
//       series.columns.template.fillOpacity = .8;
//       series.data = data;
//       var columnTemplate = series.columns.template;
//       // columnTemplate.strokeWidth = 2;
//       // columnTemplate.strokeOpacity = 1;
//       columnTemplate.width = am4core.percent(85);
//       chart.data = data;
//     };

//     $("#AMstatistics").find("[aria-labelledby]").hide();
//     // Add data
//     this.statbyMonthArr = [];
//     this.users.forEach((user, index) => {
//       console.log(this.listContract.filter(x => x.sales_admin == user.user_nm))
//       this.statbyMonthArr.push(this.groupDataByMonth(this.listContract.filter(x => x.sales_admin == user.user_nm),user.user_nm));
//       if (index == this.users.length - 1) {
//         //---c1: dùng mặc định, load tất cả chart với toàn bộ admin lên
//         //--- nếu có quá nhiều admin, chart sẽ bị vỡ, hiển thị column rất nhỏ
//         // this.statbyMonthArr.forEach((chart) => {
//         //   createLine(chart,chart[0].label);
//         // });

//         //--c2: lọc ra những admin nào có sales amount !=0, những thanh niên ==0 sẽ không hiển thị cho đỡ chật chart
//         for (let i = 0; i < this.statbyMonthArr.length; i++) {
//           for (let j = 0; j < this.statbyMonthArr[i].length; j++) {
//             if (this.statbyMonthArr[i][j].data != '0') {
//               createLine(this.statbyMonthArr[i],this.statbyMonthArr[i][j].label);
//               break;
//             }
//           }
//         }
//         this.ShowStatisticLoadingCenter = false;
//       }
//     });
//   }
//   groupDataByMonth(data,name) {
//     let arr = [];
//     for (let i = 1; i <= 12; i++) {
//       let date = i < 10 ? '0' + i : i.toString();
//       arr.push({ group: date, data: 0,label:name });

//       if (i == 12) {
//         var groups = data.reduce(function (r, o) {
//           var m = o.contract_start_ymd.split(('-'))[1];

//           if (r[m]) {
//             r[m].data = parseInt(r[m].data) + parseInt(o.contract_amt);
//           } else {
//             r[m] = { group: m, data: o.contract_amt,label:name };
//           }
//           return r;
//         }, {});

//         var result = Object.keys(groups).map(function (k) { return groups[k]; });
//         arr.forEach((_arr, index) => {
//           result.forEach(res => {
//             var m1 = _arr.group.split(('-'))[0];
//             var m2 = res.group.split(('-'))[0];
//             if (m1 == m2) {
//               arr[index] = res;
//             }
//           });
//         });
//         return arr;
//       }
//     }
//   }

//   public search() {
//     this.showAllloading();
//     this.getlistContract(this.users, this.saleType, this.listOrder,this.listCostOrder, this.listSalesOpt, this.year, this.month, this.contractName)
//   }
//   checkKeycode(event) {
//     if (event.keyCode === 13) {
//       this.search();
//     }
//   }
// }
