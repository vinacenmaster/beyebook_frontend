import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AmStatisticsRoutingModule } from './am-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { AmStatisticsComponent } from './am-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    AmStatisticsRoutingModule
  ],
  declarations: [AmStatisticsComponent]
})
export class AmStatisticsModule { }
