import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AmStatisticsComponent } from './am-statistics.component';

const routes: Routes = [{
  path: '',
  component: AmStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmStatisticsRoutingModule { }
