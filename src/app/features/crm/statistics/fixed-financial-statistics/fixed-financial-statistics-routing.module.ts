import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FixedFinancialStatisticsComponent } from './fixed-financial-statistics.component';

const routes: Routes = [{
  path: '',
  component: FixedFinancialStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FixedFinancialStatisticsRoutingModule { }
