import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FixedFinancialStatisticsRoutingModule } from './fixed-financial-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { FixedFinancialStatisticsComponent } from './fixed-financial-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    FixedFinancialStatisticsRoutingModule
  ],
  declarations: [FixedFinancialStatisticsComponent]
})
export class FixedFinancialStatisticsModule { }
