import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CommonFunction } from '@app/core/common/common-function';
import { CrmContractDetailService, CrmContractService } from '@app/core/services/crm/contract.service';

// @ts-ignore
import * as am4core from "@amcharts/amcharts4/core";
// @ts-ignore
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
@Component({
  selector: 'sa-fixed-financial-statistics',
  templateUrl: './fixed-financial-statistics.component.html',
  styleUrls: ['../statistics-common.css']
})
export class FixedFinancialStatisticsComponent extends BasePage implements OnInit {
  userLogin: any;
  years: any[] = [];
  year: any;
  yearBK: any;
  listContract: any[] = [];
  listContractDetail: any[] = [];
  listDataTable: any[] = [];
  listDataTableBK: any[] = [];
  SalesStatistics: any[] = [];
  PurchaseStatistics: any[] = [];
  ProfitStatistics: any[] = [];
  contractName: string = '';
  ShowStatus1LoadingCenter: boolean = true;
  ShowStatus2LoadingCenter: boolean = true;
  ShowStatus3LoadingCenter: boolean = true;
  ShowStatus4LoadingCenter: boolean = true;
  ShowStatisticLoadingCenter: boolean = true;
  sumDefiniteSales: number = 0;
  sumDefinitePurchase: number = 0;
  sumProfit: number = 0;
  sumContractAmount: number = 0;
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public generalMasterService: GeneralMasterService,
    private crmContractService: CrmContractService,
    private crmContractDetailService: CrmContractDetailService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.year = new Date().getFullYear();
    Promise.all([this.getListContracts()]).then(([listContract]) => {
      this.listContract = listContract;
      this.getListContractDetails(this.year);
    });
  }
  showAllloading() {
    this.ShowStatus1LoadingCenter = true;
    this.ShowStatus2LoadingCenter = true;
    this.ShowStatus3LoadingCenter = true;
    this.ShowStatus4LoadingCenter = true;
    this.ShowStatisticLoadingCenter = true;
    this.resetData();
  }
  hideAllloading() {
    this.ShowStatus1LoadingCenter = false;
    this.ShowStatus2LoadingCenter = false;
    this.ShowStatus3LoadingCenter = false;
    this.ShowStatus4LoadingCenter = false;
    this.ShowStatisticLoadingCenter = false;
  }
  resetData() {
    this.listDataTable = [];
    this.listDataTableBK = [];
    this.sumDefiniteSales = 0;
    this.sumDefinitePurchase = 0;
    this.sumProfit = 0;
    this.sumContractAmount = 0;
    this.SalesStatistics = [];
    this.PurchaseStatistics = [];
    this.ProfitStatistics = [];
  }
  getListContracts() {
    return this.crmContractService.ListFromAMStatistics(this.userLogin.company_id, 0, 0, '');
  }
  defineColumnDefaultData(){
    for (let i = 1; i < 13; i++) {
      this.SalesStatistics.push({ 'group': i, 'data': 0, label: 'Definite Sales' })
      this.PurchaseStatistics.push({ 'group': i, 'data': 0, label: 'Definite Purchase' })
      this.ProfitStatistics.push({ 'group': i, 'data': 0, label: 'Profit' })
    }
  }
  getListContractDetails(year) {
    this.crmContractDetailService.ShortList(this.userLogin.company_id,0,0, year).then(data => {
      this.listContractDetail = data;
      this.defineColumnDefaultData();
      this.addMoreInfo(data, this.SalesStatistics, this.PurchaseStatistics, this.ProfitStatistics,this.contractName)
    });
  }
  addMoreInfo(data, SalesStatistics, PurchaseStatistics, ProfitStatistics,searchString:string='') {
    if (!data.length) {
      return [this.resetData(),
        this.hideAllloading(),
        this.inItFinancialStatistics(null,null,null)
      ];
    }
    data.forEach((dt, index) => {
      this.listContract.forEach(parent => {
        if (dt.contract_id == parent.contract_id) {
          dt.contract_nm = parent.contract_nm;
          dt.contract_start_ymd = parent.contract_start_ymd;
          dt.contract_end_ymd = parent.contract_end_ymd;
          dt.src_order_type = parent.src_order_type;
          dt.contract_amt = parent.contract_amt;
          if (!parent.src_order_type && !searchString) {
            dt.issue_amt = -dt.issue_amt;
          }
        }
      });
      if (index == data.length - 1) {
        if (searchString) {
          data = data.filter(x => x.contract_nm.toLowerCase().includes(searchString.toLowerCase()))
        }
        var groups = data.reduce(function (r, o) {
          var id = o.contract_id;
          if (r[id]) {
            for (let i = 0; i < r[id].prices.length; i++) {
              if (r[id].prices[i].month == o.issue_month) {
                let sum = r[id].prices[i].amount;
                r[id].prices[i].amount = +sum + +o.issue_amt;
                if (o.src_order_type)
                  SalesStatistics[i].data += +o.issue_amt;
                else
                  PurchaseStatistics[i].data += -o.issue_amt;
                ProfitStatistics[i].data = SalesStatistics[i].data - PurchaseStatistics[i].data
                break;
              }
            }
            if (o.amt_actual_ymd !== "") {
              for (let i = 0; i < r[id].paid.length; i++) {
                if (r[id].paid[i].month == o.issue_month) {
                  r[id].paid[i].amount = +r[id].paid[i].amount + +o.issue_amt;
                  break;
                }
                if (i == r[id].paid.length - 1) {
                  r[id].paid.push({ month: o.issue_month, amount: o.issue_amt });
                  break;
                }
              }
            }
          } else {
            r[id] = o;
            r[id].prices = [];
            r[id].paid = [];
            for (let i = 1; i <= 12; i++) {
              r[id].prices.push({ month: i, amount: '', issue_yn: false });
              r[id].paid.push({ month: i, amount: '' });
              if (i == o.issue_month) {
                r[id].prices[i - 1].month = o.issue_month;
                r[id].prices[i - 1].amount = o.issue_amt;
                r[id].prices[i - 1].issue_yn = o.issue_yn;
                if (o.amt_actual_ymd !== "") {
                  r[id].paid[i - 1].month = o.issue_month;
                  r[id].paid[i - 1].amount = o.issue_amt;
                }
                if (o.src_order_type)
                  SalesStatistics[i - 1].data += +o.issue_amt;
                else
                  PurchaseStatistics[i - 1].data += -o.issue_amt;
                ProfitStatistics[i - 1].data = SalesStatistics[i - 1].data - PurchaseStatistics[i - 1].data
              }
            }
          }
          return r;
        }, {});

        var result = Object.keys(groups).map(function (k) { return groups[k]; });
        for (let i = 0; i < result.length; i++) {
          let sum_price: number = 0;
          let sum_paid: number = 0;
          for (let j = 0; j < result[i].prices.length; j++) {
            sum_price = +sum_price + +result[i].prices[j].amount
            sum_paid = +sum_paid + +result[i].paid[j].amount
            for (let k = 0; k < this.listContractDetail.length; k++) {
              if (this.listContractDetail[k].contract_id == result[i].contract_id) {
                if (this.listContractDetail[k].issue_month == result[i].prices[j].month) {
                  result[i].prices[j].issue_yn = this.listContractDetail[k].issue_yn;
                  break;
                }
              }
            }
          }
          result[i].sum_price = sum_price;
          result[i].sum_paid = sum_paid;
        }
        this.calculatingPrice(result);
        this.hideAllloading();
        this.listDataTable = result;
        Object.assign(this.listDataTableBK,this.listDataTable)
        this.inItFinancialStatistics(this.SalesStatistics, this.PurchaseStatistics, this.ProfitStatistics)
      }
    });
  }
  calculatingPrice(data) {
    let sum_contract = 0;
    data.forEach((dt, index) => {
      sum_contract += dt.contract_amt;
      if (dt.src_order_type)
        this.sumDefiniteSales += +dt.sum_price
      else this.sumDefinitePurchase += +dt.sum_price
      if (index == data.length - 1) {
        this.sumProfit = +this.sumDefiniteSales + +this.sumDefinitePurchase;
        this.sumContractAmount = sum_contract;
      }
    });
  }
  inItFinancialStatistics(SalesStatistics, PurchaseStatistics, ProfitStatistics) {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("FixedFinancialstatistics", am4charts.XYChart);
    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "group";
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.cellStartLocation = 0.1
    categoryAxis.renderer.cellEndLocation = 0.9
    chart.legend = new am4charts.Legend();
    chart.zoomOutButton.disabled = true;
    function createLine(data, name) {
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "data";
      series.dataFields.categoryX = "group";
      series.name = name;
      series.columns.template.tooltipText = "[bold]Month[/]: {categoryX}\n {label}: [bold]{valueY}[/]";
      series.columns.template.fillOpacity = .8;
      series.data = data;
      var columnTemplate = series.columns.template;
      columnTemplate.width = am4core.percent(85);
      chart.data = data;
    };
    chart.colors.list = [
      am4core.color('#2185d0'),
      am4core.color('#db2828'),
      am4core.color('#fbbd08')
    ]
    if (SalesStatistics || PurchaseStatistics || ProfitStatistics) {
      createLine(SalesStatistics, 'Definite Sales');
      createLine(PurchaseStatistics, 'Definite Purchase');
      createLine(ProfitStatistics, 'Profit');
    }
    $("#FixedFinancialstatistics").find("[aria-labelledby]").hide();
  }
  onYearChange() {
    this.showAllloading();
    if (this.year != this.yearBK) {
      this.yearBK = this.year;
      return this.getListContractDetails(this.year)
    }
  }
  public search() {
    this.showAllloading();
    if (!this.contractName) {
      return this.getListContractDetails(this.year);
    }
    let key:any = this.contractName.toLowerCase();
    Promise.all([this.defineColumnDefaultData()]).then(([dataDefault]) => {
      this.addMoreInfo(this.listContractDetail, this.SalesStatistics, this.PurchaseStatistics, this.ProfitStatistics,key)
    });
  }
  checkKeycode(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }
}
