import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CostByCustomerStatisticsComponent } from './cost-by-customer-statistics.component';

const routes: Routes = [{
  path: '',
  component: CostByCustomerStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostByCustomerStatisticsRoutingModule { }
