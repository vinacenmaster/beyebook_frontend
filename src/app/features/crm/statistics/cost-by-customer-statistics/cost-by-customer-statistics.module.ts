import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CostByCustomerStatisticsRoutingModule } from './cost-by-customer-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { CostByCustomerStatisticsComponent } from './cost-by-customer-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    CostByCustomerStatisticsRoutingModule
  ],
  declarations: [CostByCustomerStatisticsComponent]
})
export class CostByCustomerStatisticsModule { }
