import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesByCustomerStatisticsComponent } from './sales-by-customer-statistics.component';

const routes: Routes = [{
  path: '',
  component: SalesByCustomerStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesByCustomerStatisticsRoutingModule { }
