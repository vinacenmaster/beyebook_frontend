import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesByCustomerStatisticsRoutingModule } from './sales-by-customer-statistics-routing.module';
import { SalesByCustomerStatisticsComponent } from './sales-by-customer-statistics.component';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SalesByCustomerStatisticsRoutingModule
  ],
  declarations: [SalesByCustomerStatisticsComponent]
})
export class SalesByCustomerStatisticsModule { }
