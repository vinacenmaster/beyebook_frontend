import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CommonFunction } from '@app/core/common/common-function';
import { CrmContractService } from '@app/core/services/crm/contract.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { Category } from '@app/core/common/static.enum';

@Component({
  selector: 'sa-sales-by-customer-statistics',
  templateUrl: './sales-by-customer-statistics.component.html',
  styleUrls: ['../statistics-common.css']
})
export class SalesByCustomerStatisticsComponent extends BasePage implements OnInit {
  userLogin: any;
  years: any[] = [];
  year: any;
  yearBK: any;
  divisions: any[] = [];
  division: any;
  divisionBK: any;
  accountName: string = '';
  listTotalContract: any[] = [];
  listTotalContractBK: any[] = [];
  listContractByYear: any[] = [];
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public generalMasterService: GeneralMasterService,
    private crmContractService: CrmContractService,
    private traderService: TraderService,
    public salesOrderService: SalesOrderService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.year = new Date().getFullYear();

    this.getContractData();
  }
  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id)
  }
  getListOrder() {
    return this.salesOrderService.ShortList(this.userLogin.company_id)
  }
  getCate() {
    return this.generalMasterService.listGeneralByCate(Category.TransTypeCateCode.valueOf())
  }
  getContractData() {
    this.crmContractService.ShortList(this.userLogin.company_id, true).then(data => {
      this.addMoreInfo(data);
    });
  }
  addMoreInfo(data) {
    Promise.all([this.getCustomer(), this.getListOrder(), this.getCate()]).then(([customers, orders, divisions]) => {
      this.divisions = divisions;
      this.division = 'all';
      data.forEach((dt, index) => {
        orders.forEach(ord => {
          if (dt.src_order_id == ord.order_id) {
            customers.forEach(cus => {
              if (cus.trader_id == ord.customer_contactor_id) {
                dt.customer_id = cus.trader_id;
                dt.customer_nm = cus.trader_local_nm;
                divisions.forEach(dv => {
                  if (dv.gen_cd == cus.cate_gen_cd) {
                    dt.division = dv.gen_cd;
                    dt.division_nm = dv.gen_nm;
                  }
                });
              }
            });
          }
        });
        if (index === data.length - 1)
          this.listTotalContract = this.groupByCustomer(data).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
        Object.assign(this.listTotalContractBK, data);
        var _filter = this.filter(this.listTotalContractBK);
        this.listContractByYear = this.groupByCustomer(_filter).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
      });
    });
  }
  groupByCustomer(data) {
    var groups = data.reduce(function (r, o) {
      var m = o.customer_id;
      if (r[m]) {
        r[m].data = parseInt(r[m].data) + parseInt(o.contract_amt);
        r[m].count++;
      } else {
        r[m] = { group: m, group_nm: o.customer_nm, division: o.division, division_nm: o.division_nm, data: o.contract_amt, count: 1, year: o.contract_start_ymd };
      }
      return r;
    }, {});

    var result = Object.keys(groups).map(function (k) { return groups[k]; });
    return result;
  }
  filter(data) {
    let result = [];
    let yearFilter = data.filter(x => x.contract_start_ymd.split(('-'))[0] == this.year);
    result = yearFilter;
    if (this.division !== 'all') {
      result = yearFilter.filter(x => x.division == this.division);
    }
    if (this.accountName) {
      result = result.filter(x => x.customer_nm.toLowerCase().includes(this.accountName.toLowerCase()));
    }
    return result;
  }
  onFilterChange() {
    if (this.year != this.yearBK || this.division != this.divisionBK) {
      this.yearBK = this.year;
      this.divisionBK = this.division;
      var _filter = this.filter(this.listTotalContractBK);
      this.listContractByYear = this.groupByCustomer(_filter).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
    }
  }
  search() {
    var _filter = this.filter(this.listTotalContractBK);
    this.listContractByYear = this.groupByCustomer(_filter).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
  }
  checkKeycode(event) {
    if (event.keyCode === 13) this.search();
  }
}
