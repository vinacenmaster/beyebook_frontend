import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CommonFunction } from '@app/core/common/common-function';

@Component({
  selector: 'sa-sales-by-product-statistics',
  templateUrl: './sales-by-product-statistics.component.html',
  styleUrls: ['../statistics-common.css']
})
export class SalesByProductStatisticsComponent extends BasePage implements OnInit {
  userLogin: any;
  years: any[] = [];
  year: any;
  products: any[] = [];
  product: any;
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public generalMasterService: GeneralMasterService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.year = new Date().getFullYear();
    this.products = [
      {text:'Category 1',val:1},
      {text:'Service',val:2},
      {text:'Product',val:3}
    ];
    this.product = 1;
  }
  onYearChange(){
    
  }
  onProductChange(){

  }
}
