import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesByProductStatisticsComponent } from './sales-by-product-statistics.component';

const routes: Routes = [{
  path: '',
  component: SalesByProductStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesByProductStatisticsRoutingModule { }
