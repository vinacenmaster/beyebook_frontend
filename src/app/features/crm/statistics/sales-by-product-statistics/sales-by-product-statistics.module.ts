import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesByProductStatisticsRoutingModule } from './sales-by-product-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SalesByProductStatisticsComponent } from './sales-by-product-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SalesByProductStatisticsRoutingModule
  ],
  declarations: [SalesByProductStatisticsComponent]
})
export class SalesByProductStatisticsModule { }
