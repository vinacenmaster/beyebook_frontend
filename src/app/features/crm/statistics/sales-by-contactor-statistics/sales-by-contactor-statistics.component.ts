import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CommonFunction } from '@app/core/common/common-function';
import { CrmContractService } from '@app/core/services/crm/contract.service';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { Category } from '@app/core/common/static.enum';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { forEach } from '@angular/router/src/utils/collection';
import { TraderService } from '@app/core/services/features.services/trader-master.service';

@Component({
  selector: 'sa-sales-by-contactor-statistics',
  templateUrl: './sales-by-contactor-statistics.component.html',
  styleUrls: ['../statistics-common.css']
})
export class SalesByContactorStatisticsComponent extends BasePage implements OnInit {
  userLogin: any;
  years: any[] = [];
  year: any;
  yearBK: any;
  // divisions: any[] = [];
  listTotalContract: any[] = [];
  listTotalContractBK: any[] = [];
  listContractByYear: any[] = [];
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public generalMasterService: GeneralMasterService,
    private crmContractService: CrmContractService,
    public salesOrderService: SalesOrderService,
    public contactorService: ContactorService,
    public traderService: TraderService,
    public crmSalesOpportunityService: CrmSalesOpportunityService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.year = new Date().getFullYear();
    this.getContractData();
  }
  getContactor() {
    return this.contactorService.ShortList(this.userLogin.company_id)
  }
  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id)
  }
  getListOrder() {
    return this.salesOrderService.ShortList(this.userLogin.company_id)
  }
  getListSalesOpt() {
    return this.crmSalesOpportunityService.ShortList(this.userLogin.company_id)
  }
  getContactorType() {
    return this.generalMasterService.listGeneralByCate(Category.ContactorType.valueOf())
  }
  getAdminList() {
    return this.userMasterService.listUsers()
  }
  getContractData() {
    this.crmContractService.ShortList(this.userLogin.company_id, true).then(data => {
      this.addMoreInfo(data);
    });
  }
  addMoreInfo(data) {
    Promise.all([this.getContactor(),this.getCustomer(), this.getListOrder(), this.getListSalesOpt(),this.getContactorType(),this.getAdminList()])
    .then(([contactors,customers, orders, salesOpts,contactorType,admins]) => {
      data.forEach((dt, index) => {
        orders.forEach(ord => {
          if (dt.src_order_id == ord.order_id) {
            salesOpts.forEach(sales => {
              if (sales.salesopt_id == ord.salesopt_id) {
                contactors.forEach(con => {
                  if (con.contactor_id == sales.customer_contactor_id) {
                    dt.contactor_id = con.contactor_id;
                    dt.contactor_nm = con.contactor_nm;
                    contactorType.forEach(type => {
                      if (type.gen_cd == con.contactor_type_gen_cd) {
                        dt.division_nm = type.gen_nm
                      }
                    });
                    admins.forEach(admin => {
                      if (admin.user_id == con.am_id) {
                        dt.admin_nm = admin.user_nm
                      }
                    });
                    customers.forEach(cus => {
                      if (cus.trader_id == con.trader_id) 
                      dt.trader_nm = cus.trader_local_nm;
                    });
                  }
                });
              }
            });
          }
        });
        if (index === data.length - 1)
        {
          this.listTotalContract = this.groupByCustomer(data).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
        Object.assign(this.listTotalContractBK, data);
        var _filter = this.filter(this.listTotalContractBK);
        this.listContractByYear = this.groupByCustomer(_filter).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
        }
      });
    });
  }
  groupByCustomer(data) {
    var groups = data.reduce(function (r, o) {
      var m = o.contactor_id;
      if (r[m]) {
        r[m].data = parseInt(r[m].data) + parseInt(o.contract_amt);
        r[m].count++;
      } else {
        r[m] = { group: m, group_nm: o.contactor_nm,trader:o.trader_nm, division_nm: o.division_nm,admin_nm: o.admin_nm, data: o.contract_amt, count: 1, year: o.contract_start_ymd };
      }
      return r;
    }, {});

    var result = Object.keys(groups).map(function (k) { return groups[k]; });
    return result;
  }
  filter(data) {
    return data.filter(x => x.contract_start_ymd.split(('-'))[0] == this.year);
  }
  onYearChange() {
    if (this.year != this.yearBK) {
      this.yearBK = this.year;
      var _filter = this.filter(this.listTotalContractBK);
      this.listContractByYear = this.groupByCustomer(_filter).sort((a, b) => (a.data < b.data) ? 1 : ((b.data < a.data) ? -1 : 0));
    }
  }
}
