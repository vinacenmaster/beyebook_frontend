import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesByContactorStatisticsComponent } from './sales-by-contactor-statistics.component';

const routes: Routes = [{
  path: '',
  component: SalesByContactorStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesByContactorStatisticsRoutingModule { }
