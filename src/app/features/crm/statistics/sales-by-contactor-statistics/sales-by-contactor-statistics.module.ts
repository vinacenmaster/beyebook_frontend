import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesByContactorStatisticsRoutingModule } from './sales-by-contactor-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SalesByContactorStatisticsComponent } from './sales-by-contactor-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SalesByContactorStatisticsRoutingModule
  ],
  declarations: [SalesByContactorStatisticsComponent]
})
export class SalesByContactorStatisticsModule { }
