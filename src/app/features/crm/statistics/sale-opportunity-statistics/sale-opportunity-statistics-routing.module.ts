import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaleOpportunityStatisticsComponent } from './sale-opportunity-statistics.component';

const routes: Routes = [{
  path: '',
  component: SaleOpportunityStatisticsComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaleOpportunityStatisticsRoutingModule { }
