import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService } from '@app/core/services';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { CommonFunction } from '@app/core/common/common-function';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { Category } from '@app/core/common/static.enum';
import { CrmSalesOpportunityModel } from '@app/core/models/crm/sales-opportunity.model';
// @ts-ignore
import * as am4core from "@amcharts/amcharts4/core";
// @ts-ignore
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
@Component({
  selector: 'sa-sale-opportunity-statistics',
  templateUrl: './sale-opportunity-statistics.component.html',
  styleUrls: ['../statistics-common.css']
})
export class SaleOpportunityStatisticsComponent extends BasePage implements OnInit {
  userLogin: any;
  years: any[] = [];
  year: any;
  yearBK: number = 0;
  users: any[] = [];
  admin_selected: any = {};
  listSalesOpt: CrmSalesOpportunityModel[] = [];
  listSalesOptBK: CrmSalesOpportunityModel[] = [];
  listActivity: any[] = [];
  listSaleType: any[] = [];
  ShowStatus1LoadingCenter: boolean = true;
  ShowStatus2LoadingCenter: boolean = true;
  ShowStatus3LoadingCenter: boolean = true;
  ShowStatus4LoadingCenter: boolean = true;
  ShowStatisticLoadingCenter: boolean = true;
  ShowPie1LoadingCenter: boolean = true;
  ShowPie2LoadingCenter: boolean = true;
  ShowPie3LoadingCenter: boolean = true;
  percentCheck: boolean = false;

  listStatus: any = { total: 0, success: 0, failure: 0, proceeding: 0 };
  listStatusBK: any = {};
  listSalesOptFiltered: CrmSalesOpportunityModel[] = [];
  listStatisticbyStatus: any[] = [];
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    private crmSalesOpportunityService: CrmSalesOpportunityService,
    public generalMasterService: GeneralMasterService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.admin_selected = { id: 0, nm: 'AM All' };
    this.year = new Date().getFullYear();
    this.percentCheck = false;
    this.getAdminList();
    Promise.all([this.getActivitiStatus(), this.getSaleType()]).then(([listActivity, listSaleType]) => {
      this.listActivity = listActivity;
      this.listSaleType = listSaleType;
      this.getlistSalesOpt(this.year, listActivity, listSaleType);
    });
  }
  getAdminList() {
    this.userMasterService.listUsers().then(data => {
      this.users = data;
    })
  }
  getActivitiStatus() {
    return this.generalMasterService.listGeneralByCate(Category.ActivityStatus.valueOf())
  }
  getSaleType() {
    return this.generalMasterService.listGeneralByCate(Category.SalesOptType.valueOf())
  }
  showAllloading() {
    this.ShowStatus1LoadingCenter = true,
      this.ShowStatus2LoadingCenter = true,
      this.ShowStatus3LoadingCenter = true,
      this.ShowStatus4LoadingCenter = true;
    this.ShowStatisticLoadingCenter = true;
    this.ShowPie1LoadingCenter = true;
    this.ShowPie2LoadingCenter = true;
    this.ShowPie3LoadingCenter = true;
    this.resetData();
  }
  hideAllloading() {
    this.ShowStatus1LoadingCenter = false,
      this.ShowStatus2LoadingCenter = false,
      this.ShowStatus3LoadingCenter = false,
      this.ShowStatus4LoadingCenter = false;
    this.ShowStatisticLoadingCenter = false;
    this.ShowPie1LoadingCenter = false;
    this.ShowPie2LoadingCenter = false;
    this.ShowPie3LoadingCenter = false;
  }
  resetData() {
    this.listStatus = { total: 0, success: 0, failure: 0, proceeding: 0 };
    this.listSalesOptFiltered = [];
    this.listStatisticbyStatus = [];
  }
  getlistSalesOpt(year: number = 0, listActivity, listSaleType) {
    this.crmSalesOpportunityService.ListFromSalesOptStatistics(this.userLogin.company_id, year).then(data => {
      this.listSalesOpt = data;
      this.listSalesOptBK = data;
      if (this.admin_selected.id == 0) this.addMoreInfo(data, listActivity, listSaleType);
      else {
        this.addMoreInfo(this.listSalesOptBK.filter(x => x.admin_id == this.admin_selected.id), listActivity, listSaleType)
      }
    })
  }
  addMoreInfo(listSalesOpt, listActivity, listSaleType) {
    if (listSalesOpt.length <= 0) {
      this.hideAllloading();
      this.intItSalesOptstatistics(null, null, null);

    }
    if (this.listStatisticbyStatus.length <= 0) {
      this.inItPieStatistics(null, 'SuccessPieStatistics');
      this.inItPieStatistics(null, 'FailurePieStatistics');
      this.inItPieStatistics(null, 'ProceedPieStatistics');
    }
    listSalesOpt.forEach((sales, index) => {
      listSaleType.forEach(type => {
        if (sales.salesopt_type_gen_cd == type.gen_cd)
          sales.salesopt_type_nm = type.gen_nm;
      });
      if (sales.sale_activitys) {
        listActivity.forEach(act => {
          if (sales.sale_activitys.sales_status_gen_cd == act.gen_cd) {
            sales.sale_activitys.sales_status_gen_nm = act.gen_nm;
            this.listSalesOptFiltered.push(sales);
            this.listStatus.total++;
            this.calcStatusSaleOpt(act.gen_nm);
          }
        });
      }
      if (index == listSalesOpt.length - 1) {
        this.hideAllloading();
        this.listStatusBK = Object.assign({}, this.listStatus);
        if (!this.listSalesOptFiltered.length) return this.intItSalesOptstatistics(null, null, null);
        Promise.all([this.groupDataByMonth(this.listSalesOptFiltered.filter(x => x.sale_activitys.sales_status_gen_nm.includes('Get Order')), 'success')
          , this.groupDataByMonth(this.listSalesOptFiltered.filter(x => x.sale_activitys.sales_status_gen_nm.includes('Lost or Hoding')), 'failure')
          , this.groupDataByMonth(this.listSalesOptFiltered.filter(x => !x.sale_activitys.sales_status_gen_nm.includes('Get Order') && !x.sale_activitys.sales_status_gen_nm.includes('Lost or Hoding')), 'proceeding')
        ]).then(([success, failure, proceed]) => {
          this.intItSalesOptstatistics(success, failure, proceed);
          this.inItPieStatistics(this.listStatisticbyStatus[0], 'SuccessPieStatistics');
          this.inItPieStatistics(this.listStatisticbyStatus[1], 'FailurePieStatistics');
          this.inItPieStatistics(this.listStatisticbyStatus[2], 'ProceedPieStatistics');
        });
      }
    });
  }
  calcStatusSaleOpt(type) {
    switch (type) {
      case '100% - Get Order':
        this.listStatus.success++;
        break;
      case 'Lost or Hoding':
        this.listStatus.failure++;
        break;
      default:
        this.listStatus.proceeding++;
        break;
    }
  }
  onYearChange() {
    this.showAllloading();
    if (this.year != this.yearBK) {
      this.yearBK = this.year;
      return this.getlistSalesOpt(this.year, this.listActivity, this.listSaleType)
    }
  }
  onAMChange(userId, userName) {
    this.showAllloading();
    if (userName === 'all')
      return [this.admin_selected = { id: 0, nm: 'AM All' },
      this.listSalesOpt = this.listSalesOptBK,
      this.addMoreInfo(this.listSalesOpt, this.listActivity, this.listSaleType)];
    this.admin_selected = { id: userId, nm: userName };
    this.listSalesOpt = this.listSalesOptBK.filter(x => x.admin_id == userId);
    this.addMoreInfo(this.listSalesOpt, this.listActivity, this.listSaleType)
  }
  viewAsPercent(checked) {
    if (checked)
      return [
        this.listStatus.success = this.listStatus.total <= 0 ? 0 : parseFloat((this.listStatus.success / this.listStatus.total * 100).toFixed(1)),
        this.listStatus.failure = this.listStatus.total <= 0 ? 0 : parseFloat((this.listStatus.failure / this.listStatus.total * 100).toFixed(1)),
        this.listStatus.proceeding = this.listStatus.total <= 0 ? 0 : parseFloat((this.listStatus.proceeding / this.listStatus.total * 100).toFixed(1)),
      ];
    else this.listStatus = Object.assign({}, this.listStatusBK);
  }
  intItSalesOptstatistics(success, failure, proceed) {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("SalesOptstatistics", am4charts.XYChart);
    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "group";
    // categoryAxis.renderer.grid.template.location = 0;
    // categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.cellStartLocation = 0.1
    categoryAxis.renderer.cellEndLocation = 0.9
    chart.legend = new am4charts.Legend();
    chart.zoomOutButton.disabled = true;
    chart.colors.list = [
      am4core.color('#21ba45'),
      am4core.color('#db2828'),
      am4core.color('#fbbd08')
    ]
    function createLine(data, name) {
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      // valueAxis.renderer.grid.template.disabled = true;
      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "data";
      series.dataFields.categoryX = "group";
      // series.fill = am4core.color(color);
      // series.stroke = am4core.color(color);
      series.name = name;
      series.columns.template.tooltipText = "Month: {categoryX}\n [bold]{label}: {valueY}[/]";
      series.columns.template.fillOpacity = .8;
      series.data = data;
      var columnTemplate = series.columns.template;
      // columnTemplate.strokeWidth = 2;
      // columnTemplate.strokeOpacity = 1;
      columnTemplate.width = am4core.percent(85);
      // Add data
      chart.data = data;
    };
    
    if (success || failure || proceed) {
      createLine(success, 'success');
      createLine(failure, 'failure');
      createLine(proceed, 'proceeding');
    }
    $("#SalesOptstatistics").find("[aria-labelledby]").hide();
  }
  inItPieStatistics(data, divId) {
    let chart = am4core.create(divId, am4charts.PieChart);
    let pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "data";
    pieSeries.dataFields.category = "group";
    // Let's cut a hole in our Pie chart the size of 30% the radius
    chart.innerRadius = am4core.percent(30);
    // Put a thick white border around each Slice
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.slices.template
      // change the cursor on hover to make it apparent the object can be interacted with
      .cursorOverStyle = [
        {
          "property": "cursor",
          "value": "pointer"
        }
      ];
    pieSeries.ticks.template.disabled = true;
    pieSeries.labels.template.disabled = true;
    pieSeries.colors.list = [
      am4core.color('#FF6384'),
      am4core.color('#36A2EB'),
      am4core.color('#FFCE56')
    ]
    // Create a base filter effect (as if it's not there) for the hover to return to
    let shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
    shadow.opacity = 0;
    // Create hover state
    let hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists
    // Slightly shift the shadow and make it more prominent on hover
    let hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
    hoverShadow.opacity = 0.7;
    hoverShadow.blur = 5;
    // Add a legend
    chart.legend = new am4charts.Legend();
    chart.data = data;
    $("#SuccessPieStatistics").find("[aria-labelledby]").hide();
    $("#FailurePieStatistics").find("[aria-labelledby]").hide();
    $("#ProceedPieStatistics").find("[aria-labelledby]").hide();
  }
  groupDataByMonth(data, label) {
    this.groupDatabyStatus(data, label);
    let arr = [];
    for (let i = 1; i <= 12; i++) {
      let date = i < 10 ? '0' + i : i.toString();
      arr.push({ group: date, data: 0, label: label });

      if (i == 12) {
        var groups = data.reduce(function (r, o) {
          var m = o.sale_activitys.activity_ymd.split(('-'))[1];
          if (r[m]) {
            r[m].data++;
          } else {
            r[m] = { group: m, data: 1, label: label };
          }
          return r;
        }, {});
        var result = Object.keys(groups).map(function (k) { return groups[k]; });
        arr.forEach((_arr, index) => {
          result.forEach(res => {
            var m1 = _arr.group.split(('-'))[0];
            var m2 = res.group.split(('-'))[0];
            if (m1 == m2) {
              arr[index] = res;
            }
          });
        });
        return arr;
      }
    }
  }
  groupDatabyStatus(data, label) {
    let arr = [];
    arr.push({ group: 'Inbound', data: 0, label: label });
    arr.push({ group: 'Outbound', data: 0, label: label });
    arr.push({ group: 'Existing Customer', data: 0, label: label });

    var groups = data.reduce(function (r, o) {
      var m = o.salesopt_type_nm;
      if (r[m]) {
        r[m].data++;
      } else {
        r[m] = { group: m, data: 1, label: label };
      }
      return r;
    }, {});
    var result = Object.keys(groups).map(function (k) { return groups[k]; });
    arr.forEach((_arr, index) => {
      result.forEach(res => {
        if (_arr.group == res.group) {
          arr[index] = res;
        }
      });
    });
    this.listStatisticbyStatus.push(arr);
  }
}
