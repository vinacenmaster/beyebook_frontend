import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaleOpportunityStatisticsRoutingModule } from './sale-opportunity-statistics-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SaleOpportunityStatisticsComponent } from './sale-opportunity-statistics.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SaleOpportunityStatisticsRoutingModule
  ],
  declarations: [SaleOpportunityStatisticsComponent]
})
export class SaleOpportunityStatisticsModule { }
