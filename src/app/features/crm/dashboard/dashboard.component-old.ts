// import { Component, OnInit } from '@angular/core';
// import { CrmDashboardService } from '@app/core/services/crm/dashboard.service';
// import { BasePage } from '@app/core/common/base-page';
// import { AuthService, UserMasterService, NotificationService } from '@app/core/services';
// import { TraderService } from '@app/core/services/features.services/trader-master.service';
// import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
// import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
// import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
// import { Category } from '@app/core/common/static.enum';
// import { CrmProjectService } from '@app/core/services/crm/project.service';
// import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
// import { Router } from '@angular/router';
// import { CommonFunction } from '@app/core/common/common-function';
// import { CrmContractDetailService, CrmContractService } from '@app/core/services/crm/contract.service';
// import { CrmSalesTargetDetailService } from '@app/core/services/crm/sales-target.service';

// // @ts-ignore
// import * as am4core from "@amcharts/amcharts4/core";
// // @ts-ignore
// import * as am4charts from "@amcharts/amcharts4/charts";
// import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// // import am4lang_lt_LT from "@amcharts/amcharts4/lang/lt_LT";

// @Component({
//   selector: 'sa-dashboard',
//   templateUrl: './dashboard.component.html',
//   styleUrls: ['./dashboard.component.css', '../common-activity/common-activity.css']
// })
// export class DashboardComponent extends BasePage implements OnInit {
//   recentlyActivity: any[] = [];
//   recentlyOrder: any[] = [];
//   userLogin: any;
//   indexExpand: number = -1;
//   customer: any[] = [];
//   // contactor: any[] = [];
//   // user: any[] = [];
//   salesOpt: any[] = [];
//   // project: any[] = [];
//   // taskType: any[] = [];
//   // workUnit: any[] = [];
//   ShowStatus1LoadingCenter: boolean = true;
//   ShowStatus2LoadingCenter: boolean = true;
//   ShowStatus3LoadingCenter: boolean = true;
//   ShowStatus4LoadingCenter: boolean = true;
//   ShowActivityLoadingCenter: boolean = true;
//   ShowOrderLoadingCenter: boolean = true;
//   ShowQuarterlySalesLoading: boolean = true;
//   ShowFixedFinancialLoadingCenter: boolean = true;
//   years: any[] = [];
//   year: number = 0;
//   yearBK: number = 0;
//   listSaleStatus: any[] = [];
//   DBSaleStatus_amt: number = 0;
//   SumSaleStatus: number = 0;
//   saleStatusRate: number = 0;
//   listContract: any[] = [];
//   listContractDetail: any[] = [];

//   listGrossProfit: any[] = [];
//   DBGrossProfit_amt: number = 0;
//   SumGrossProfit: number = 0;
//   GrossProfitRate: number = 0;
//   Order_ratio: any = {};

//   listQuarterSaleTarget: any[] = [];
//   listQuarterSales: any[] = [];

//   SalesStatistics: any[] = [];
//   PurchaseStatistics: any[] = [];
//   ProfitStatistics: any[] = [];
//   constructor(
//     private crmDashboardService: CrmDashboardService,
//     public userService: AuthService,
//     private traderService: TraderService,
//     private contactorService: ContactorService,
//     private crmSalesOpportunityService: CrmSalesOpportunityService,
//     private generalMasterService: GeneralMasterService,
//     private crmProjectService: CrmProjectService,
//     private userMasterService: UserMasterService,
//     private salesOrderService: SalesOrderService,
//     private router: Router,
//     private crmContracService: CrmContractService,
//     private crmContractDetailService: CrmContractDetailService,
//     private crmSalesTargetDetailService: CrmSalesTargetDetailService,
//     private notificationService: NotificationService
//   ) {
//     super(userService);
//   }

//   ngOnInit() {
//     this.userLogin = this.userService.getUserInfo();
//     this.years = CommonFunction.generateYear(-10, 0).reverse();
//     this.year = new Date().getFullYear();
//     this.getCustomer().then(data =>{ this.customer.push(...data)});
//     this.getSaleOpt().then(data =>{ this.salesOpt.push(...data)});
//     this.getListRecentlyActivity(0,30);
//     this.getListRecentlyOrder();
//     this.loadChart();
//   }
//   getCustomer() {
//     return this.traderService.ShortList(this.userLogin.company_id)
//   }
//   getSaleOpt() {
//     return this.crmSalesOpportunityService.ShortList(this.userLogin.company_id)
//   }
//   getListContracts() {
//     return this.crmContracService.ListFromAMStatistics(this.userLogin.company_id, 0, 0, '');
//   }

//   getListRecentlyActivity(skip: number = 0, take: number = 0) {
//     this.crmDashboardService.ListActivityBoard(
//       this.userLogin.company_id
//       , skip
//       , take
//       , ''
//       , ''
//       , ''
//       , this.year
//       , 0
//     ).then(data => {
//       this.recentlyActivity.push(...data);
//       this.ShowActivityLoadingCenter = false;
//       this.indexExpand = -1;
//       this.subStringContent();
//     });
//   }
//   getListRecentlyOrder() {
//     this.salesOrderService.ShortList(this.userLogin.company_id, '', 10).then(data => {
//       data.forEach((dt, index) => {
//         this.customer.forEach(cus => {
//           if (dt.customer_contactor_id == cus.trader_id) dt.account = cus.trader_local_nm;
//         });
//         if (index == data.length - 1) {
//           this.ShowOrderLoadingCenter = false;
//           this.recentlyOrder = data;
//         }
//       });
//     })
//   }
//   subStringContent() {
//     $(document).ready(function () {
//       $(".content-title").text(function (index, currentText) {
//         var maxLength = $(this).attr('data-maxlength');
//         if (currentText.length >= maxLength) {
//           return currentText.substr(0, maxLength) + "...";
//         } else {
//           return currentText
//         }
//       });
//     })
//   }
//   Expand(index) {
//     this.indexExpand == index ? this.indexExpand = -1 : this.indexExpand = index;
//     this.subStringContent();
//   }
//   collapse() {
//     this.indexExpand = -1;
//     this.subStringContent();
//   }
//   goToComponent(component: string, id: number) {
//     switch (component) {
//       case "1":
//         this.router.navigate([`customer-detail/${id}`]);
//         break;
//       case "2":
//         this.router.navigate([`contactor-detail/${id}`]);
//         break;
//       case "3":
//         this.router.navigate([`opportunity-detail/${id}`]);
//         break;
//       case "4":
//         this.router.navigate([`project-detail/${id}`]);
//         break;
//         default: this.router.navigate([`crm-sales-order-detail/${id}`]);
//         break;
//     }
//   }
//   onRefresh(type: string) {
//     switch (type) {
//       case 'activity':
//         this.ShowActivityLoadingCenter = true;
//         this.getListRecentlyActivity(0,30);
//         break;
//       case 'order':
//         this.ShowOrderLoadingCenter = true;
//         this.getListRecentlyOrder();
//         break;
//       case 'sales':
//         this.ShowQuarterlySalesLoading = true;
//         this.intItQuarterSales(this.listQuarterSales, this.listQuarterSaleTarget);
//         break;
//       case 'financial':
//         this.ShowFixedFinancialLoadingCenter = true;
//         this.intItFixedFinancialStatistics(this.SalesStatistics, this.PurchaseStatistics, this.ProfitStatistics);
//         break;
//       default:
//         break;
//     }
//   }
//   sumSalesStatus(data) {
//     data.forEach(dt => {
//       this.SumSaleStatus += dt.contract_amt;
//     });
//   }
//   getSaleStatus() {
//     return this.crmContracService.ShortList(this.userLogin.company_id, true, this.year);
//   }
//   loadChart() {
//     Promise.all([this.getSaleStatus(), this.getSumGrossProfit(), this.getSaleTargetDetail()]).then(([salest, gross, saleTarget]) => {
//       this.getDBSaleStatus(salest);
//       this.sumSalesStatus(salest);
//       Promise.all([this.groupToQuarter(salest, 'sales'), this.groupToQuarter(saleTarget, 'saletarget')]).then(([quarterSale, quarterTarget]) => {
//         this.listQuarterSales = quarterSale;
//         this.listQuarterSaleTarget = quarterTarget;
//         this.intItQuarterSales(quarterSale, quarterTarget);
//       });
//     });
//     Promise.all([this.getNowSalesTrend(), this.getLastSalesTrend()]).then(([nowSale, lastSale]) => {
//       Promise.all([this.groupSalesTrend(nowSale, this.year), this.groupSalesTrend(lastSale, this.year - 1)]).then(([thisYear, lastYear]) => {
//         this.inItSalesTrend(thisYear, lastYear);
//       });
//     });
//     Promise.all([this.getListContracts()]).then(([listContract]) => {
//       this.listContract = listContract;
//       this.handleFixedFinancial(this.year);
//       this.getDBOrderStatic()
//     });
//   }
//   getDBSaleStatus(listSaleStatus) {
//     this.crmContractDetailService.ShortList(this.userLogin.company_id, this.year).then(data => {
//       this.listSaleStatus = listSaleStatus;
//       this.listContractDetail = data;
//       this.getDBGrossProfit(data);
//       listSaleStatus.forEach((sale, index) => {
//         data.forEach(dt => {
//           if (sale.contract_id == dt.contract_id) {
//             this.DBSaleStatus_amt += dt.issue_amt;
//           }
//         });
//         if (index == listSaleStatus.length - 1) {
//           this.saleStatusRate = parseFloat((this.DBSaleStatus_amt / this.SumSaleStatus * 100).toFixed(1));
//           this.ShowStatus1LoadingCenter = false;
//         }
//       });
//     })
//   }
//   getSumGrossProfit() {
//     this.crmContracService.ShortList(this.userLogin.company_id, false, this.year).then(data => {
//       data.forEach(dt => {
//         this.SumGrossProfit += dt.contract_amt;
//       });
//       this.listGrossProfit = data;
//     });
//   }
//   getDBGrossProfit(listContractDetail) {
//     this.listContractDetail = listContractDetail;
//     this.listGrossProfit.forEach((gross, index) => {
//       this.listContractDetail.forEach(dt => {
//         if (gross.contract_id == dt.contract_id) {
//           this.DBGrossProfit_amt += dt.issue_amt;
//         }
//       });
//       if (index == this.listGrossProfit.length - 1) {
//         this.GrossProfitRate = parseFloat((this.DBGrossProfit_amt / this.SumGrossProfit * 100).toFixed(1));
//         this.ShowStatus2LoadingCenter = false;
//       }
//     });
//   }
//   getNowSalesTrend() {
//     return this.crmContractDetailService.ShortList(this.userLogin.company_id, 0, 0, this.year)
//   }
//   getLastSalesTrend() {
//     return this.crmContractDetailService.ShortList(this.userLogin.company_id, 0, 0, this.year - 1)
//   }

//   groupSalesTrend(listData, year) {
//     let arr = [];
//     for (let i = 1; i <= 12; i++) {
//       let date = i < 10 ? '0' + i : i.toString();
//       arr.push({ group: date, data: 0, label: year + '-' + date });

//       if (i == 12) {
//         var groups = listData.reduce(function (r, o) {
//           var m = o.issue_ymd.split(('-'))[1];

//           if (r[m]) {
//             r[m].data = parseInt(r[m].data) + parseInt(o.issue_amt);
//           } else {
//             r[m] = { group: m, data: o.issue_amt, label: year + '-' + m };
//           }
//           return r;
//         }, {});

//         var result = Object.keys(groups).map(function (k) { return groups[k]; });
//         arr.forEach((_arr, index) => {
//           result.forEach(res => {
//             var m1 = _arr.group.split(('-'))[0];
//             var m2 = res.group.split(('-'))[0];
//             if (m1 == m2) {
//               arr[index] = res;
//             }
//           });
//         });
//         // console.log("arr",arr);
//         return arr;
//       }
//     }

//   }

//   getDBOrderStatic() {
//     let contracts_total = this.listContract.filter(x => x.src_order_type == true && x.contract_start_ymd.split('-')[0] == this.year);
//     let opportunities_total = this.salesOpt.filter(x => x.created_time.split('-')[0] == this.year);
//     let ratio_check = opportunities_total.length > 0 ? contracts_total.length * 100 / opportunities_total.length : 0
//     let OrderRate = parseFloat(ratio_check.toString()).toFixed(1)
//     this.Order_ratio = { contracts_total: contracts_total, opportunities_total: opportunities_total, OrderRate: OrderRate }
//     this.ShowStatus4LoadingCenter = false;
//   }

//   inItSalesTrend(thisYear, lastYear) {
//     am4core.useTheme(am4themes_animated);
//     let chart = am4core.create("chartdiv", am4charts.XYChart);

//     // Enable chart cursor
//     chart.cursor = new am4charts.XYCursor();
//     chart.cursor.lineX.disabled = true;
//     chart.cursor.lineY.disabled = true;

//     // Create axes
//     let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
//     dateAxis.renderer.grid.template.location = 0.5;
//     dateAxis.dateFormatter.inputDateFormat = "yyyy-MM";
//     dateAxis.renderer.minGridDistance = 40;
//     // dateAxis.tooltipDateFormat = "MMM dd, yyyy";
//     dateAxis.dateFormats.setKey("day", "dd");
//     dateAxis.renderer.labels.template.disabled = true;
//     dateAxis.renderer.grid.template.disabled = true;

//     var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
//     valueAxis.renderer.labels.template.disabled = true;
//     valueAxis.renderer.grid.template.disabled = true;

//     function createLine(data) {
//       // Create series
//       var series = chart.series.push(new am4charts.LineSeries());
//       series.dataFields.valueY = "data";
//       series.dataFields.dateX = "group";
//       series.stroke = am4core.color("#fbbd08");
//       series.strokeWidth = 2;
//       series.strokeOpacity = 1;
//       series.strokeDasharray = "0"
//       series.data = data;
//       series.tooltipText = "{label.formatDate('yyyy-MM')} sales: {valueY}[/]";
//       var bullet = series.bullets.push(new am4charts.Bullet());
//       // let circle = bullet.createChild(am4core.Circle);
//       // circle.width = 1;
//       // circle.height = 1;
//       bullet.strokeWidth = 10;
//       bullet.stroke = am4core.color("#fff");
//       // bullet.setStateOnChildren = true;
//       bullet.propertyFields.fillOpacity = "opacity";
//       bullet.propertyFields.strokeOpacity = "opacity";
//     };
//     createLine(thisYear);
//     createLine(lastYear);
//     $("#chartdiv").find("[aria-labelledby]").hide();
//     this.ShowStatus3LoadingCenter = false;
//   }

//   groupToQuarter(data, component: string = '') {
//     if (data.length <= 0) {
//       let lb = component === 'saletarget' ? 'target' : 'sales';
//       return [{ group: "quarter1", data: 0, label: lb },
//       { group: "quarter2", data: 0, label: lb },
//       { group: "quarter3", data: 0, label: lb },
//       { group: "quarter4", data: 0, label: lb }];
//     }
//     else {
//       var groups = data.reduce(function (r, o) {
//         var m = 0;
//         var amt = 0;
//         var lb: string = '';
//         if (component === 'saletarget') {
//           m = o.month;
//           amt = o.monthly_target_amt;
//           lb = 'target';
//         }
//         else {
//           m = o.contract_start_ymd.split(('-'))[1];
//           amt = o.contract_amt;
//           lb = 'sales';
//         }
//         m = Number(m);
//         if (m < 4)
//           m = 0;
//         else if (m < 7)
//           m = 1;
//         else if (m < 10)
//           m = 2;
//         else if (m < 13)
//           m = 3;

//         if (r[m]) {
//           r[m].data = parseInt(r[m].data) + amt;
//         } else {
//           if (m == 0)
//             r[m] = { group: "quarter1", data: amt, label: lb };
//           else if (m == 1)
//             r[m] = { group: "quarter2", data: amt, label: lb };
//           else if (m == 2)
//             r[m] = { group: "quarter3", data: amt, label: lb };
//           else if (m == 3)
//             r[m] = { group: "quarter4", data: amt, label: lb };
//         }
//         return r;
//       }, {});

//       var result = Object.keys(groups).map(function (k) { return groups[k]; });
//       if (component === 'saletarget') {
//         result.forEach(rs => {
//           switch (rs.group) {
//             case 'quarter2':
//               rs.data += result[0].data;
//               break;
//             case 'quarter3':
//               rs.data += result[1].data;
//               break;
//             case 'quarter4':
//               rs.data += result[2].data;
//               break;
//             default:
//               break;
//           }
//         });
//       } else {
//         for (let i = result.length; i < 5; i++) {
//           result[i] = { group: `quarter${i + 1}`, data: 0, label: 'sales' };
//           if (i == 4) {
//             result.forEach(rs => {
//               switch (rs.group) {
//                 case 'quarter2':
//                   rs.data += result[0].data;
//                   break;
//                 case 'quarter3':
//                   rs.data += result[1].data;
//                   break;
//                 case 'quarter4':
//                   rs.data += result[2].data;
//                   break;
//                 default:
//                   break;
//               }
//             });
//           }
//         }

//       }
//       // console.log("result", result);
//       return result;
//     }
//   }
//   getSaleTargetDetail() {
//     return this.crmSalesTargetDetailService.ListSalesTargetDetail(this.userLogin.company_id, this.year)
//   }
//   intItQuarterSales(listQuarterSales, listQuarterSaleTarget) {
//     console.log("",listQuarterSales)
//     var chart = am4core.create("quarterSales", am4charts.XYChart);
//     // Create axes
//     var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
//     categoryAxis.dataFields.category = "group";
//     // categoryAxis.renderer.grid.template.location = 0;
//     categoryAxis.renderer.grid.template.disabled = true;
//     categoryAxis.renderer.minGridDistance = 30;
//     categoryAxis.renderer.cellStartLocation = 0.1
//     categoryAxis.renderer.cellEndLocation = 0.9
//     chart.legend = new am4charts.Legend();
//     // chart.legend.position='top'
//     chart.zoomOutButton.disabled = true;
//     function createLine(data, color, name) {
//       var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
//       // valueAxis.renderer.grid.template.disabled = true;
//       // Create series
//       var series = chart.series.push(new am4charts.ColumnSeries());
//       series.dataFields.valueY = "data";
//       series.dataFields.categoryX = "group";
//       series.fill = am4core.color(color);
//       series.stroke = am4core.color(color);
//       series.name = name;
//       series.columns.template.tooltipText = "{categoryX}\n [bold]{label}: {valueY}[/]";
//       series.columns.template.fillOpacity = .8;
//       series.data = data;
//       var columnTemplate = series.columns.template;
//       columnTemplate.strokeWidth = 2;
//       columnTemplate.strokeOpacity = 1;
//       columnTemplate.width = am4core.percent(85);
//       // Add data
//       chart.data = data;
//     };

//     createLine(listQuarterSales, '#3B83C0', 'Sales');
//     createLine(listQuarterSaleTarget, '#D95C5C', 'Target');
//     $("#quarterSales").find("[aria-labelledby]").hide();
//     this.ShowQuarterlySalesLoading = false;
//   }

//   defineColumnFixedFinancial() {
//     for (let i = 1; i < 13; i++) {
//       this.SalesStatistics.push({ 'group': i, 'data': 0, label: 'Definite Sales' })
//       this.PurchaseStatistics.push({ 'group': i, 'data': 0, label: 'Definite Purchase' })
//       this.ProfitStatistics.push({ 'group': i, 'data': 0, label: 'Profit' })
//     }
//   }
//   handleFixedFinancial(year) {
//     this.crmContractDetailService.ShortList(this.userLogin.company_id, 0, 0, year).then(data => {
//       this.defineColumnFixedFinancial();
//       this.addMoreInfo(data, this.SalesStatistics, this.PurchaseStatistics, this.ProfitStatistics)
//     });
//   }
//   addMoreInfo(data, SalesStatistics, PurchaseStatistics, ProfitStatistics) {
//     if (!data.length) {
//       return this.intItFixedFinancialStatistics(null, null, null);
//     }
//     data.forEach((dt, index) => {
//       this.listContract.forEach(parent => {
//         if (dt.contract_id == parent.contract_id) {
//           dt.src_order_type = parent.src_order_type;
//           if (!parent.src_order_type) {
//             dt.issue_amt = -dt.issue_amt;
//           }
//         }
//       });
//       if (index == data.length - 1) {
//         data.forEach(o => {
//           for (let i = 1; i <= 12; i++) {
//             if (o.issue_amt !== 0) {
//               if (i == o.issue_month) {
//                 if (o.src_order_type)
//                   SalesStatistics[i - 1].data += +o.issue_amt;
//                 else
//                   PurchaseStatistics[i - 1].data += -o.issue_amt;
//                 ProfitStatistics[i - 1].data = SalesStatistics[i - 1].data - PurchaseStatistics[i - 1].data
//               }
//             }
//           }
//         });
//         this.intItFixedFinancialStatistics(this.SalesStatistics, this.PurchaseStatistics, this.ProfitStatistics)
//       }
//     });

//   }
//   intItFixedFinancialStatistics(SalesStatistics, PurchaseStatistics, ProfitStatistics) {
//     am4core.useTheme(am4themes_animated);
//     var chart = am4core.create("FixedFinancialstatistics", am4charts.XYChart);
//     // Create axes
//     var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
//     categoryAxis.dataFields.category = "group";
//     categoryAxis.renderer.minGridDistance = 30;
//     categoryAxis.renderer.cellStartLocation = 0.1
//     categoryAxis.renderer.cellEndLocation = 0.9
//     chart.legend = new am4charts.Legend();
//     chart.zoomOutButton.disabled = true;
//     function createLine(data, name) {
//       var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
//       // Create series
//       var series = chart.series.push(new am4charts.ColumnSeries());
//       series.dataFields.valueY = "data";
//       series.dataFields.categoryX = "group";
//       series.name = name;
//       series.columns.template.tooltipText = "[bold]Month[/]: {categoryX}\n {label}: [bold]{valueY}[/]";
//       series.columns.template.fillOpacity = .8;
//       series.data = data;
//       var columnTemplate = series.columns.template;
//       columnTemplate.width = am4core.percent(85);
//       chart.data = data;
//     };
//     chart.colors.list = [
//       am4core.color('#2185d0'),
//       am4core.color('#db2828'),
//       am4core.color('#fbbd08')
//     ]
//     if (SalesStatistics || PurchaseStatistics || ProfitStatistics) {
//       createLine(SalesStatistics, 'Definite Sales');
//       createLine(PurchaseStatistics, 'Definite Purchase');
//       createLine(ProfitStatistics, 'Profit');
//     }
//     $("#FixedFinancialstatistics").find("[aria-labelledby]").hide();
//     this.ShowFixedFinancialLoadingCenter = false;
//   }
//   resetData() {
//     this.SumSaleStatus = 0;
//     this.DBSaleStatus_amt = 0;
//     this.saleStatusRate = 0;
//     this.SumGrossProfit = 0;
//     this.DBGrossProfit_amt = 0;
//     this.GrossProfitRate = 0;
//     this.SalesStatistics = [];
//     this.PurchaseStatistics = [];
//     this.ProfitStatistics = [];
//   }
//   onYearChange() {
//     if (this.year != this.yearBK) {
//       this.resetData();
//       this.loadChart();
//       this.yearBK = this.year;
//     }
//   }
// }

