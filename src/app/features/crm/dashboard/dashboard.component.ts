import { Component, OnInit } from '@angular/core';
import { CrmDashboardService } from '@app/core/services/crm/dashboard.service';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService, NotificationService } from '@app/core/services';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { Category } from '@app/core/common/static.enum';
import { CrmProjectService } from '@app/core/services/crm/project.service';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { Router } from '@angular/router';
import { CommonFunction } from '@app/core/common/common-function';
import { CrmContractDetailService, CrmContractService } from '@app/core/services/crm/contract.service';
import { CrmSalesTargetDetailService } from '@app/core/services/crm/sales-target.service';

// @ts-ignore
import * as am4core from "@amcharts/amcharts4/core";
// @ts-ignore
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
// import am4lang_lt_LT from "@amcharts/amcharts4/lang/lt_LT";

@Component({
  selector: 'sa-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css', '../common-activity/common-activity.css']
})
export class DashboardComponent extends BasePage implements OnInit {
  recentlyActivity: any[] = [];
  recentlyOrder: any[] = [];
  userLogin: any;
  indexExpand: number = -1;
  customer: any[] = [];

  ShowLoading = {
    status1 : true,
    status2 : true,
    status3 : true,
    status4 : true,
    recentActivity: true,
    recentOrder : true,
    quarterlySale: true,
    fixedFinancial: true
  };
  years: any[] = [];
  year: number = 0;
  yearBK: number = 0;
  listSaleStatus: any[] = [];
  DBSaleStatus_amt: number = 0;
  SumSaleStatus: number = 0;
  saleStatusRate: number = 0;
  listContract: any[] = [];
  listContractDetail: any[] = [];

  listGrossProfit: any[] = [];
  DBGrossProfit_amt: number = 0;
  SumGrossProfit: number = 0;
  GrossProfitRate: number = 0;
  Order_ratio: any = {};

  listQuarterSaleTarget: any[] = [];
  listQuarterSales: any[] = [];

  SalesStatistics: any[] = [];
  PurchaseStatistics: any[] = [];
  ProfitStatistics: any[] = [];

  fixedfinancialData:any[] = [];
  SalesTrendData:any[] = [];
  constructor(
    private crmDashboardService: CrmDashboardService,
    public userService: AuthService,
    private salesOrderService: SalesOrderService,
    private router: Router,
    private notificationService: NotificationService,
    private traderService: TraderService
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.year = new Date().getFullYear();
    this.getCustomer().then(data =>{ this.customer.push(...data)});
    this.getListRecentlyActivity(0,30);
    this.getListRecentlyOrder();
    this.loadDATA(this.year);
  }
  loadDATA(year){
    this.getSalesStatus(year).then(data =>{
      this.ShowLoading.status1 = false;
      this.DBSaleStatus_amt = data.sales_amount;
      this.saleStatusRate = data.sales_percent;
    })
    this.getSalesStatus(year,false).then(data =>{
      this.ShowLoading.status2 = false;
      this.DBGrossProfit_amt =data.sales_amount;
      this.GrossProfitRate = data.sales_percent;
    })
    Promise.all([this.getSalesTrendThisYear(year),this.getSalesTrendLastYear(year)]).then(([thisYear,lastYear]) =>{
      this.inItSalesTrend(thisYear,lastYear);
      this.ShowLoading.status3 = false;
    })
    this.getOrderStatistic(year).then(data =>{
      this.ShowLoading.status4 = false;
      this.Order_ratio = data;
    })
    Promise.all([this.getListSales(year),this.getListSalestarget(year)]).then(([quarterSale, quarterTarget]) => {
      this.listQuarterSales = quarterSale;
      this.listQuarterSaleTarget = quarterTarget;
      this.intItQuarterSales(quarterSale, quarterTarget);
      this.ShowLoading.quarterlySale = false;
    });
    this.getFixedFinancial(year).then(data => {
      this.fixedfinancialData = data;
      this.intItFixedFinancialStatistics(data);
      this.ShowLoading.fixedFinancial = false;
    });
  }
  getListSales(year){
    return this.crmDashboardService.ListSales(this.userLogin.company_id,year);
  }
  getListSalestarget(year){
    return this.crmDashboardService.ListSalestarget(this.userLogin.company_id,year);
  }
  getFixedFinancial(year){
    return this.crmDashboardService.getFixedFinancial(this.userLogin.company_id,year);
  }
  getSalesStatus(year,orderType:boolean = true){
    return this.crmDashboardService.getSalesStatus(this.userLogin.company_id,year,orderType);
  }
  getSalesTrendThisYear(year){
    return this.crmDashboardService.getSalesTrend(this.userLogin.company_id,year);
  }
  getSalesTrendLastYear(year){
     return this.crmDashboardService.getSalesTrend(this.userLogin.company_id,year-1);
  }
  getOrderStatistic(year){
    return this.crmDashboardService.getOrderStatistic(this.userLogin.company_id,year);
  }
  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id)
  }

  getListRecentlyActivity(skip: number = 0, take: number = 0) {
    this.crmDashboardService.ListActivityBoard(
      this.userLogin.company_id
      , skip
      , take
      , ''
      , ''
      , ''
      , this.year
      , 0
    ).then(data => {
      this.recentlyActivity.push(...data);
      this.ShowLoading.recentActivity = false;
      this.indexExpand = -1;
      this.subStringContent();
    });
  }
  getListRecentlyOrder() {
    this.salesOrderService.ShortList(this.userLogin.company_id, '', 10).then(data => {
      if (data.length <= 0) {
        return this.ShowLoading.recentOrder = false;
      }
      data.forEach((dt, index) => {
        this.customer.forEach(cus => {
          if (dt.customer_contactor_id == cus.trader_id) dt.account = cus.trader_local_nm;
        });
        if (index == data.length - 1) {
          this.ShowLoading.recentOrder = false;
          this.recentlyOrder = data;
        }
      });
    })
  }
  subStringContent() {
    $(document).ready(function () {
      $(".content-title").text(function (index, currentText) {
        var maxLength = $(this).attr('data-maxlength');
        if (currentText.length >= maxLength) {
          return currentText.substr(0, maxLength) + "...";
        } else {
          return currentText
        }
      });
    })
  }
  Expand(index) {
    this.indexExpand == index ? this.indexExpand = -1 : this.indexExpand = index;
    this.subStringContent();
  }
  collapse() {
    this.indexExpand = -1;
    this.subStringContent();
  }
  goToComponent(component: string, id: number) {
    switch (component) {
      case "1":
        this.router.navigate([`customer-detail/${id}`]);
        break;
      case "2":
        this.router.navigate([`contactor-detail/${id}`]);
        break;
      case "3":
        this.router.navigate([`opportunity-detail/${id}`]);
        break;
      case "4":
        this.router.navigate([`project-detail/${id}`]);
        break;
        default: this.router.navigate([`crm-sales-order-detail/${id}`]);
        break;
    }
  }
  onRefresh(type: string) {
    switch (type) {
      case 'activity':
        this.ShowLoading.recentActivity = true;
        this.getListRecentlyActivity(0,30);
        break;
      case 'order':
        this.ShowLoading.recentOrder = true;
        this.getListRecentlyOrder();
        break;
      case 'sales':
        this.ShowLoading.quarterlySale = true;
        this.intItQuarterSales(this.listQuarterSales, this.listQuarterSaleTarget);
        break;
      case 'financial':
        this.ShowLoading.fixedFinancial = true;
        this.intItFixedFinancialStatistics(this.fixedfinancialData);
        break;
      default:
        break;
    }
  }
  inItSalesTrend(thisYear, lastYear) {
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create("chartdiv", am4charts.XYChart);

    // Enable chart cursor
    chart.cursor = new am4charts.XYCursor();
    chart.cursor.lineX.disabled = true;
    chart.cursor.lineY.disabled = true;

    // Create axes
    let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
    dateAxis.cursorTooltipEnabled = false;
    dateAxis.renderer.grid.template.location = 0.5;
    // dateAxis.dateFormatter.inputDateFormat = "yyyy-MM";
    dateAxis.renderer.minGridDistance = 40;
    // dateAxis.tooltipDateFormat = "MMM dd, yyyy";
    // dateAxis.dateFormats.setKey("day", "dd");
    dateAxis.renderer.labels.template.disabled = true;
    dateAxis.renderer.grid.template.disabled = true;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.cursorTooltipEnabled = false;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.renderer.grid.template.disabled = true;

    function createLine(data) {
      // Create series
      var series = chart.series.push(new am4charts.LineSeries());
      series.dataFields.valueY = "amount";
      series.dataFields.dateX = "bymonth";
      series.stroke = am4core.color("#fbbd08");
      series.strokeWidth = 2;
      series.strokeOpacity = 1;
      series.strokeDasharray = "0"
      series.data = data;
      series.tooltipText = "{label} [bold]sales: {valueY}[/]";
      var bullet = series.bullets.push(new am4charts.Bullet());
      // let circle = bullet.createChild(am4core.Circle);
      // circle.width = 1;
      // circle.height = 1;
      bullet.strokeWidth = 10;
      bullet.stroke = am4core.color("#fff");
      // bullet.setStateOnChildren = true;
      bullet.propertyFields.fillOpacity = "opacity";
      bullet.propertyFields.strokeOpacity = "opacity";
    };
    createLine(thisYear);
    createLine(lastYear);
    $("#chartdiv").find("[aria-labelledby]").hide();
    this.ShowLoading.status3 = false;
  }
  intItQuarterSales(listQuarterSales, listQuarterSaleTarget) {
    var chart = am4core.create("quarterSales", am4charts.XYChart);

    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "group";
    categoryAxis.renderer.grid.template.disabled = true;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.cellStartLocation = 0.1
    categoryAxis.renderer.cellEndLocation = 0.9
    chart.legend = new am4charts.Legend();
    chart.zoomOutButton.disabled = true;
    function createLine(data, color, name) {
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

      var series = chart.series.push(new am4charts.ColumnSeries());
      series.dataFields.valueY = "data";
      series.dataFields.categoryX = "group";
      series.fill = am4core.color(color);
      series.stroke = am4core.color(color);
      series.name = name;
      series.columns.template.tooltipText = "{categoryX}\n [bold]{label}: {valueY}[/]";
      series.columns.template.fillOpacity = .8;
      series.data = data;
      var columnTemplate = series.columns.template;
      columnTemplate.strokeWidth = 2;
      columnTemplate.strokeOpacity = 1;
      columnTemplate.width = am4core.percent(85);
      // Add data
      chart.data = data;
    };

    setTimeout(() => {
      createLine(listQuarterSales, '#3B83C0', 'Sales');
    createLine(listQuarterSaleTarget, '#D95C5C', 'Target');
    }, 300);
    $("#quarterSales").find("[aria-labelledby]").hide();
    this.ShowLoading.quarterlySale = false;
  }

  intItFixedFinancialStatistics(data) {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("FixedFinancialstatistics", am4charts.XYChart);
    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "bymonth";
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.renderer.cellStartLocation = 0.1
    categoryAxis.renderer.cellEndLocation = 0.9
    chart.legend = new am4charts.Legend();
    chart.zoomOutButton.disabled = true;
    function createLine(data, name) {
      var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
      // Create series
      var series = chart.series.push(new am4charts.ColumnSeries());
      switch (name) {
        case 'Definite Sales':
          series.dataFields.valueY = "sales";
          break;
        case 'Definite Purchase':
            series.dataFields.valueY = "purchase";
            break;
        default:series.dataFields.valueY = "profit";
          break;
      }
      
      series.dataFields.categoryX = "bymonth";
      series.name = name;
      series.columns.template.tooltipText = "[bold]Month[/]: {categoryX}\n {name}: [bold]{valueY}[/]";
      series.columns.template.fillOpacity = .8;
      series.data = data;
      var columnTemplate = series.columns.template;
      columnTemplate.width = am4core.percent(85);
      chart.data = data;
    };
    chart.colors.list = [
      am4core.color('#2185d0'),
      am4core.color('#db2828'),
      am4core.color('#fbbd08')
    ]
    if (data.length > 0) {
      createLine(data, 'Definite Sales');
      createLine(data, 'Definite Purchase');
      createLine(data, 'Profit');
    }
    $("#FixedFinancialstatistics").find("[aria-labelledby]").hide();
    this.ShowLoading.fixedFinancial = false;
  }
  
  resetData() {
    // this.DBSaleStatus_amt = 0;
    // this.saleStatusRate = 0;
    // this.DBGrossProfit_amt = 0;
    // this.GrossProfitRate = 0;
    // this.Order_ratio = {};
    this.ShowLoading = {
      status1 : true,
      status2 : true,
      status3 : true,
      status4 : true,
      recentActivity: false,
      recentOrder : false,
      quarterlySale: true,
      fixedFinancial: true
    };
  }
  onYearChange() {
    if (this.year != this.yearBK) {
      this.resetData();
      this.loadDATA(this.year)
      this.yearBK = this.year;
    }
  }
}

