import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AuthService, UserMasterService, NotificationService, ProgramService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { BasePage } from '@app/core/common/base-page';
import { Router } from '@angular/router';
import { CrmProjectService } from '@app/core/services/crm/project.service';
import { CrmProjectModel } from '@app/core/models/crm/project.model';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { CrmSalesOpportunityModel } from '@app/core/models/crm/sales-opportunity.model';
import { Category } from '@app/core/common/static.enum';
import { ProgramModel } from '@app/core/models/program.model';
import { config } from '@app/core/smartadmin.config';


@Component({
  selector: 'sa-project-master',
  templateUrl: './project-master.component.html',
  styleUrls: ['./project-master.component.css']
})
export class ProjectMasterComponent extends BasePage implements OnInit {
  @ViewChild("popupProject") popupProject;
  projectInfor: CrmProjectModel[] = [];
  modalRef: BsModalRef;
  userLogin: any;
  opportunities: CrmSalesOpportunityModel[] = [];
  projectType: any[] = [];
  workUnit: any[] = [];
  options: any;
  openedPrograms: Array<ProgramModel> = [];
  constructor(
    public userService: AuthService,
    public userMasterService: UserMasterService,
    private modalService: BsModalService,
    private generalMasterService: GeneralMasterService,
    private crmProjectService: CrmProjectService,
    private crmSalesOpportunityService: CrmSalesOpportunityService,
    private programService: ProgramService,
    private notification: NotificationService,
    public router: Router,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.initDatatable();
  }

  public search() {
    let name = $('#txtKeyword').val().toLowerCase();
    var tbl = $('.ProjectTbl').DataTable();
    tbl.search(name).draw();
  }
  openPopup() {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.modalRef = this.modalService.show(this.popupProject, config);
  }
  closePopup() {
    this.modalRef && this.modalRef.hide();
  }
  getOpportunityShortList() {
    return this.crmSalesOpportunityService.ShortList(this.userLogin.company_id);
  }
  getProjectType() {
    return this.generalMasterService.listGeneralByCate(Category.ProjectType.valueOf())
  }
  getWorkUnit() {
    return this.generalMasterService.listGeneralByCate(Category.ItemUnit.valueOf())
  }

  initDatatable() {
    this.options = {
      dom: "Bfrtip",
      ajax: (data, callback, settings) => {
        this.crmProjectService.ListProject(this.userLogin.company_id).then(Data => {
          if (!Data.length) {
            callback({
              aaData: data
            });
          }
          Promise.all([this.getOpportunityShortList(), this.getProjectType(), this.getWorkUnit()]).then(([OpportunityList, ProjectTypeList, WorkUnitList]) => {
            this.bindEventPoppup();
            this.opportunities = OpportunityList;
            this.projectType = ProjectTypeList;
            Data.forEach((data, index) => {
              OpportunityList.forEach(opp => {
                if (data.salesopt_id == opp.salesopt_id) {
                  data.salesopt_nm = opp.salesopt_nm
                }
              });
              ProjectTypeList.forEach(opp => {
                if (data.project_type_gen_cd == opp.gen_cd) {
                  data.project_type_nm = opp.gen_nm
                }
              });
              if (data.project_tasks.length > 0) {
                data.project_tasks.forEach(task => {
                  WorkUnitList.forEach(work => {
                    if (task.work_unit_gen_cd == work.gen_cd) {
                      task.work_unit_gen_nm = work.gen_nm;
                    }
                  });
                });
                data.sum_task = {};
                var total_hour = 0;
                var total_minute = 0;
                data.project_tasks.sort((a, b) => (a.work_unit_gen_cd > b.work_unit_gen_cd) ? 1 : ((b.work_unit_gen_cd > a.work_unit_gen_cd) ? -1 : 0));
                data.project_tasks.forEach(entry => {
                  if (entry.work_unit_gen_nm == "M/Hour" || entry.work_unit_gen_nm == "M/Day" || entry.work_unit_gen_nm == "M/Month" || entry.work_unit_gen_nm == "Per Year") {
                    if (!data.sum_task[entry.work_unit_gen_nm]) {
                      total_minute = 0;
                      total_hour = 0;
                    }
                    total_minute = total_minute + entry.work_minute;
                    total_hour += entry.work_hour;
                    if (total_minute >= 60) {
                      total_hour += Math.floor(total_minute / 60);
                      total_minute = total_minute % 60;
                    }
                    total_minute > 0 ? data.sum_task[entry.work_unit_gen_nm] = total_hour + "h " + total_minute + "m" : data.sum_task[entry.work_unit_gen_nm] = total_hour + "h ";
                  }

                });
              } else {
                data.sum_task = {};
              }
              if (index == Data.length - 1) {
                this.projectInfor = Data;
                callback({
                  aaData: this.projectInfor
                });
              }
            });
          });
        })
      },
      columns: [
        {
          data: (data) => {
            return `<a class="goto" data-projectId = "${data.project_id}">${data.project_nm}</a>`
          }, className: "", width: "150px"
        },
        { data: "project_type_nm", className: "", width: "20px" },
        { data: "salesopt_nm", className: "", width: "150px" },
        { data: "start_ymd", className: "", width: "50px" },
        { data: "end_ymd", className: "", width: "50px" },
        { data: "work_hours", className: "", width: "20px" },
        {
          data: (data) => {
            if (data.sum_task) {
              var _result = '';
              var oldStr = JSON.stringify(data.sum_task);
              var newStr = oldStr.replace(/{/g, "").replace(/}/g, "").replace(/"/g, "").split(",");
              newStr.forEach(z => {
                _result = _result + `<span class="label label-default">${z}</span> `
              });
              return _result;
            }
            else return "";
          }, className: "", width: "150px"
        },
        {
          data: (data) => {
            if (data.project_tasks.length > 0) {
              var a = data.project_tasks.sort((a, b) => (a.changed_time > b.changed_time) ? 1 : ((b.changed_time > a.changed_time) ? -1 : 0));
              return data.project_tasks[data.project_tasks.length - 1].changed_time
            }
            else return "";
          }, className: "", width: "50px"
        }
      ],
      scrollY: 210,
      scrollX: true,
      paging: true,
      pageLength: 25,

      buttons: [
        {
          text: '<i class="fa fa-refresh" title="Refresh"></i>',
          action: (e, dt, node, config) => {
            dt.ajax.reload();
          }
        },
        "copy",
        "excel",
        "pdf",
        "print"
      ]
    };
  }
  private reloadDatatable() {
    $(".ProjectTbl")
      .DataTable()
      .ajax.reload();
  }
  checkKeycode(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }
  bindEventPoppup() {
    let vm = this;
    setTimeout(() => {
      $('.goto').click(function () {
        let id = $(this).attr('data-projectId');
        var menu_url:string;
        var menu_id:number;
        var menu_name:string;
        menu_url = "/project-detail/" + id;
        menu_id = 9002;
        menu_name = "Project detail";
        
        if (!vm.programService.openedPrograms.filter(x => x.id == menu_id).length && vm.programService.openedPrograms.length == config.maxOpenedPrograms) {
          vm.notification.smartMessageBox({
            title: "Notification",
            content: `Maximum ${config.maxOpenedPrograms} programs can be opened!`,
            buttons: '[OK]'
          });
          return;
        }
        $('.center-loading').show();
        vm.programService.addOpenedPrograms(new ProgramModel(menu_id, menu_name, menu_url),true);

      });
    }, 500);
  }
}
