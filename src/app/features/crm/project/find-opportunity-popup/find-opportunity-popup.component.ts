import { Component, OnInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { NotificationService, AuthService } from '@app/core/services';
import { BsModalService } from 'ngx-bootstrap';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { BasePage } from '@app/core/common/base-page';
import { CrmSalesOpportunityModel } from '@app/core/models/crm/sales-opportunity.model';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';

@Component({
  selector: 'sa-find-opportunity-popup',
  templateUrl: './find-opportunity-popup.component.html',
  styleUrls: ['../project-master/project-master.component.css']
})
export class FindOpportunityPopupComponent extends BasePage implements OnInit {
  @Output() childCall = new EventEmitter();
  @Input() JSopportunities : string;
  userLogin : any;
  opportunities : any[] = [];
  keyWord : any;
  customer: any[] = [];
  contactor: any[] = [];
  opportunitiesBK: CrmSalesOpportunityModel[] = [];
  constructor(
    private notification: NotificationService,
    public userService: AuthService,
    private modalService: BsModalService,
    private crmSalesOpportunityService : CrmSalesOpportunityService,
    private traderService: TraderService,
    private contactorService: ContactorService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    var inItdata = this.getCustomer();
    inItdata.then((customer) =>{
      this.customer = customer;
      this.getAllSales();
    })
  }
  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id)
  }
  getAllSales(){
    this.opportunities = JSON.parse(this.JSopportunities)
    this.opportunities.forEach(opp => {
      this.customer.forEach(cus => {
        if (opp.customer_id == cus.trader_id) {
          opp.customer_nm = cus.trader_local_nm;
        }
        if (opp.contractor_id == cus.trader_id) {
          opp.contractor_nm = cus.trader_local_nm;
        }
      });
    });
    setTimeout(() => {
      this.opportunitiesBK = this.opportunities;
    }, 500);
  }
  search(){
    let keyword = this.keyWord.toLowerCase();
    if (!keyword) {
      // this.notification.showMessage("error", "Please enter a business name");
      // return;
      this.opportunities = this.opportunitiesBK
    }
    this.opportunities = this.opportunitiesBK.filter(
        opp => opp.salesopt_nm.toLowerCase().includes(keyword)||
        opp.customer_nm.toLowerCase().includes(keyword) ||
        opp.contractor_nm.toLowerCase().includes(keyword)
      );
  }
  selectOpportunity(item){
    this.childCall.emit(item);
  }

  checkKeycode(event){
    if (event.keyCode === 13) {
      this.search();
    }
  }
  closePopup(){
    this.childCall.emit();
  }
}
