import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivityBoardComponent } from './activity-board.component';

const routes: Routes = [{
  path: '',
  component: ActivityBoardComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActivityBoardRoutingModule { }
