import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, UserMasterService, NotificationService } from '@app/core/services';
import { CrmDashboardService } from '@app/core/services/crm/dashboard.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CrmProjectService } from '@app/core/services/crm/project.service';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { CrmContractService, CrmContractDetailService } from '@app/core/services/crm/contract.service';
import { CrmSalesTargetDetailService } from '@app/core/services/crm/sales-target.service';
import { Category } from '@app/core/common/static.enum';
import { CommonFunction } from '@app/core/common/common-function';

@Component({
  selector: 'sa-activity-board',
  templateUrl: './activity-board.component.html',
  styleUrls: ['./activity-board.component.css', '../dashboard/dashboard.component.css', '../common-activity/common-activity.css']
})
export class ActivityBoardComponent extends BasePage implements OnInit {
  userLogin: any;
  total_Count: number = 0;
  business_Count: number = 0;
  support_Count: number = 0;
  email_Count: number = 0;
  telephone_Count: number = 0;
  conference_Count: number = 0;
  issue_Count: number = 0;
  listActivity: any[] = [];
  listYear: any[] = [];
  listMonth: any[] = [];
  ShowActivityLoadingCenter: boolean = false;
  indexExpand: number = -1;
  userList: any[] = [];
  activityTypeList: any[] = [];
  searchKeyword: string = '';
  searchByCreator: string = '';
  searchByActivityType: string = '';
  searchByYear: number = new Date().getFullYear();
  searchByMonth: number = 0;
  searchBK: any = {};
  constructor(
    private crmDashboardService: CrmDashboardService,
    public userService: AuthService,
    private userMasterService: UserMasterService,
    private router: Router,
    private notificationService: NotificationService
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.resetData();
    this.getSytemUser().then(data => {
      this.userList = data;
    });
    this.searchByCreator = " ";
    this.activityTypeList = [
      { value: 'business', text: 'Business Status' },
      { value: 'support', text: 'Technical support' },
      { value: 'email', text: 'E-mail' },
      { value: 'tel', text: 'Telephone' },
      { value: 'issue', text: 'Issue' },
      { value: 'meeting', text: 'Conference' },
    ];
    this.searchByActivityType = " ";
    this.listYear = CommonFunction.generateYear(-10, 0).reverse();
    this.searchByYear = new Date().getFullYear();
    this.listMonth = this.genMonths();
    this.searchByMonth = 0;
    this.getActivityBoard(0, 10);
  }
  getSytemUser() {
    return this.userMasterService.listUsers()
  }
  genMonths() {
    let months: any = [];
    for (let i = 1; i < 13; i++) {
      let _value = i > 9 ? i : `0${i}`;
      months.push({ val: _value, text: new Date(2019, i - 1, 1).toLocaleString('en-us', { month: 'long' }) });
    }
    return months;
  }
  getActivityBoard(skip: number = 0, take: number = 0) {
    this.crmDashboardService.ListActivityBoard(
      this.userLogin.company_id
      , skip
      , take
      , this.searchKeyword
      , this.searchByCreator
      , this.searchByActivityType
      , this.searchByYear
      , this.searchByMonth
    ).then(data => {
      this.listActivity.push(...data);
      this.total_Count = +this.total_Count + data.length;
      for (let i = 0; i < data.length; i++) {
        switch (data[i].child_type) {
          case 'email':
            this.email_Count++;
            break;
          case 'tel':
            this.telephone_Count++;
            break;
          case 'meeting':
            this.conference_Count++;
            break;
          case 'issue':
            this.issue_Count++;
            break;
          case 'business':
            this.business_Count++;
            break;
          default: this.support_Count++;
            break;
        }
      }
      this.indexExpand = -1;
      this.subStringContent();
    });
  }

  subStringContent() {
    $(document).ready(function () {
      $(".content-title").text(function (index, currentText) {
        var maxLength = $(this).attr('data-maxlength');
        if (currentText.length >= maxLength) {
          return currentText.substr(0, maxLength) + "...";
        } else {
          return currentText
        }
      });
    })
  }
  Expand(index) {
    this.indexExpand == index ? this.indexExpand = -1 : this.indexExpand = index;
    this.subStringContent();
  }
  addMore() {
    this.getActivityBoard(this.listActivity.length, 10);
  }
  resetData() {
    this.total_Count = 0;
    this.email_Count = 0;
    this.telephone_Count = 0;
    this.conference_Count = 0;
    this.issue_Count = 0;
    this.business_Count = 0;
    this.support_Count = 0;
    this.searchBK={
      "searchKeyword":this.searchKeyword,
      "searchByCreator":this.searchByCreator,
      "searchByActivityType":this.searchByActivityType,
      "searchByYear":this.searchByYear,
      "searchByMonth":this.searchByMonth,
    }
  }
  search() {
    if (this.searchKeyword != this.searchBK.searchKeyword
      || this.searchByCreator != this.searchBK.searchByCreator
      || this.searchByActivityType != this.searchBK.searchByActivityType
      || this.searchByYear != this.searchBK.searchByYear
      || this.searchByMonth != this.searchBK.searchByMonth
    ) {
      this.resetData();
      this.listActivity = [];
      this.getActivityBoard(0, 10);
    }

  }
  checkKeycode(event) {
    if (event.keyCode === 13) {
      this.search();
    }
  }
  goToComponent(component: string, id: number) {
    switch (component) {
      case "1":
        this.router.navigate([`customer-detail/${id}`]);
        break;
      case "2":
        this.router.navigate([`contactor-detail/${id}`]);
        break;
      case "3":
        this.router.navigate([`opportunity-detail/${id}`]);
        break;
      case "4":
        this.router.navigate([`project-detail/${id}`]);
        break;
      default: this.notificationService.showError("not found");
        break;
    }
  }
}
