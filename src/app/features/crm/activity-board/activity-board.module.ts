import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ActivityBoardRoutingModule } from './activity-board-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { ActivityBoardComponent } from './activity-board.component';
import { MomentModule } from 'angular2-moment';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    ActivityBoardRoutingModule,
    MomentModule
  ],
  declarations: [ActivityBoardComponent]
})
export class ActivityBoardModule { }
