import { Component, OnInit } from '@angular/core';
import { ProgramService, NotificationService, LayoutService } from '@app/core/services';
import { config } from "@app/core/smartadmin.config";
import { ProgramModel } from '@app/core/models/program.model';
@Component({
  selector: 'sa-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class CrmHomeComponent implements OnInit {
  openedPrograms: Array<ProgramModel> = [];
  constructor(
    private programService: ProgramService,
    private notiService: NotificationService,
    private layoutService: LayoutService,
    ) { }

  ngOnInit() {

    this.programService.getOpenedPrograms
      .subscribe(
        (programs: Array<ProgramModel>) => {
          this.openedPrograms = programs;
          // console.log(programs);
        }
      );
    this.programService.refreshOpenedPrograms();

  }
  goto(step,el) {
    var menu_url:string;
    var menu_id:number;
    var menu_name:string;
    switch (step) {
      case 1:
        menu_url = "/register-organization";
        menu_id = 100005;
        menu_name = "Register Organization";
        break;
      case 2:
        menu_url = "/register-position";
        menu_id = 100006;
        menu_name = "Register Position";
        break;
      case 3:
        menu_url = "/register-authority-group";
        menu_id = 100008;
        menu_name = "Register Authority Group";
        break;
      case 4:
        menu_url = "/authority-group-menu-setting";
        menu_id = 100009;
        menu_name = "Authority Group Menu";
        break;
      case 5:
        menu_url = "/register-user";
        menu_id = 100007;
        menu_name = "Register User";
        break;
      case 6:
        menu_url = "/user-authority-group-setting";
        menu_id = 100010;
        menu_name = "User Menu Setting";
        break;
      case 7:
        menu_url = "/group-sharing-setting";
        menu_id = 100024;
        menu_name = "Data-sharing Group";
        break;
      default:
        break;
    }
    if (!this.programService.openedPrograms.filter(x => x.id == menu_id).length && this.programService.openedPrograms.length == config.maxOpenedPrograms) {
      this.notiService.smartMessageBox({
        title: "Notification",
        content: `Maximum ${config.maxOpenedPrograms} programs can be opened!`,
        buttons: '[OK]'
      });
      return;
    }
    $('.center-loading').show();
    // console.log(menu_id, menu_name, menu_url)
    this.programService.addOpenedPrograms(new ProgramModel(menu_id, menu_name, menu_url),true);

    // this.$menu.find("li.active").removeClass("active")
    // $(el.target).parent().addClass("active");

  }

 
}
