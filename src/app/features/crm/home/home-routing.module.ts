import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrmHomeComponent } from './home.component';

const routes: Routes = [{
  path: '',
  component: CrmHomeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrmHomeRoutingModule { }
