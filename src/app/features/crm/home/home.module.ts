import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmHomeRoutingModule } from './home-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { CrmHomeComponent } from './home.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    CrmHomeRoutingModule
  ],
  declarations: [CrmHomeComponent]
})
export class CrmHomeModule { }
