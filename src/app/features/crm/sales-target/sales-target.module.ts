import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SalesTargetRoutingModule } from './sales-target-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';
import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { SalesTargetComponent } from './sales-target.component';
import { NgxCurrencyModule } from 'ngx-currency';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SmartadminDatatableModule,
    SalesTargetRoutingModule,
    NgxCurrencyModule
  ],
  declarations: [SalesTargetComponent]
})
export class SalesTargetModule { }
