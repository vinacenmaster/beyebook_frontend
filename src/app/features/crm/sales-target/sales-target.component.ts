import { Component, OnInit } from '@angular/core';
import { CrmSalesTargetService } from '@app/core/services/crm/sales-target.service';
import { BasePage } from '@app/core/common/base-page';
import { AuthService, NotificationService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { CrmMasSalesTargetHeaderModel, CrmMasSalesTargetDetailModel } from '@app/core/models/crm/sales-target.model';
import { ProgramList } from '@app/core/common/static.enum';
import { CommonFunction } from '@app/core/common/common-function';
import { CrmMasServiceCategoryService } from '@app/core/services/crm/setting-item.service';
import { CrmMasServiceCategoryModel } from '@app/core/models/crm/setting-item.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'sa-sales-target',
  templateUrl: './sales-target.component.html',
  styleUrls: ['./sales-target.component.css']
})
export class SalesTargetComponent extends BasePage implements OnInit {
  salesTargetInfo: CrmMasSalesTargetHeaderModel[] = [];
  userLogin: any;
  years: any[] = [];
  year: any;
  yearBK: any;
  listService: any[] = [];
  listServiceFilter: any[] = [];
  listDetail: CrmMasSalesTargetDetailModel[] = [];
  sumYear: number = 0;
  sumMonth: number[] = [];
  constructor(
    private crmSalesTargetService: CrmSalesTargetService,
    private notification: NotificationService,
    public generalMasterService: GeneralMasterService,
    public userService: AuthService,
    public crmMasServiceCategoryService: CrmMasServiceCategoryService,
    private route: ActivatedRoute,
    public router: Router,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.checkPermission(ProgramList.Sales_Opportunity.valueOf());
    this.userLogin = this.userService.getUserInfo();
    this.years = CommonFunction.generateYear(-10, 0).reverse();
    this.year = new Date().getFullYear()
    this.getListServiceItem();
    setTimeout(() => {
      this.getSalesTarget();
    }, 100);
  }
  generateServiceName(){
    this.salesTargetInfo.forEach(item => {
      this.listServiceFilter.forEach(fil => {
        if (item.crm_item_id == fil.crm_service_cate_id) {
          this.listService.forEach(list => {
            if (fil.parent_cate_id == list.crm_service_cate_id) {
              item.parent_nm = list.crm_service_cate_nm;
              item.crm_service_cate_nm = fil.crm_service_cate_nm;
            }
          });
        }
      });
    });
  }
  getSalesTarget() {
    this.crmSalesTargetService.ListSalesTarget(this.userLogin.company_id, this.year).then(data => {
      this.sumMonth = [];
      this.salesTargetInfo = [];
      var _data = data.reverse();
      if (_data.length > 0) {
        setTimeout(() => {
          //check data changed from db
          if (_data.length != this.listServiceFilter.length) {
            this.generateSalesTarget();
            _data.forEach((dt,index) => {
              this.salesTargetInfo.forEach((sale,index) => {
                if (dt.crm_item_id ==sale.crm_item_id) {
                  this.salesTargetInfo[index] = _data[index];
                }
              });
              if (index == _data.length -1)
              this.generateServiceName();
            });
          }else{
            this.salesTargetInfo =_data;
            this.generateServiceName();
          }
          
          this.sumYear = this.sumYear_amt();
          for (let i = 1; i <= 12; i++) {
            this.sumMonth.push(this.sumMonth_amt(i))
          }
        }, 500);
      }
      else setTimeout(() => {
        this.generateSalesTarget(true);
      }, 500);
    });
    this.yearBK = this.year;
  }
  getListServiceItem() {
    this.crmMasServiceCategoryService.getList(this.loggedUser.company_id).then(data => {
      this.listService.push(...data)
      this.listServiceFilter = data.filter(x => x.sales_target_yn == true)
    });
  }

  addDetail(crmId) {
    this.listDetail = [];
    for (let i = 1; i <= 12; i++) {
      this.listDetail.push(
        {
          company_id: this.loggedUser.company_id,
          crm_item_id: crmId,
          year: this.year,
          month: i,
          monthly_target_amt: 0,
          use_yn: true,
          remark: '',
          creator: '',
          changer: '',
          created_time: '',
          changed_time: '',
        }
      )
    }
  }

  generateSalesTarget(generateMonth:boolean=false) {
    this.listServiceFilter.forEach(list => {
      this.listService.forEach(dt => {
        if (list.parent_cate_id == dt.crm_service_cate_id) {
          this.addDetail(list.crm_service_cate_id);
          this.salesTargetInfo.push({
            company_id: this.loggedUser.company_id,
            crm_item_id: list.crm_service_cate_id,
            year: this.year,
            total_target_amt: 0,
            sales_target_details: this.listDetail,
            parent_nm: dt.crm_service_cate_nm,
            crm_service_cate_nm: list.crm_service_cate_nm,
            use_yn: true,
            remark: '',
            creator: '',
            changer: '',
            created_time: '',
            changed_time: ''
          })
        }
      });
    });
    if (generateMonth) {
    this.defineSumMonth();
    }
  }

  onSave() {
    this.crmSalesTargetService.InsertSalesTarget(this.salesTargetInfo).then(data => {
      if (data.error) {
        if (data.error.code === 403) {
          this.router.navigate(["/error/error403"]);
        }
        this.notification.showMessage("error", "error");
      } else {
        this.notification.showMessage("success", "Success");
      }
    });
  }
  defineSumMonth() {
    this.sumYear = 0;
    for (let i = 0; i < 12; i++) {
      this.sumMonth.push(0);
    }
  }
  sumYear_amt(): number {
    // let sum:number = 0;
    // for(let i = 0; i < this.salesTargetInfo.length; i++) {
    //   sum += parseInt(this.salesTargetInfo[i].total_target_amt.toString());
    // }
    // return sum;
    return this.salesTargetInfo.reduce((sum, item) => sum + parseInt(item.total_target_amt.toString()), 0)
  }
  sumMonth_amt(month): number {
    // let sum = 0;
    // for(let i = 0; i < this.salesTargetInfo.length; i++) {
    //   sum += parseFloat(this.salesTargetInfo[i].sales_target_details[month-1].monthly_target_amt.toString());
    // }
    // return sum;
    return this.salesTargetInfo.reduce((sum, item) => sum + parseFloat(item.sales_target_details[month - 1].monthly_target_amt.toString()), 0)
  }
  Average(index) {
    this.salesTargetInfo[index].sales_target_details
    this.salesTargetInfo[index].sales_target_details.forEach(dt => {
      dt.monthly_target_amt = parseFloat((this.salesTargetInfo[index].total_target_amt / 12).toFixed(0));
    });
    for (let i = 0; i < 12; i++) {
      this.sumMonth[i] = this.sumMonth_amt(i + 1);
    }
  }
  onChangeTotalYear() {
    this.sumYear = this.sumYear_amt();
  }
  onChangeTotalMonth(index, parentIndex) {
    this.sumMonth[index] = this.sumMonth_amt(index + 1);
    this.salesTargetInfo[parentIndex].total_target_amt = this.salesTargetInfo[parentIndex].sales_target_details.reduce((sum, item) => sum + parseInt(item.monthly_target_amt.toString()), 0);
    this.sumYear = this.sumYear_amt();
  }
  onYearChange() {
    if (this.yearBK != this.year) this.getSalesTarget();
  }
}
