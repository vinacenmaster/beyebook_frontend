import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesTargetComponent } from './sales-target.component';

const routes: Routes = [{
  path: '',
  component: SalesTargetComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesTargetRoutingModule { }
