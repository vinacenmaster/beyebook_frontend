import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CostOrderDetailComponent } from './cost-order-detail.component';

const routes: Routes = [{
  path: '',
  component: CostOrderDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostOrderDetailRoutingModule { }
