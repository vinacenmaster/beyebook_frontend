import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';

import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { NgxCurrencyModule } from 'ngx-currency';
import { CostOrderDetailComponent } from './cost-order-detail.component';
import { CostOrderDetailRoutingModule } from './cost-order-detail-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SmartadminDatatableModule,
    NgxCurrencyModule,
    CostOrderDetailRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [
    CostOrderDetailComponent
  ]
})
export class CostOrderDetailModule { }
