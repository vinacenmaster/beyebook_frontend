import { Component, OnInit, ViewChild } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService, UserMasterService, CanDeactivateGuard, ProgramService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { ExpensesService } from '@app/core/services/crm/expenses-magt.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { CrmProjectService } from '@app/core/services/crm/project.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CrmMasServiceItemService } from '@app/core/services/crm/setting-item.service';
import { CrmMasServiceItemModel } from '@app/core/models/crm/setting-item.model';
import * as _ from 'lodash';
import { CostOrderService, CostOrderDetailService } from '@app/core/services/crm/cost-order.service';
import { CostOrderModel } from '@app/core/models/crm/cost-order.model';
import { Location } from '@angular/common';
import { GeneralMasterModel } from '@app/core/models/general_master.model';
import { Category } from '@app/core/common/static.enum';



@Component({
  selector: 'sa-customer-master',
  templateUrl: './cost-order-detail.component.html',
  styleUrls: ['./cost-order-detail.component.css']
})
export class CostOrderDetailComponent extends BasePage implements OnInit {


  @ViewChild("popupExpenses") popupExpenses;

  modalRef: BsModalRef;
  settings: any = {}
  data: any = {}
  userLogin: any = {};

  salesOpportunity: any[] = [];
  customer: any[] = [];
  contactor: any[] = [];
  contactor_nm: string = '';
  user: any[] = [];
  newRows: number = 1
  costOrderModel: CostOrderModel
  costOrderModelClone: CostOrderModel
  isUpdateCostOrderDetail: boolean

  listServiceItem: CrmMasServiceItemModel[] = []
  loadSharing: boolean = false
  validationOptions: any = {}
  optionsDatePicker: any = {}
  private location: Location
  unitOfCount: GeneralMasterModel[] = []

  orderId: any

  //Data table option
  optionsCategory: {}

  constructor(
    private notificationService: NotificationService,

    public userService: AuthService,
    public userMasterService: UserMasterService,
    private generalMasterService: GeneralMasterService,
    public programService: ProgramService,
    private modalService: BsModalService,

    private crmSalesOpportunityService: CrmSalesOpportunityService,
    private CrmProjectService: CrmProjectService,

    private traderService: TraderService,
    private contactorService: ContactorService,

    public expensesService: ExpensesService,

    private router: Router,
    private route: ActivatedRoute,


    public costOrderService: CostOrderService,
    public costOrderDetailService: CostOrderDetailService,


    private crmMasServiceItemService: CrmMasServiceItemService,


  ) {
    super(userService);
  }

  ngOnInit() {
    //general info
    this.loadGeneralInfo()
  }
  loadGeneralInfo() {
    this.userLogin = this.userService.getUserInfo();
    // console.log("this.userLogin", this.userLogin)
    this.loadUnitOfCount().then(data => {
      this.unitOfCount.push(...data)
      console.log("unitOfCount", this.unitOfCount)
    })
    this.costOrderModel = this.costOrderService.initModel(this.userLogin.company_id);
    this.orderId = this.route.snapshot.paramMap.get("id");
    if (this.orderId == "0") {
      this.loadSharing = true
      this.costOrderModelClone = _.cloneDeep(this.costOrderModel)
    } else {
      this.costOrderService.getDetail(this.userLogin.company_id, this.orderId).then(data => {
        this.costOrderModel = data
        console.log('this.costOrderModel', this.costOrderModel)
        this.loadSharing = true
        setTimeout(() => {
          this.loadUnitOfService('for_list')
        }, 50);
        this.costOrderModelClone = _.cloneDeep(this.costOrderModel)
        setTimeout(() => {
          this.bindContactor()
          // console.log('this.contactor_nm', this.contactor_nm)
        }, 2000);
        // console.log("this.costOrderModel", this.costOrderModel)
      })
    }
    this.getCustomer();
    this.getSytemUser();
    this.getListSaleOpportunity()

    this.initValidationForm();
    this.getAllServiceItem();
  }
  loadUnitOfService(row) {
    if (row === 'for_list') {
      for (let item of this.costOrderModel.list_cost_order_detail) {
        let infoServiceOfitem = this.listServiceItem.find(c => c.crm_item_id == item.crm_item_id)
        for (let unit of this.unitOfCount) {
          if (infoServiceOfitem.item_unit_gen_cd.toString() == unit.gen_cd) {
            item.item_unit_gen_nm = unit.gen_nm
          }
        }
      }
    } else {
      let infoServiceOfitem = this.listServiceItem.find(c => c.crm_item_id == row.crm_item_id)
      for (let unit of this.unitOfCount) {
        if (infoServiceOfitem.item_unit_gen_cd.toString() == unit.gen_cd) {
          row.item_unit_gen_nm = unit.gen_nm
        }
      }
    }
  }
  loadUnitOfCount() {
    return this.generalMasterService.listGeneralByCate(Category.ItemUnit)
  }
  getAllServiceItem() {
    this.crmMasServiceItemService.getList(this.userLogin.company_id, null).then(data => {
      this.listServiceItem = data
      // console.log("this.listServiceItem", this.listServiceItem)
    })
  }

  getListSaleOpportunity() {
    this.crmSalesOpportunityService.ShortList(this.userLogin.company_id).then(data => {
      this.salesOpportunity.push(...data);
      // console.log('List salesOpportunity', this.salesOpportunity)
    })
  }



  getCustomer() {
    this.traderService.ShortList(this.userLogin.company_id).then(data => {
      this.customer.push(...data)
      console.log('List customer', this.customer)
    })
  }

  getSytemUser() {
    this.userMasterService.listUsers().then(data => {
      this.user.push(...data)
      // console.log('List user', this.user)
    })
  }
  onSubmit() {
    if (this.costOrderModel.cost_order_id != 0) {
      this.costOrderModel.is_update_cost_order_detail = false
      this.costOrderModel.is_update_cost_order_detail = !_.isEqual(this.costOrderModel.list_cost_order_detail, this.costOrderModelClone.list_cost_order_detail)
    }

    this.costOrderService.addOrUpdate(this.costOrderModel).then(data => {
      if (data.success <= 0) {
        this.notificationService.showMessage("error", data.message);
      } else {
        this.costOrderModelClone = _.cloneDeep(this.costOrderModel)
        this.notificationService.showMessage("success", data.message);
      }
    })
  }
  onChangeSalesOpt() {
    // console.log("this.costOrderModel.salesopt_id", this.costOrderModel.salesopt_id)
    this.bindContactor()

  }
  bindContactor() {
    this.salesOpportunity.forEach(sale => {
      if (this.costOrderModel.salesopt_id == sale.salesopt_id) {
        this.costOrderModel.customer_contactor_id = sale.contractor_id;
        this.customer.forEach(con => {
          if (con.trader_id == sale.contractor_id) {
            this.contactor_nm = con.trader_local_nm
          }
        });
      }
    });
  }
  initValidationForm() {
    this.optionsDatePicker = {
      onClose: function () {
        $('.datePicker').valid();
      }
    }
    this.validationOptions = {
      ignore: [],
      rules: {
        salesOpportunityName: {
          required: true
        },
        contactor: {
          required: true
        },
        orderName: {
          required: true
        },
        salesOrderDate: {
          required: true
        },
        amount: {
          required: true
        },
      },
      messages: {
        salesOpportunityName: {
          required: "Please enter"
        },
        contactor: {
          required: "Please select"
        },
        orderName: {
          required: "Please select"
        },
        salesOrderDate: {
          required: "Please select"
        },
        amount: {
          required: "Please select"
        }
      }
    }
  }

  onCloseProgram() {
    this.router.navigate(['crm-purch-order'])
    this.location.back()

  }

  onCopy() {

    for (let item of this.salesOpportunity) {
      if (item.salesopt_id == this.costOrderModel.salesopt_id) {
        this.costOrderModel.cost_order_nm = item.salesopt_nm
      }
    }
    // console.log('this.salesOpportunity', this.salesOpportunity)
  }

  onDelete() {

    this.notificationService.confirmDialog(
      "Deleting this item ?",
      `Are You Delete Order ${this.costOrderModel.cost_order_nm} ?`,
      x => {
        if (x) {
          this.costOrderService.delete(this.costOrderModel).then(data => {
            if (data.success <= 0) {
              this.notificationService.showMessage("error", data.message);
            } else {
              this.notificationService.showMessage("success", data.message);
              this.router.navigate(['crm-purch-order'])
            }
          })
        }
      }
    )
  }

  onReset() {
    this.costOrderModel = this.costOrderService.initModel(this.userLogin.company_id)
    $("#e1").find('option').prop("selected", false);
    $("#e1").trigger('change');
  }
  onServiceItemChange(data, index) {
    let row = this.costOrderModel.list_cost_order_detail[index]
    console.log('row', row)
    this.loadUnitOfService(row)
  }
  sharingToSelected(data) {
    this.costOrderModel.sharing_to = data;
    // console.log('sharingdata', this.costOrderModel.sharing_to)
  }
  onHRChange(data) {
    // console.log('changes', this.listOrderDetailModel)
  }
  onchangePrice(value, index) {
    this.sumPrice('price', value, index)
  }
  onchangeQuantity(value, index) {
    this.sumPrice('quantity', value, index)
  }
  sumPrice(type, value, index) {
    let item = this.costOrderModel.list_cost_order_detail[index]
    if (type == 'price') {
      item.sub_total_amt = item.qty * value
      item.estimate_atm = item.qty * value
      this.costOrderModel.cost_order_atm = _.sumBy(this.costOrderModel.list_cost_order_detail, c => c.sub_total_amt);
    }
    if (type == 'quantity') {
      item.sub_total_amt = item.price * value
      item.estimate_atm = item.price * value
      this.costOrderModel.cost_order_atm = _.sumBy(this.costOrderModel.list_cost_order_detail, c => c.sub_total_amt);
    }
  }
  addNewRows() {
    // console.log('newRows', this.newRows)
    for (let i = 0; i < this.newRows; i++) {
      let costOrderDetail = this.costOrderDetailService.initModel(this.userLogin.company_id)
      costOrderDetail.cost_order_id = this.costOrderModel.cost_order_id
      costOrderDetail.qty = 1
      costOrderDetail.creator = costOrderDetail.changer = this.userLogin.user_name
      this.costOrderModel.list_cost_order_detail.push(costOrderDetail)
    }
  }

  removeDetail(row, index) {
    if (this.costOrderModel.list_cost_order_detail.length > 0) {
      console.log('row', row)

      this.notificationService.confirmDialog(
        "Deleting this item ?",
        `Do you want to delete it ?`,
        x => {
          if (x) {
            // this.costOrderModel.list_order_detail = _.remove(this.costOrderModel.list_order_detail, c => c.trans_seq == row.trans_seq && c.crm_item_id == row.crm_item_id)
            let newInsert = _.isEqual(this.costOrderModel.list_cost_order_detail, this.costOrderModelClone.list_cost_order_detail)
            if (!newInsert) {
              this.costOrderModel.list_cost_order_detail.splice(index, 1);
              this.notificationService.showMessage("success", "Success");
            }
            else {
              this.costOrderDetailService.delete(row).then(data => {
                if (data.error) {
                  if (data.error.code === 403) {
                    this.router.navigate(["/error/error403"]);
                  }
                  this.notificationService.showMessage("error", data.error.message);
                } else {
                  this.costOrderModel.list_cost_order_detail.splice(index, 1);
                  this.costOrderModelClone = _.cloneDeep(this.costOrderModel)
                  this.notificationService.showMessage("success", data.message);
                }
              })
            }
            // console.log('test', test)
          }
        }
      );
    }
  }
  refreshOrderDetail(row, index) {
    // console.log('list_order_detail', this.costOrderModel.list_order_detail[index])
    this.costOrderModel.list_cost_order_detail[index] = _.cloneDeep(this.costOrderModelClone.list_cost_order_detail[index])
    this.costOrderModel.cost_order_atm = this.costOrderModelClone.cost_order_atm
  }
}
