import { Component, OnInit, ViewChild, } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService, UserMasterService, ProgramService } from '@app/core/services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { Router } from '@angular/router';
import { CrmContractModel } from '@app/core/models/crm/contract.model';
import { CrmContractService } from '@app/core/services/crm/contract.service';
import { CostOrderModel } from '@app/core/models/crm/cost-order.model';
import { CostOrderService } from '@app/core/services/crm/cost-order.service';
import { ProgramModel } from '@app/core/models/program.model';
import { config } from '@app/core/smartadmin.config';



@Component({
  selector: 'sa-customer-master',
  templateUrl: './cost-order-master.component.html',
  styleUrls: ['./cost-order-master.component.css']
})
export class CostOrderMasterComponent extends BasePage implements OnInit {
  @ViewChild("popupContract") popupContract;

  userLogin: any = {};
  salesOpportunity: any[] = [];
  modalRef: BsModalRef;
  options: any = {}
  costOrderModel: CostOrderModel
  listCostOrderModel: CostOrderModel[] = []
  contractList: CrmContractModel[] = [];
  customer: any[] = [];

  //Data table option
  optionsCategory: {}

  constructor(
    private notificationService: NotificationService,
    private router: Router,
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public programService: ProgramService,
    public modalService: BsModalService,
    public CostOrderService: CostOrderService,
    private notification: NotificationService,
    private crmContractService: CrmContractService,
    private traderService: TraderService,

  ) {
    super(userService);
  }

  ngOnInit() {
    //general info
    this.loadGeneralInfo()
    this.initDatatable()
    this.getCustomer()
  }
  loadGeneralInfo() {
    this.userLogin = this.userService.getUserInfo();
    this.costOrderModel = this.CostOrderService.initModel(this.loggedUser.company_id)
    // console.log("this.userInfo", this.userLogin)
  }

  initDatatable() {
    this.options = {
      dom: "Bfrtip",
      ajax: (data, callback, settings) => {
        Promise.all([this.getContract(),this.getCustomer()]).then(AllData => {
          // console.log('AllData', AllData)
          this.bindEventPoppup()
          this.CostOrderService.getList(this.loggedUser.company_id).then(data => {
            this.listCostOrderModel = data
            // console.log('this.listCostOrderModel', this.listCostOrderModel)
            callback({
              aaData: data
            })
          })
        })
      },
      columns: [
        {
          data: (data) => {
            return `<a class="goto" data-costId = "${data.cost_order_id}">${data.cost_order_nm}</a>`
          },
          width: "350px"
        },
        {
          data: (data) => {
            var dt = this.customer.filter(c => c.trader_id == data.customer_contactor_id)
            if (dt.length > 0) {
              return dt[0].trader_local_nm
            }
            return 'NA'
          },
          width: "50px",
          className:'center'
        },
        { data: "cost_order_ymd", className: "center", width: "100px" },
        {
          data: "cost_order_atm", className: "right", width: "50px",
          render: $.fn.dataTable.render.number(','),
        },
        {

          data: (data) => {
            // console.log('this.data', data)
            var a = this.contractList.filter(x => x.src_order_id == data.cost_order_id && x.src_order_type == false)
            if (a.length > 0)
              return `<p class="label label-info" style="background-color: #00b5ad; color: white; height: 20px; font-size: 11px; width: 135px; line-height: 15px; display: inline-block; font-weight: 300; margin: 0; border-radius: 2px;">Contract completion</p>`
            else
              return `<button class="btn btn-warning openPopup" style="background-color: #fbbd08; color: white; height: 20px; font-size: 11px; width: 135px; line-height: 0px; border: none;" data-costOrderId="${data.cost_order_id}">Contract registration</button>`
          },
          width: "40px",
          className: "center"
        },
        { data: "creator", class: "center", width: "100px" },
        { data: "changed_time", class: "center", width: '100px' },
      ],
      pageLength: 25,
      scrollY: 500,
      scrollX: true,
      buttons: [
        {
          text: '<i class="fa fa-refresh" title="Refresh"></i>',
          action: (e, dt, node, config) => {
            dt.ajax.reload();
            this.bindEventPoppup()
          }
        },
        {
          extend: "csv",
          text: "Excel"
        },
        {
          extend: "pdf",
          text: "Pdf"
        },
        {
          extend: "print",
          text: "Print"
        }
      ]
    }
  }
  getContract() {
    return this.crmContractService.getAllShortList(this.userLogin.company_id).then(data => {
      this.contractList = data
      // console.log('this.contractList', this.contractList)

    })
  }
  getListOrder() {
    this.CostOrderService.getList(this.loggedUser.company_id).then(data => {
      this.listCostOrderModel = data
      // console.log('this.listCostOrderModel', this.listCostOrderModel)
    })
  }
  onNewOrder() {
    this.router.navigate(['crm-purch-order-detail/0'])
  }
  openPopup() {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.modalRef = this.modalService.show(this.popupContract, config);
  }
  bindEventPoppup() {
    let vm = this
    setTimeout(() => {
      $('.openPopup').click(function () {
        let orderId = $(this).attr('data-costOrderId')
        vm.costOrderModel = vm.listCostOrderModel.filter(c => c.cost_order_id == orderId)[0]
        console.log('orderModel', vm.costOrderModel)
        vm.customer.forEach(con => {
          if (con.trader_id == vm.costOrderModel.customer_contactor_id) {
            vm.costOrderModel.customer_contactor_nm = con.trader_local_nm
            vm.costOrderModel.order_type = false
            // console.log('costOrderModel', vm.costOrderModel)
          }
        });
        vm.openPopup()
      });
      $('.goto').click(function () {
        let id = $(this).attr('data-costId');
        var menu_url:string;
        var menu_id:number;
        var menu_name:string;
        menu_url = "/crm-purch-order-detail/" + id;
        menu_id = 9005;
        menu_name = "Cost order detail";
        
        if (!vm.programService.openedPrograms.filter(x => x.id == menu_id).length && vm.programService.openedPrograms.length == config.maxOpenedPrograms) {
          vm.notification.smartMessageBox({
            title: "Notification",
            content: `Maximum ${config.maxOpenedPrograms} programs can be opened!`,
            buttons: '[OK]'
          });
          return;
        }
        $('.center-loading').show();
        vm.programService.addOpenedPrograms(new ProgramModel(menu_id, menu_name, menu_url),true);
  
      });
    }, 500);
  }
  reloadDatatable(value) {
    if (value) {
      $(".costOrder")
        .DataTable()
        .ajax.reload();
    }

  }
  closePopup() {
    this.modalRef && this.modalRef.hide();
  }

  getCustomer() {
   return  this.traderService.ShortList(this.userLogin.company_id).then(data => {
      this.customer.push(...data)
      // console.log('List customer', this.customer)
    })
  }

}
