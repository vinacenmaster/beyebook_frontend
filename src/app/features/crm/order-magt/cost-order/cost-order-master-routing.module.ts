import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CostOrderMasterComponent } from './cost-order-master.component';

const routes: Routes = [{
  path: '',
  component: CostOrderMasterComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CostOrderMasterRoutingModule { }
