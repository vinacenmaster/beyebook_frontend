import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';

import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { NgxCurrencyModule } from 'ngx-currency';
import { SalesOrderMasterComponent } from './sales-order-master.component';
import { SalesOrderMasterRoutingModule } from './sales-order-master-routing.module';
import { ContractPopupModule } from '../../contract/contract-popup/contract-popup.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SmartadminDatatableModule,
    NgxCurrencyModule,
    SalesOrderMasterRoutingModule,
    ContractPopupModule,
  ],
  declarations: [
    SalesOrderMasterComponent
  ]
})
export class SalesOrderMasterModule { }
