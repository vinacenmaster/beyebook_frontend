import { Component, OnInit, ViewChild } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService, UserMasterService, ProgramService } from '@app/core/services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import * as _ from 'lodash';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { SalesOrderService } from '@app/core/services/crm/sales-order.service';
import { Router } from '@angular/router';
import { GetOrderModel } from '@app/core/models/crm/get-order.model';
import { CrmContractModel } from '@app/core/models/crm/contract.model';
import { CrmContractService } from '@app/core/services/crm/contract.service';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { ProgramModel } from '@app/core/models/program.model';
import { config } from '@app/core/smartadmin.config';



@Component({
  selector: 'sa-customer-master',
  templateUrl: './sales-order-master.component.html',
  styleUrls: ['./sales-order-master.component.css']
})
export class SalesOrderMasterComponent extends BasePage implements OnInit {
  @ViewChild("popupContract") popupContract;

  userLogin: any = {};
  salesOpportunity: any[] = [];
  modalRef: BsModalRef;
  options: any = {}
  orderModel: GetOrderModel
  listOrderModel: GetOrderModel[] = []
  contractList: CrmContractModel[] = [];
  customer: any[] = [];

  //Data table option
  optionsCategory: {}

  constructor(
    private notificationService: NotificationService,
    private router: Router,
    public userService: AuthService,
    public userMasterService: UserMasterService,
    public programService: ProgramService,
    public modalService: BsModalService,
    public salesOrderService: SalesOrderService,
    private crmContractService: CrmContractService,
    private traderService: TraderService,
    private notification: NotificationService,
    private crmSalesOpportunityService: CrmSalesOpportunityService,

  ) {
    super(userService);
  }

  ngOnInit() {
    //general info
    this.loadGeneralInfo()
    this.initDatatable()

  }
  loadGeneralInfo() {
    this.userLogin = this.userService.getUserInfo();
    this.orderModel = this.salesOrderService.initModel(this.loggedUser.company_id)
    this.getCustomer()
    // console.log("this.userInfo", this.userLogin)
  }

  initDatatable() {
    this.options = {
      dom: "Bfrtip",
      ajax: (data, callback, settings) => {
        Promise.all([this.getContract(), this.getCustomer()]).then(AllData => {
          // console.log('AllData', AllData)
          this.bindEventPoppup()
          this.salesOrderService.getList(this.loggedUser.company_id).then(data => {
            this.listOrderModel = data
            // console.log('this.listOrderModel', this.listOrderModel)
            callback({
              aaData: data
            })
          })
        })
      },
      columns: [
        {
          data: (data) => {
            return `<a class="goto" data-saleOrderId = "${data.order_id}">${data.order_nm}</a>`
          },
          width: "350px"
        },
        {
          data: (data) => {
            var dt = this.customer.filter(c => c.trader_id == data.customer_contactor_id)
            if (dt.length > 0) {
              return dt[0].trader_local_nm
            }
            return 'NA'
          },
          width: "50px",className:'center'
        },

        { data: "order_ymd", className: "center", width: "50px" },
        {
          data: "total_atm", className: "right", width: "50px",
          render: $.fn.dataTable.render.number(',')
        },
        {
          data: (data) => {
            // console.log('this.data', data)
            var a = this.contractList.filter(x => x.src_order_id == data.order_id && x.src_order_type == true)
            if (a.length > 0)
              return `<p class="label label-info" style="background-color: #00b5ad; color: white; height: 20px; font-size: 11px; width: 135px; line-height: 15px; display: inline-block; font-weight: 300; margin: 0; border-radius: 2px;">Contract completion</p>`
            else
              return `<button class="btn btn-warning openPopup" style="background-color: #fbbd08; color: white; height: 20px; font-size: 11px; width: 135px; line-height: 0px; border: none;" data-orderId="${data.order_id}">Contract registration</button>`
          },
          width: "50px",
          className: "center"
        },
        { data: "creator", class: "center", width: "100px" },
        { data: "changed_time", class: "center", width: '100px' },
      ],
      pageLength: 25,
      scrollY: 500,
      scrollX: true,
      buttons: [
        {
          text: '<i class="fa fa-refresh" title="Refresh"></i>',
          action: (e, dt, node, config) => {
            dt.ajax.reload();
            this.bindEventPoppup()
          }
        },
        {
          extend: "csv",
          text: "Excel"
        },
        {
          extend: "pdf",
          text: "Pdf"
        },
        {
          extend: "print",
          text: "Print"
        }
      ]
    }
  }
  getContract() {
    return this.crmContractService.getAllShortList(this.userLogin.company_id).then(data => {
      this.contractList = data
      console.log('this.contractList', this.contractList)

    })
  }
  getListOrder() {
    this.salesOrderService.getList(this.loggedUser.company_id).then(data => {
      this.listOrderModel = data
      // console.log('this.listOrderModel', this.listOrderModel)
    })
  }
  onNewOrder() {
    this.router.navigate(['crm-sales-order-detail/0'])
  }
  openPopup() {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.modalRef = this.modalService.show(this.popupContract, config);
  }
  bindEventPoppup() {
    let vm = this
    setTimeout(() => {
      $('.openPopup').click(function () {
        let orderId = $(this).attr('data-orderId')
        vm.orderModel = vm.listOrderModel.filter(c => c.order_id == orderId)[0]
        // console.log('orderModel', vm.orderModel)
        vm.customer.forEach(con => {
          if (con.trader_id == vm.orderModel.customer_contactor_id) {
            vm.orderModel.customer_contactor_nm = con.trader_local_nm
            vm.orderModel.order_type = true
          }
        });
        vm.openPopup()
      });
      
      $('.goto').click(function () {
        let id = $(this).attr('data-saleOrderId');
        var menu_url:string;
        var menu_id:number;
        var menu_name:string;
        menu_url = "/crm-sales-order-detail/" + id;
        menu_id = 9004;
        menu_name = "Sales order detail";
        
        if (!vm.programService.openedPrograms.filter(x => x.id == menu_id).length && vm.programService.openedPrograms.length == config.maxOpenedPrograms) {
          vm.notification.smartMessageBox({
            title: "Notification",
            content: `Maximum ${config.maxOpenedPrograms} programs can be opened!`,
            buttons: '[OK]'
          });
          return;
        }
        $('.center-loading').show();
        vm.programService.addOpenedPrograms(new ProgramModel(menu_id, menu_name, menu_url),true);
  
      });
    }, 500);
  }
  reloadDatatable(value) {
    if (value) {
      $(".salesOrder")
        .DataTable()
        .ajax.reload();
    }

  }
  closePopup() {
    this.modalRef && this.modalRef.hide();
  }

  getCustomer() {
    return this.traderService.ShortList(this.userLogin.company_id).then(data => {
      this.customer.push(...data)
      // console.log('List customer', this.customer)
    })
  }

}
