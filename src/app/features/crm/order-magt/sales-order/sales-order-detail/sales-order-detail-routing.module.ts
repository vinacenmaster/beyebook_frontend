import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SalesOrderDetailComponent } from './sales-order-detail.component';

const routes: Routes = [{
  path: '',
  component: SalesOrderDetailComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesOrderDetailRoutingModule { }
