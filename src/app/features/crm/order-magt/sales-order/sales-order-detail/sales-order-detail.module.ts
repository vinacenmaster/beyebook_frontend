import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { SmartadminInputModule } from '@app/shared/forms/input/smartadmin-input.module';

import { SmartadminDatatableModule } from '@app/shared/ui/datatable/smartadmin-datatable.module';
import { NgxCurrencyModule } from 'ngx-currency';
import { SalesOrderDetailComponent } from './sales-order-detail.component';
import { SalesOrderDetailRoutingModule } from './sales-order-detail-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SmartadminValidationModule,
    SmartadminInputModule,
    SmartadminDatatableModule,
    NgxCurrencyModule,
    SalesOrderDetailRoutingModule,
    Ng2SmartTableModule
  ],
  declarations: [
    SalesOrderDetailComponent
  ]
})
export class SalesOrderDetailModule { }
