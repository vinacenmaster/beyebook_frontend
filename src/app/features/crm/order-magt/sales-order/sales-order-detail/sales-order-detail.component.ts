import { Component, OnInit, ViewChild } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService, UserMasterService, ProgramService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { ExpensesService } from '@app/core/services/crm/expenses-magt.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CrmSalesOpportunityService } from '@app/core/services/crm/sale-opportunity.service';
import { CrmProjectService } from '@app/core/services/crm/project.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { GetOrderModel } from '@app/core/models/crm/get-order.model';
import { Router, ActivatedRoute } from '@angular/router';
import { SalesOrderService, SalesOrderDetailService } from '@app/core/services/crm/sales-order.service';
import { CrmMasServiceItemService } from '@app/core/services/crm/setting-item.service';
import { CrmMasServiceItemModel } from '@app/core/models/crm/setting-item.model';
import * as _ from 'lodash';
import { Location } from '@angular/common';
import { GeneralMasterModel } from '@app/core/models/general_master.model';
import { Category } from '@app/core/common/static.enum';



@Component({
  selector: 'sa-customer-master',
  templateUrl: './sales-order-detail.component.html',
  styleUrls: ['./sales-order-detail.component.css']
})
export class SalesOrderDetailComponent extends BasePage implements OnInit {


  @ViewChild("popupExpenses") popupExpenses;

  modalRef: BsModalRef;
  settings: any = {}
  data: any = {}
  userLogin: any = {};

  salesOpportunity: any[] = [];
  customer: any[] = [];
  contactor: any[] = [];
  contactor_nm: string = '';
  user: any[] = [];
  newRows: number = 1
  orderModel: GetOrderModel
  orderModelClone: GetOrderModel
  isUpdateOrderDetail: boolean

  listServiceItem: CrmMasServiceItemModel[] = []
  loadSharing: boolean = false
  validationOptions: any = {}
  optionsDatePicker: any = {}
  unitOfCount: GeneralMasterModel[] = []

  orderId: any

  //Data table option
  optionsCategory: {}

  constructor(
    private notificationService: NotificationService,

    public userService: AuthService,
    public userMasterService: UserMasterService,
    private generalMasterService: GeneralMasterService,
    public programService: ProgramService,
    private modalService: BsModalService,

    private crmSalesOpportunityService: CrmSalesOpportunityService,
    private CrmProjectService: CrmProjectService,

    private traderService: TraderService,
    private contactorService: ContactorService,

    public expensesService: ExpensesService,

    private router: Router,
    private route: ActivatedRoute,

    public salesOrderService: SalesOrderService,
    public salesOrderDetailService: SalesOrderDetailService,

    private crmMasServiceItemService: CrmMasServiceItemService,

    private location: Location

  ) {
    super(userService);
  }

  ngOnInit() {
    //general info
    this.loadGeneralInfo()
  }
  loadGeneralInfo() {
    this.userLogin = this.userService.getUserInfo();
    // console.log("this.userLogin", this.userLogin)
    this.loadUnitOfCount().then(data => {
      this.unitOfCount.push(...data)
      console.log("unitOfCount", this.unitOfCount)
    })
    this.orderModel = this.salesOrderService.initModel(this.userLogin.company_id);
    this.orderId = this.route.snapshot.paramMap.get("id");
    if (this.orderId == "0") {
      this.loadSharing = true
      this.orderModelClone = _.cloneDeep(this.orderModel)
    } else {
      this.salesOrderService.getDetail(this.userLogin.company_id, this.orderId).then(data => {
        console.log("===================", data);
        this.orderModel = data
        this.loadSharing = true
        setTimeout(() => {
          this.loadUnitOfService('for_list')
        }, 50);
        this.orderModelClone = _.cloneDeep(this.orderModel)
        console.log('this.list_order_detail', this.orderModel.list_order_detail)
        setTimeout(() => {
          this.bindContactor()
        }, 2000);
      })
    }
    // var users = [
    //   { 'user': 'barney', 'age': 36, 'active': true },
    //   { 'user': 'fred',   'age': 40, 'active': false }
    // ];


    this.getCustomer();
    this.getSytemUser();
    this.getListSaleOpportunity()
    this.initValidationForm();
    this.getAllServiceItem();
  }
  loadUnitOfService(row) {
    if (row === 'for_list') {
      for (let item of this.orderModel.list_order_detail) {
        let infoServiceOfitem = this.listServiceItem.find(c => c.crm_item_id == item.crm_item_id)
        for (let unit of this.unitOfCount) {
          if (infoServiceOfitem.item_unit_gen_cd.toString() == unit.gen_cd) {
            item.item_unit_gen_nm = unit.gen_nm
          }
        }
      }
    } else {
      let infoServiceOfitem = this.listServiceItem.find(c => c.crm_item_id == row.crm_item_id)
      for (let unit of this.unitOfCount) {
        if (infoServiceOfitem.item_unit_gen_cd.toString() == unit.gen_cd) {
          row.item_unit_gen_nm = unit.gen_nm
        }
      }
    }
  }
  loadUnitOfCount() {
    return this.generalMasterService.listGeneralByCate(Category.ItemUnit)
  }
  getAllServiceItem() {
    this.crmMasServiceItemService.getListForDropdown(this.userLogin.company_id).then(data => {
      this.listServiceItem = data
      console.log("this.listServiceItem", this.listServiceItem)
    })
    // this.crmMasServiceItemService.getList(this.userLogin.company_id,null).then(data => {
    //   this.listServiceItem = data
    //   console.log("this.listServiceItem", this.listServiceItem)
    // })
  }

  getListSaleOpportunity() {
    this.crmSalesOpportunityService.ShortList(this.userLogin.company_id).then(data => {
      this.salesOpportunity.push(...data);
      // console.log('List salesOpportunity', this.salesOpportunity)
    })
  }
  getCustomer() {
    this.traderService.ShortList(this.userLogin.company_id).then(data => {
      this.customer.push(...data)
      // console.log('List customer', this.customer)
    })
  }

  getSytemUser() {
    this.userMasterService.listUsers().then(data => {
      this.user.push(...data)
      // console.log('List user', this.user)
    })
  }
  onSubmit() {
    // console.log('this.orderModel.list_order_detail1', this.orderModel.list_order_detail)
    if (this.orderModel.order_id != 0) {
      this.orderModel.is_update_order_detail = false
      this.orderModel.is_update_order_detail = !_.isEqual(this.orderModel.list_order_detail, this.orderModelClone.list_order_detail)
    }
    this.salesOrderService.addOrUpdate(this.orderModel).then(data => {
      if (data.success <= 0) {
        this.notificationService.showMessage("error", data.message);
      } else {
        this.orderModelClone = _.cloneDeep(this.orderModel)
        this.notificationService.showMessage("success", data.message);
      }
    })
  }
  changTranSeq() {
    let groupService = _.groupBy(this.orderModel.list_order_detail, function (o) {
      return o.crm_item_id
    })
    // console.log('this.orderModel.groupService', groupService)
    let newArr = []
    for (let item in groupService) {
      if (groupService[item].length > 0) {
        let count = 0
        for (let e of groupService[item]) {
          e.trans_seq = count + 1
          count++;
        }
      }
      newArr.push(...groupService[item])
    }
    // this.orderModel.list_order_detail = newArr
    this.orderModel.list_order_detail = newArr.reverse()
  }
  onChangeSalesOpt() {
    this.bindContactor()
  }
  bindContactor() {
    this.salesOpportunity.forEach(sale => {
      if (this.orderModel.salesopt_id == sale.salesopt_id) {
        this.orderModel.customer_contactor_id = sale.contractor_id;
        this.customer.forEach(con => {
          if (con.trader_id == sale.contractor_id) {
            this.contactor_nm = con.trader_local_nm
          }
        });
      }
    });
  }
  initValidationForm() {
    this.optionsDatePicker = {
      onClose: function () {
        $('.datePicker').valid();
      }
    }
    this.validationOptions = {
      ignore: [],
      rules: {
        salesOpportunityName: {
          required: true
        },
        contactor: {
          required: true
        },
        orderName: {
          required: true
        },
        salesOrderDate: {
          required: true
        },
        amount: {
          required: true
        },
      },
      messages: {
        salesOpportunityName: {
          required: "Please enter"
        },
        contactor: {
          required: "Please select"
        },
        orderName: {
          required: "Please select"
        },
        salesOrderDate: {
          required: "Please select"
        },
        amount: {
          required: "Please select"
        }
      }
    }
  }

  onCloseProgram() {
    // this.router.navigate(['crm-sales-order'])
    this.location.back()
  }

  onCopy() {

    for (let item of this.salesOpportunity) {
      if (item.salesopt_id == this.orderModel.salesopt_id) {
        this.orderModel.order_nm = item.salesopt_nm
      }
    }
    // console.log('this.salesOpportunity', this.salesOpportunity)
  }

  onDelete() {

    this.notificationService.confirmDialog(
      "Deleting this item ?",
      `Are You Delete Order ${this.orderModel.order_nm} ?`,
      x => {
        if (x) {
          this.salesOrderService.delete(this.orderModel).then(data => {
            if (data.success <= 0) {
              this.notificationService.showMessage("error", data.message);
            } else {
              this.notificationService.showMessage("success", data.message);
              this.router.navigate(['crm-sales-order'])
            }
          })
        }
      }
    )
  }

  onReset() {
    this.orderModel = this.salesOrderService.initModel(this.userLogin.company_id)
    $("#e1").find('option').prop("selected", false);
    $("#e1").trigger('change');
  }

  sharingToSelected(data) {
    this.orderModel.sharing_to = data;
    // console.log('sharingdata', this.orderModel.sharing_to)
  }
  onServiceItemChange(data, index) {
    let row = this.orderModel.list_order_detail[index]
    console.log('row', row)
    this.loadUnitOfService(row)
  }
  onchangePrice(value, index) {
    this.sumPrice('price', value, index)
  }
  onchangeQuantity(value, index) {
    this.sumPrice('quantity', value, index)
  }
  sumPrice(type, value, index) {
    // console.log('sharingdata', this.orderModel.list_order_detail[index])
    let item = this.orderModel.list_order_detail[index]
    if (type == 'price') {
      item.sub_total_amt = item.qty * value
      item.estimate_atm = item.qty * value
      this.orderModel.total_atm = _.sumBy(this.orderModel.list_order_detail, c => c.sub_total_amt);
    }
    if (type == 'quantity') {
      item.sub_total_amt = item.price * value
      item.estimate_atm = item.price * value
      this.orderModel.total_atm = _.sumBy(this.orderModel.list_order_detail, c => c.sub_total_amt);
    }
  }
  addNewRows() {
    // console.log('newRows', this.newRows)
    for (let i = 0; i < this.newRows; i++) {
      let orderDetail = this.salesOrderDetailService.initModel(this.userLogin.company_id)
      orderDetail.order_id = this.orderModel.order_id
      orderDetail.qty = 1
      orderDetail.creator = orderDetail.changer = this.userLogin.user_name
      this.orderModel.list_order_detail.push(orderDetail)
    }
    // console.log('this.orderModel.list_order_detail', this.orderModel.list_order_detail)
  }
  removeDetail(row, index) {
    // console.log('this.orderModelClone', this.orderModelClone.list_order_detail)
    if (this.orderModel.list_order_detail.length > 0) {
      // console.log('row', row)
      this.notificationService.confirmDialog(
        "Deleting this item ?",
        `Do you want to delete it`,
        x => {
          if (x) {
            // this.orderModel.list_order_detail = _.remove(this.orderModel.list_order_detail, c => c.trans_seq == row.trans_seq && c.crm_item_id == row.crm_item_id)
            let newInsert = _.isEqual(this.orderModel.list_order_detail, this.orderModelClone.list_order_detail)

            if (!newInsert) {
              this.orderModel.list_order_detail.splice(index, 1);
              this.notificationService.showMessage("success", "Success");
            }
            else {
              this.salesOrderDetailService.delete(row).then(data => {
                if (data.error) {
                  if (data.error.code === 403) {
                    this.router.navigate(["/error/error403"]);
                  }
                  this.notificationService.showMessage("error", data.error.message);
                } else {
                  this.orderModel.list_order_detail.splice(index, 1);
                  this.orderModelClone = _.cloneDeep(this.orderModel)
                  this.notificationService.showMessage("success", data.message);
                }
              })
            }
            // console.log('test', test)
          }
        }
      );
    }
  }
  refreshOrderDetail(row, index) {
    // console.log('list_order_detail', this.orderModel.list_order_detail[index])
    this.orderModel.list_order_detail[index] = _.cloneDeep(this.orderModelClone.list_order_detail[index])
    this.orderModel.total_atm = this.orderModelClone.total_atm
  }
}
