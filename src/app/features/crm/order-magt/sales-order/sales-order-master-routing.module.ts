import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesOrderMasterComponent } from './sales-order-master.component';

const routes: Routes = [{
  path: '',
  component: SalesOrderMasterComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesOrderMasterRoutingModule { }
