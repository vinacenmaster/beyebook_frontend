import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CrmActivityEmailService, CrmActivityTelService, CrmActivityMeetingService, CrmActivityIssueService, CrmActivitySupportService } from '@app/core/services/crm/customer-detail.service';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { UserMasterService, AuthService } from '@app/core/services';
import { CrmActivityBusinessService } from '@app/core/services/crm/sale-opportunity.service';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { Category } from '@app/core/common/static.enum';

@Component({
  selector: 'sa-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['../../common-activity.css']
})
export class TimelineComponent extends BasePage implements OnInit {
  @Output() childCall = new EventEmitter();
  @ViewChild("popupSupport") popupSupport;
  @ViewChild("popupBusiness") popupBusiness;
  @ViewChild("popupEmail") popupEmail;
  @ViewChild("popupTelephone") popupTelephone;
  @ViewChild("popupConference") popupConference;
  @ViewChild("popupIssue") popupIssue;
  modalRef: BsModalRef;
  saleActivityInfo: any[] = [];
  timelineInfo: any[] = [];
  emailInfo: any[] = [];
  telInfo: any[] = [];
  meetingInfo: any[] = [];
  issueInfo: any[] = [];
  userLogin: any;
  companyId: any;
  activityStatus: any[] = [];
  customer: any[] = [];
  contactor: any[] = [];
  user: any[] = [];
  taskType: any[] = [];
  workUnit: any[] = [];
  supportJson: any;
  bussinessJson: any;
  emailJson: any;
  telJson: any;
  meetingJson: any;
  issueJson: any;
  inputFuncId: any;
  funcRefCd: any;
  salesoptNm: string = '';
  SliderdefaultVal: 0;
  Id: any;
  indexExpand: number = -1;
  isLoaded: boolean = false;
  ShowLoadingCenter: boolean = true;
  constructor(
    private crmActivityEmailService: CrmActivityEmailService,
    private crmActivityTelService: CrmActivityTelService,
    private crmActivityMeetingService: CrmActivityMeetingService,
    private crmActivityIssueService: CrmActivityIssueService,
    private crmActivityBusinessService: CrmActivityBusinessService,
    private crmActivitySupportService: CrmActivitySupportService,
    private generalMasterService: GeneralMasterService,
    private modalService: BsModalService,
    private traderService: TraderService,
    private contactorService: ContactorService,
    private userMasterService: UserMasterService,
    public userService: AuthService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    Promise.all([this.getCustomer(), this.getContactor(), this.getSytemUser(), this.getActivitiStatus()]).then(([customer, contactor, user, activityStatus]) => {
      this.activityStatus = activityStatus;
    });
  }
  getActivitiStatus() {
    return this.generalMasterService.listGeneralByCate(Category.ActivityStatus.valueOf())
  }
  getTaskType() {
    return this.generalMasterService.listGeneralByCate(Category.TaskType.valueOf())
  }
  getWorkUnit() {
    return this.generalMasterService.listGeneralByCate(Category.ItemUnit.valueOf())
  }
  getCustomer() {
    this.traderService.ShortList(this.userLogin.company_id).then(data => {
      this.customer.push(...data)
      this.customer.forEach(element => {
        element.with_type = 1;
      });
    })
  }
  getContactor() {
    this.contactorService.ShortList(this.userLogin.company_id).then(data => {
      this.contactor.push(...data)
      this.contactor.forEach(element => {
        element.with_type = 2;
      });
    })
  }
  getSytemUser() {
    this.userMasterService.listUsers().then(data => {
      this.user.push(...data)
    })
  }
  getTimelineInfo(companyId, inputFuncId, funcRefCd, reload?, salesoptNm: string = '') {
    //console.log("=============", companyId);
    this.ShowLoadingCenter = true;
    this.companyId = companyId;
    this.inputFuncId = inputFuncId;
    this.funcRefCd = funcRefCd;
    if (!this.isLoaded || reload) {
      this.timelineInfo = [];
      Promise.all([
        this.getEmailInfo(this.companyId, inputFuncId, funcRefCd),
        this.getTelInfo(this.companyId, inputFuncId, funcRefCd),
        this.getMeetingInfo(this.companyId, inputFuncId, funcRefCd),
        this.getIssueInfo(this.companyId, inputFuncId, funcRefCd)])
        .then(([EmailInfo, TelInfo, MeetingInfo, IssueInfo]) => {
          if (!EmailInfo.data.length && !TelInfo.data.length && !MeetingInfo.data.length && !IssueInfo.data.length) {
            return this.ShowLoadingCenter = false;
          }
          EmailInfo.data.forEach(element => {
            element.email_details.forEach(dt => {
              if (dt.person_type === 0) {
                this.customer.forEach(cs => {
                  if (dt.with_ref_id == cs.trader_id) {
                    dt.name = cs.trader_local_nm;
                  }
                });
              } else if (dt.person_type == 1) {
                this.user.forEach(us => {
                  if (dt.with_ref_id == us.user_id) {
                    dt.name = us.user_nm;
                  }
                });
              } else {
                this.contactor.forEach(us => {
                  if (dt.with_ref_id == us.contactor_id) {
                    dt.name = us.contactor_nm;
                  }
                });
              }
            });
            element.icon = "fa-envelope-o";
            element.type = "email";
            this.timelineInfo.push(element)
            this.sortTimeline();
            this.indexExpand = -1;
            this.subStringContent();
          });
          TelInfo.data.forEach(element => {
            element.tel_details.forEach(dt => {
              if (dt.person_type === 0) {
                this.customer.forEach(cs => {
                  if (dt.with_ref_id == cs.trader_id) {
                    dt.name = cs.trader_eng_nm;
                  }
                });
              } else if (dt.person_type == 1) {
                this.user.forEach(us => {
                  if (dt.with_ref_id == us.user_id) {
                    dt.name = us.user_nm;
                  }
                });
              } else {
                this.contactor.forEach(us => {
                  if (dt.with_ref_id == us.contactor_id) {
                    dt.name = us.contactor_nm;
                  }
                });
              }
            });
            element.icon = "fa-phone";
            element.type = "tel";
            this.timelineInfo.push(element)
            this.sortTimeline();
            this.indexExpand = -1;
            this.subStringContent();
          });
          MeetingInfo.data.forEach(element => {
            element.meeting_details.forEach(dt => {
              if (dt.person_type === 0) {
                this.customer.forEach(cs => {
                  if (dt.with_ref_id == cs.trader_id) {
                    dt.name = cs.trader_eng_nm;
                  }
                });
              } else if (dt.person_type == 1) {
                this.user.forEach(us => {
                  if (dt.with_ref_id == us.user_id) {
                    dt.name = us.user_nm;
                  }
                });
              } else {
                this.contactor.forEach(us => {
                  if (dt.with_ref_id == us.contactor_id) {
                    dt.name = us.contactor_nm;
                  }
                });
              }
            });
            element.icon = "fa-comments-o";
            element.type = "meeting";
            this.timelineInfo.push(element)
            this.sortTimeline();
            this.indexExpand = -1;
            this.subStringContent();
          });
          IssueInfo.data.forEach(element => {
            element.issue_details.forEach(dt => {
              if (dt.person_type === 0) {
                this.customer.forEach(cs => {
                  if (dt.with_ref_id == cs.trader_id) {
                    dt.name = cs.trader_eng_nm;
                  }
                });
              } else if (dt.person_type == 1) {
                this.user.forEach(us => {
                  if (dt.with_ref_id == us.user_id) {
                    dt.name = us.user_nm;
                  }
                });
              } else {
                this.contactor.forEach(us => {
                  if (dt.with_ref_id == us.contactor_id) {
                    dt.name = us.contactor_nm;
                  }
                });
              }
            });
            element.icon = "fa-info-circle";
            element.type = "issue";
            this.timelineInfo.push(element)
            this.sortTimeline();
            this.indexExpand = -1;
            this.subStringContent();
          });
        });
      if (inputFuncId === 3) {
        this.getBusinessInfo(companyId, funcRefCd);
        this.salesoptNm = salesoptNm;
        var bus = this.getBusinessInfo(companyId, funcRefCd);
        bus.then((data) => {
          if (!data.data) {
            return this.ShowLoadingCenter = false;
          }
          data.data.forEach(activity => {
            this.activityStatus.forEach(status => {
              if (activity.sales_status_gen_cd == status.gen_cd) {
                activity.sales_status_gen_nm = status.gen_nm;
              }
            });
            activity.icon = "fa-suitcase";
            activity.type = "business";
            this.timelineInfo.push(activity)
            this.sortTimeline();
            this.indexExpand = -1;
            this.subStringContent();
          });
        })
      }
      if (inputFuncId === 4) {
        Promise.all([this.getTaskType(), this.getWorkUnit(), this.getSupportInfo(companyId, funcRefCd)]).then(([taskType, WorkUnit, SupportInfo]) => {
          if (!SupportInfo.data) {
            return this.ShowLoadingCenter = false;
          }
          SupportInfo.data.forEach(supp => {
            taskType.forEach(pt => {
              if (supp.project_task_type_gen_cd == pt.gen_cd) {
                supp.project_task_type_nm = pt.gen_nm;
              }
            });
            WorkUnit.forEach(unit => {
              if (supp.work_unit_gen_cd == unit.gen_cd) {
                supp.work_unit_gen_nm = unit.gen_nm;
              }
            });
            if (supp.project_task_person) {
              supp.project_task_person.forEach(per => {
                if (per.worker_yn) {
                  if (per.person_type) {//user
                    this.user.forEach(con => {
                      if (per.person_id == con.user_id) {
                        per.person_nm = con.user_nm;
                      }
                    });
                  } else {
                    this.contactor.forEach(con => {
                      if (per.person_id == con.contactor_id) {
                        per.person_nm = con.contactor_nm;
                      }
                    });
                  }
                }
              });
            }
            supp.icon = "fa-tasks";
            supp.type = "support";
            this.timelineInfo.push(supp)
            this.sortTimeline();
            this.indexExpand = -1;
            this.subStringContent();
          });
        });
      }
      this.isLoaded = true;
    }
  }
  getBusinessInfo(companyId, opportunityId) {
    return this.crmActivityBusinessService.getBusinessInfo(companyId, opportunityId)
  }
  getSupportInfo(companyId, projectId) {
    return this.crmActivitySupportService.getSupportInfo(companyId, projectId)
  }
  getEmailInfo(companyId, inputFuncId, funcRefCd) {
    return this.crmActivityEmailService.getEmailInfo(companyId, inputFuncId, funcRefCd)
  }
  getTelInfo(companyId, inputFuncId, funcRefCd) {
    return this.crmActivityTelService.getTelInfo(companyId, inputFuncId, funcRefCd)
  }
  getMeetingInfo(companyId, inputFuncId, funcRefCd) {
    return this.crmActivityMeetingService.getMeetingInfo(companyId, inputFuncId, funcRefCd)
  }
  getIssueInfo(companyId, inputFuncId, funcRefCd) {
    return this.crmActivityIssueService.getIssueInfo(companyId, inputFuncId, funcRefCd)
  }
  sortTimeline() {
    this.timelineInfo = this.timelineInfo.sort((a, b) => (a.created_time < b.created_time) ? 1 : ((b.created_time < a.created_time) ? -1 : 0));
    this.ShowLoadingCenter = false;
  }
  subStringContent() {
    $(document).ready(function () {
      $(".content-title").text(function (index, currentText) {
        var maxLength = $(this).attr('data-maxlength');
        if (currentText.length >= maxLength) {
          return currentText.substr(0, maxLength) + "...";
        } else {
          return currentText
        }
      });
    })
  }
  Expand(index) {
    this.indexExpand == index ? this.indexExpand = -1 : this.indexExpand = index;
    this.subStringContent();
  }
  collapse() {
    this.indexExpand = -1;
    this.subStringContent();
  }
  openPopup(item) {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    if (item.type == 'support') {
      this.supportJson = item;
      this.Id = item.project_task_id;
      this.modalRef = this.modalService.show(this.popupSupport, config);
    }
    if (item.type == 'business') {
      this.bussinessJson = item;
      this.Id = item.activity_id;
      this.SliderdefaultVal = item.possibility;
      this.modalRef = this.modalService.show(this.popupBusiness, config);
    }
    if (item.type == 'email') {
      this.emailJson = item;
      this.Id = item.email_id;
      this.modalRef = this.modalService.show(this.popupEmail, config);
    }
    if (item.type == 'tel') {
      this.telJson = item;
      this.Id = item.tel_id;
      this.modalRef = this.modalService.show(this.popupTelephone, config);
    }
    if (item.type == 'meeting') {
      this.meetingJson = item;
      this.Id = item.meeting_id;
      this.modalRef = this.modalService.show(this.popupConference, config);
    }
    if (item.type == 'issue') {
      this.issueJson = item;
      this.Id = item.issue_id;
      this.modalRef = this.modalService.show(this.popupIssue, config);
    }
  }
  closePopup() {
    this.modalRef && this.modalRef.hide();
  }
  reloadData(fromChild) {
    this.isLoaded = false;
    this.getTimelineInfo(this.companyId, this.inputFuncId, this.funcRefCd, true, this.salesoptNm);
    if (fromChild == 'business') {
      this.childCall.emit('business');
    }
  }
}
