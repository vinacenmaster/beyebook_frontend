import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { CrmActivitySupportService } from '@app/core/services/crm/customer-detail.service';
import { AuthService, UserMasterService } from '@app/core/services';
import { TraderService } from '@app/core/services/features.services/trader-master.service';
import { ContactorService } from '@app/core/services/features.services/contactor-master.service';
import { BasePage } from '@app/core/common/base-page';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { Category } from '@app/core/common/static.enum';

@Component({
  selector: 'sa-support-detail',
  templateUrl: './support-detail.component.html',
  styleUrls: ['../../common-activity.css']
})
export class SupportDetailComponent extends BasePage implements OnInit {
  @Output() childCall = new EventEmitter();
  @ViewChild("popupSupport") popupSupport;
  modalRef: BsModalRef;
  supportInfo: any[] = [];
  userLogin: any;
  companyId : any;
  contactor: any[] = [];
  projectId :number = 0;
  user: any[] = [];
  supportJson : any;
  Id : any;
  indexExpand: number = -1;
  taskType :any[] = [];
  workUnit :any[] = [];
  total : number = 0;
  ShowLoadingCenter:boolean=true;

  constructor(
    private crmActivitySupportService: CrmActivitySupportService,
    private modalService: BsModalService,
    public userService: AuthService,
    private generalMasterService: GeneralMasterService,
  ) {
    super(userService);
  }

  ngOnInit() {
    this.userLogin = this.userService.getUserInfo();
    this.getTaskType(), this.getWorkUnit();
  }
  getTaskType(){
    this.generalMasterService.listGeneralByCate(Category.TaskType.valueOf()).then(data =>{
      this.taskType.push(...data);
    });
  }
  getWorkUnit(){
    this.generalMasterService.listGeneralByCate(Category.ItemUnit.valueOf()).then(data =>{
      this.workUnit.push(...data);
    });
  }
  getSupportInfo(companyId,projectId,contactor,user) {
    this.ShowLoadingCenter = true;
    this.companyId = this.userLogin.company_id;
    this.projectId = projectId;
    this.contactor = contactor;
    this.user = user;
    this.crmActivitySupportService.getSupportInfo(this.companyId,projectId).then(data => {
      if (data.data.length == 0) return this.ShowLoadingCenter = false;
      data.data.forEach((supp,index) => {
        this.taskType.forEach(pt => {
          if (supp.project_task_type_gen_cd == pt.gen_cd) {
            supp.project_task_type_nm = pt.gen_nm;
          }
        });
        this.workUnit.forEach(unit => {
          if (supp.work_unit_gen_cd == unit.gen_cd) {
            supp.work_unit_gen_nm = unit.gen_nm;
          }
        });
        if (supp.project_task_person) {
          supp.project_task_person.forEach(per => {
            if (per.worker_yn) {
              if (per.person_type) {//user
                 this.user.forEach(con => {
                  if (per.person_id == con.user_id) {
                    per.person_nm = con.user_nm;
                  }
                }); 
              }else{
                this.contactor.forEach(con => {
                  if (per.person_id == con.contactor_id) {
                    per.person_nm = con.contactor_nm;
                  }
                }); 
              }
            }
          });
        }
        if (index == data.data.length - 1) {
          this.supportInfo = data.data;
          this.total = data.total;
          this.ShowLoadingCenter = false;
        }
      });
      this.indexExpand = -1;
      this.subStringContent();
    })
  }
  subStringContent(){
    $(document).ready(function(){
      $(".content-title").text(function(index, currentText) {
        var maxLength = $(this).attr('data-maxlength');
          if(currentText.length >= maxLength) {
            return currentText.substr(0, maxLength) + "...";
          } else {
            return currentText
          } 
      });
    })
  }
  Expand(index){
    this.indexExpand == index? this.indexExpand = -1 : this.indexExpand = index;
    this.subStringContent();
  }
  collapse(){
    this.indexExpand = -1;
    this.subStringContent();
  }
  openPopup(item) {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.supportJson = item;
    this.Id = item.project_task_id;
        this.modalRef = this.modalService.show(this.popupSupport, config);
    
  }
  closePopup() {
    this.modalRef && this.modalRef.hide();
  }
  reloadData(fromChild) {
    this.getSupportInfo(this.companyId, this.projectId,this.contactor,this.user);
    this.childCall.emit('support');
  }
}
