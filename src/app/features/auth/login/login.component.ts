import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "@app/core/services/auth.service";
import { AuthUserModel } from "@app/core/models/auth_user.model";
import { NotificationService } from "@app/core/services/notification.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  loading:boolean=false;
  model: AuthUserModel;
  submitted = false;
  loginSuccess: boolean = false;
  errMessage: string = '';
  returnUrl: string;
  constructor(private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private notification: NotificationService) {
    this.model = new AuthUserModel("", "");
  }

  ngOnInit() {
    //reset login status
    this.authService.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.model.username = this.model.password = 'demo';
  }

  onSubmit() {
    this.errMessage='';
    this.submitted = true;
    this.loading=true;
    this.authService.login(this.model.username, this.model.password).subscribe(data => {
      this.loading=false;
      if (data.status) {
        this.router.navigate([this.returnUrl]);
      }
      else {
        this.errMessage = data.message;
        this.notification.showMessage('error', this.errMessage)
      }
    });
  }

}
