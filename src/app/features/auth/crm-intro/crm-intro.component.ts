import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crm-intro',
  template: '<router-outlet></router-outlet>',
})
export class CrmIntroComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
