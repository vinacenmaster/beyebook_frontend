import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmIntroComponent } from './crm-intro.component';
import { routing } from './crm-intro.routing';

@NgModule({
  imports: [
    CommonModule,

    routing,
  ],
  declarations: [ CrmIntroComponent]
})
export class CrmIntroModule { }
