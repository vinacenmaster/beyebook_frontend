import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrmHomepageComponent } from './crm-homepage.component';

const routes: Routes = [
  {
    path: '',
    component: CrmHomepageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrmHomepageRoutingModule { }
