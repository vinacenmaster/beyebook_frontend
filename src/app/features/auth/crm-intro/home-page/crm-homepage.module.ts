import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrmHomepageRoutingModule } from './crm-homepage-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SmartadminValidationModule } from '@app/shared/forms/validation/smartadmin-validation.module';
import { CrmHomepageComponent } from './crm-homepage.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CrmHomepageRoutingModule,
    SmartadminValidationModule,
    ReactiveFormsModule
  ],
  declarations: [CrmHomepageComponent]
})
export class CrmHomepageModule { }
