
import {ModuleWithProviders} from "@angular/core"
import {Routes, RouterModule} from "@angular/router";


export const routes:Routes = [
  {
    path: 'home-page',
    loadChildren: './home-page/crm-homepage.module#CrmHomepageModule'
  },
];

export const routing = RouterModule.forChild(routes);
