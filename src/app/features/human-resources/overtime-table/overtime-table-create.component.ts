import { Component, OnInit, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { HrMasOvertimeTableModel, HrOvertimeTableDetailModel } from '@app/core/models/hr/hr-overtime-table.model';
import { NotificationService } from '@app/core/services/notification.service';
import { HrOvertimeTableService } from '@app/core/services/hr.services/hr-overtime-table.service';
import { AuthService } from '@app/core/services/auth.service';
import { BasePage } from '@app/core/common/base-page';
import { I18nService } from '@app/shared/i18n/i18n.service';
import { CRMSolutionApiService } from '@app/core/services';


@Component({
    selector: 'sa-overtime-table-create',
    templateUrl: './overtime-table-create.component.html',
  })

  export class OvertimeTableCreateComponent extends BasePage implements OnInit {
    overtimeTableInfo: HrMasOvertimeTableModel;
    overTimeTableMax : any;
    isOTTableExisted = false;
    @Input() maxOverTableId : number = 0;
    @Output() onCreatedNew = new EventEmitter<any>();
    ngOnInit() {
        this.overtimeTableInfo = new HrMasOvertimeTableModel;
        this.overtimeTableInfo.company_id = this.companyInfo.company_id;
    }

    constructor(
        private api: CRMSolutionApiService,
        private notification: NotificationService,
        private hrOvertimeTableService: HrOvertimeTableService,
        public userService: AuthService
    ) {
        super(userService);
    }

    public validationOptions: any = {
        ignore: [], //enable hidden validate
        // Rules for form validation
        rules: {
          ot_table_id: {
            required: true
          }
        },
        // Messages for form validation
        messages: {
          ot_table_id: {
            required: "*"
          },
        }
      };

    insertOvertimeTable() {
        this.hrOvertimeTableService.InsertOvertimeTable(this.overtimeTableInfo).then(data => {
          console.log("data",data)
          if (data.error) {
            this.notification.showMessage("error", data.error.message);
          } else {
            this.notification.showMessage("success", data.message);
            //this.overtimeTableInfo.ot_table_id = data.data.ot_table_id;
            if(this.onCreatedNew){
              this.onCreatedNew.emit();
            }
          }
        });
      }
  }