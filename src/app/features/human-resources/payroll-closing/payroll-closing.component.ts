import { Component, OnInit } from '@angular/core';
import { BasePage } from '@app/core/common/base-page';
import { NotificationService, AuthService, ProgramService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { Category } from '@app/core/common/static.enum';
import { GeneralMasterModel } from '@app/core/models/general_master.model';
import { PayrollClosingModel } from '@app/core/models/hr/hr-payroll-closing.model';
import { CommonFunction } from '@app/core/common/common-function';

@Component({
  selector: 'sa-payroll-closing',
  templateUrl: './payroll-closing.component.html',
  styleUrls: ['./payroll-closing.component.css']
})
export class PayrollClosingComponent extends BasePage implements OnInit {

  salaryKind: GeneralMasterModel[] = [];
  model : PayrollClosingModel = new PayrollClosingModel;
  monthsYear: any = [];
  options: any;
  validationOptions:any={}
  constructor( 
    public userService: AuthService,
    private generalMasterService: GeneralMasterService,
    public programService: ProgramService,
    private notification: NotificationService) {
    super(userService);
  }

  ngOnInit() {
    this.monthsYear = this.loadYearMonth();
    this.getSalaryKind().then(data => {
      this.salaryKind.push(...data);
    });
  }

  private getSalaryKind() {
    return this.generalMasterService.listGeneralByCate(Category.SalaryKind.valueOf())
  }

   loadYearMonth() {
    let monthsYear: any = [];
    var d1 = new Date().addMonths(-1);
    var d2 = new Date().addMonths(11);
    while (d2 > d1) {
      d1 = d1.addMonths(+1);
      monthsYear.push({ text: d1.toString('yyyy-MM'), val: d1.toString('yyyy-MM-dd') });
    }
    return monthsYear;
  }

  // getMonthYear(value) {
  //   let date = new Date(value);
  //   this.model.month = date.getMonth() + 1;
  //   this.model.year = date.getFullYear();
  // }

  private initDatatable() {
    this.options = {
      dom: "Bfrtip",
      ajax: (data, callback, settings) => {
       
      },
      columns: [
        { data: "index", className: "center", width: "40px" },
        {
          data: (data, type, dataToSet) => {
            var o = this.salaryKind.filter(x => x.gen_cd === data.type_gen_cd);
            if (o.length > 0) return o[0].gen_nm;
            else return "N/A";
          }, className: "", width: "150px"
        },
        { data: "last_created", className: "right", width: "80px" },
        { data: "number_of_created", className: "", width: "80px" },
        { data: "target", className: "", width: "80px" },
        { data: "created", className: "", width: "80px" },
        { data: "failed", className: "", width: "80px" },
        { data: "description", className: "center",width: "200px"},
      ],
      scrollY: 210,
      scrollX: true,
      paging: false,
      buttons: [
        {
          text: '<i class="fa fa-refresh" title="Refresh"></i>',
          action: (e, dt, node, config) => {
            dt.ajax.reload();
            
          }
        },
        {
          extend: "selected",
          text: '<i class="fa fa-times text-danger" title="Delete"></i>',
          action: (e, dt, button, config) => {
            var rowSelected = dt.row({ selected: true }).data();
            if (rowSelected) {
              this.notification.confirmDialog(
                "Delete Salary Confirmation!",
                `Are you sure to delete ?`,
                x => {
                  
                }
              );
            }
          }
        },
        "copy",
        "excel",
        "pdf",
        "print"
      ]
    };
  }

  onRowClick(e){

  }
  onSubmit(){
    
  }

  onCloseProgram() {
    this.programService.closeCurrentProgram();
  }
}
