import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { BasePage } from "@app/core/common/base-page";
import { I18nService } from "@app/shared/i18n/i18n.service";
import {
  NotificationService,
  AuthService,
  OrganizationMasterService
} from "@app/core/services";
import { BsModalRef, BsModalService } from "ngx-bootstrap";
import { AttendanceService } from "@app/core/services/hr.services/hr-attendance.service";
import { CommonFunction } from "@app/core/common/common-function";

@Component({
  selector: "hr-attendance-person",
  templateUrl: "./attendance-person.component.html",
  styleUrls: ["./attendance-person.component.css"]
})
export class AttendancePersonComponent extends BasePage implements OnInit {
  modalRef: BsModalRef;
  employees: any[] = [];
  attendanceLogs: any = [];
  specialAttendanceLogs: any = [];
  hrid: string = "";
  department: string = "";
  from_record: number = 0;
  to_record: number = 0;
  page: number = 1;
  pageSize: number = 20;
  totalItems: number = 0;
  attendances: any[] = [];
  employeeName: string = "";
  isShowSetting: boolean = false;
  startdate: string;
  finishdate: string;
  labels: any = {
    previousLabel: this.i18n.getTranslation("SPREVIOUS"),
    nextLabel: this.i18n.getTranslation("SNEXT")
  };
  setting: any = {
    in_time: "",
    out_time: "",
    reset_name: "",
    description: ""
  };

  @ViewChild("popupSetting") popupSetting;
  @ViewChild("frmDetail") frmDetail: ElementRef;

  constructor(
    public i18n: I18nService,
    private notification: NotificationService,
    private modalService: BsModalService,
    public userService: AuthService,
    public organizationMasterService: OrganizationMasterService,
    public attendanceByPersonService: AttendanceService
  ) {
    super(userService);
  }

  ngOnInit() {
    this.attendanceByPersonService.getEmployees().then(d => {
      this.employees = d.data;
      this.initSelectize(this.employees);
    });
    this.attendanceByPersonService.getAttendanceLog().then(d => {
      this.attendanceLogs = d.data;
      this.specialAttendanceLogs = this.attendanceLogs.filter(function(data) {
        return data.category == "4";
      });
    });
    this.displayRecord();
  }

  initSelectize(employees) {
    var self = this;
    $("#select-employee").selectize({
      maxItems: 1,
      sortField: "text",
      valueField: "hr_id",
      searchField: ["employee_name", "department", "position"],
      options: employees,
      plugins: {
        dropdown_header: {
          title:
            "<table><tr>" +
            '<td class="employee-name"><strong>' +
            this.i18n.getTranslation("ATTEND_PERSON_LBL_HEADER_EPL_NAME") +
            "</strong></td>" +
            '<td class="department"><strong>' +
            this.i18n.getTranslation("ATTEND_PERSON_LBL_HEADER_DEPARTMENT") +
            "</strong></td>" +
            '<td class="position"><strong>' +
            this.i18n.getTranslation("ATTEND_PERSON_LBL_HEADER_POSITION") +
            "</strong></td>" +
            "</tr></table>"
        }
      },
      render: {
        option: function(data, escape) {
          return (
            '<div class="row">' +
            '<div class="row-employee-name">' +
            escape(data.employee_name) +
            "</div>" +
            '<div class="row-department">' +
            escape(data.department) +
            "</div>" +
            '<div class="row-position">' +
            escape(data.position) +
            "</div>" +
            "</div>"
          );
        },
        item: function(data, escape) {
          if (data.hr_id > 0) {
            return '<div class="item">' + escape(data.employee_name) + "</div>";
          }
        }
      },
      onChange(value) {
        self.hideSetting();
        employees.filter(function(e) {
          if (e.hr_id == value) {
            self.hrid = value;
            self.department = e.department;
          }
        });
      },
      create: false
    });
  }

  hideSetting() {
    this.isShowSetting = false;
  }

  reset() {
    this.attendances = [];
    this.page = 1;
    this.totalItems = 0;
    this.employeeName = "";
  }

  searchAttendance(isChange) {
    if (this.startdate && this.finishdate) {
      if (!isChange) this.page = 1;
      let model = {
        hrid: this.hrid,
        from_date: new Date(this.startdate).toJSON(),
        to_date: new Date(this.finishdate).toJSON(),
        page_size: this.pageSize,
        page: this.page
      };
      this.attendanceByPersonService.getAttendanceByPerson(model).then(data => {
        if (data.data) {
          this.attendances = data.data;
          this.totalItems = data.total;
          this.isShowSetting = true;
          this.displayRecord();
        } else {
          this.reset();
        }
      });
    } else {
      this.notification.showMessage(
        "error",
        this.i18n.getTranslation("DAILYATTEND_MSG_DATE_REQUIRED")
      );
    }
  }

  displayRecord() {
    var totalPage = Math.ceil(this.totalItems / this.pageSize);
    if (this.totalItems < this.pageSize && this.totalItems > 0) {
      this.from_record = 1;
      this.to_record = this.totalItems;
    } else if (this.page == totalPage) {
      this.from_record = (this.pageSize * (this.page-1)) + 1;
      this.to_record = this.totalItems;
    } else if(this.totalItems > 0){
      if (this.page == 1) {
        this.from_record = 1;
        this.to_record = this.pageSize;
      } else {
        this.from_record = (this.pageSize * (this.page-1)) + 1;
        this.to_record = this.from_record + this.pageSize -1;
      }
    }
    else{
        this.from_record = 0;
        this.to_record = 0;
    }
  }

  changePage(page) {
    this.page = page;
    this.searchAttendance(true);
  }

  saveChange() {
    this.attendanceByPersonService
      .saveEmployeeAttendance(this.attendances)
      .then(data => {
        if (!data.success) {
          this.notification.showMessage("error", data.message);
        } else {
          this.notification.showMessage(
            "success",
            this.i18n.getTranslation("DAILYATTEND_MSG_SAVE_SUCCESSUL")
          );
        }
      });
  }

  showSettingPopup() {
    let config = {
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.attendanceByPersonService.getSetting().then(data => {
      if (data.data) {
        this.setting = data.data;
      } else {
        this.setting = {
          in_time: "",
          out_time: "",
          reset_name: "",
          description: ""
        };
      }
    });
    this.modalRef = this.modalService.show(this.popupSetting, config);
  }

  saveSetting() {
    if (
      this.setting &&
      this.setting.reset_name &&
      ((this.setting.in_time && this.setting.in_time != "") ||
        (this.setting.out_time && this.setting.out_time != ""))
    ) {
      var in_time = Date.parse(
        new Date().toString("M/d/yyyy") + " " + this.setting.in_time
      );
      var out_time = Date.parse(
        new Date().toString("M/d/yyyy") + " " + this.setting.out_time
      );
      this.setting.in_time =
        CommonFunction.pad2(in_time.getHours()) +
        ":" +
        CommonFunction.pad2(in_time.getMinutes());
      this.setting.out_time =
        CommonFunction.pad2(out_time.getHours()) +
        ":" +
        CommonFunction.pad2(out_time.getMinutes());
      this.attendances.forEach(function(item, index, arr) {
        item.in_time =
          CommonFunction.pad2(in_time.getHours()) +
          ":" +
          CommonFunction.pad2(in_time.getMinutes());
        item.out_time =
          CommonFunction.pad2(out_time.getHours()) +
          ":" +
          CommonFunction.pad2(out_time.getMinutes());
      });
      this.attendanceByPersonService.saveSetting(this.setting).then(data => {
        if (data.success) {
          this.saveChange();
          this.closeFactoryPopup();
        } else {
          this.notification.showMessage("error", data.message);
        }
      });
    } else {
      this.notification.showMessage(
        "error",
        this.i18n.getTranslation("DAILYATTEND_MSG_TIME_NAME_REQUIRED")
      );
    }
  }

  deleteSetting() {
    this.attendanceByPersonService
      .deleteSetting(this.setting.reset_id)
      .then(data => {
        if (data.success) {
          this.notification.showMessage(
            "success",
            this.i18n.getTranslation("DAILYATTEND_MSG_DELETE_SUCCESSFUL")
          );
          this.setting = {};
          this.closeFactoryPopup();
        } else {
          this.notification.showMessage("error", data.message);
        }
      });
  }

  closeFactoryPopup() {
    this.modalRef && this.modalRef.hide();
  }
}
