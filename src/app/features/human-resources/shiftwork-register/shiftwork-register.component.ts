import { Component, OnInit } from '@angular/core';
import { OrganizationMasterService, NotificationService, AuthService } from '@app/core/services';
import { GeneralMasterService } from '@app/core/services/features.services/general-master.service';
import { BasePage } from '@app/core/common/base-page';
import { OrganizationModel, FactoryModel } from '@app/core/models/organization.model';
import { HrShiftworkService } from '@app/core/services/hr.services/shiftwork.service';
import { HrShiftworkModel } from '@app/core/models/hr/hr-shiftwork.model';
import 'datejs'
import { GeneralMasterModel } from '@app/core/models/general_master.model';
import { Category } from '@app/core/common/static.enum';
import { getDay } from 'ngx-bootstrap/chronos/utils/date-getters';
@Component({
  selector: 'sa-shiftwork-register',
  templateUrl: './shiftwork-register.component.html',
  styleUrls: ['./shiftwork-register.component.css']
})
export class ShiftworkRegisterComponent extends BasePage implements OnInit {
  //factories: FactoryModel[] = [];
  departs: OrganizationModel[] = [];
  dutyTypes: GeneralMasterModel[] = [];
  monthsYear: any = [];
  shiftworkInfo: HrShiftworkModel = new HrShiftworkModel();
  shiftWorkTable: any;
  isCreated: boolean;
  hasChanged: boolean = false;
  savingAllChanges: boolean = false;
  daysInMonth: number;
  isChangingDutyType: boolean = false;
  changingHrId: any;
  changingDay: any;
  currentDutyType: any;
  selectedRowIndex:any;
  totalRows:number=0;
  constructor(
    private notification: NotificationService,
    private orgService: OrganizationMasterService,
    private shiftworkService: HrShiftworkService,
    public userService: AuthService,
    private generalMasterService: GeneralMasterService) {
    super(userService);

  }
  private loadDutyType() {
    this.generalMasterService.listGeneralByCate(Category.DutyType.valueOf()).then(data => {
      this.dutyTypes.push(...data);
    });
  }
  private loadYearMonths() {
    var d1 = new Date().addMonths(-6)
    var d2 = new Date().addMonths(6);
    var today = Date.today();
    while (d2 > d1) {
      d2 = d2.addMonths(-1);
      //console.log(d2.toString('yyyy-MM'))
      var ym = d2.toString('yyyy-MM');
      if (ym == today.toString('yyyy-MM')) {
        this.shiftworkInfo.month = today.getMonth() + 1;
        this.shiftworkInfo.year = today.getFullYear();
        this.daysInMonth = Date.getDaysInMonth(today.getFullYear(), today.getMonth());
        this.shiftworkInfo.month_year = ym;
      }
      this.monthsYear.push({ text: ym, val: ym });
    }
  }
  // private loadFactories() {
  //   return this.orgService.listFactory(this.companyInfo.company_id)
  // }
  private loadDepart() {
    return this.orgService.listOrganization(this.companyInfo.company_id)
  }
  ngOnInit() {
    this.shiftworkInfo.company_id = this.companyInfo.company_id;
    this.loadYearMonths();
    // this.loadFactories().then(data => {
    //   this.factories.push(...data);
    // });
    this.loadDepart().then(data => {
      this.departs.push(...data);
    });
    this.loadDutyType();

  }
  searchData() {
    console.log(this.shiftworkInfo)

    if (this.shiftworkInfo.depart_id > 0 && this.shiftworkInfo.month > 0 && this.shiftworkInfo.year > 0) {
      //console.log(factoryId,departId,m,y)
      this.shiftworkService.getShiftWorkTable(this.companyInfo.company_id, this.shiftworkInfo.depart_id, this.shiftworkInfo.month, this.shiftworkInfo.year).then(data => {

        if (data.error) {
          this.notification.showMessage("error", data.error.message);
          return;
        }
        if (!data.created) {
          this.notification.showMessage("info", data.message);
          this.isCreated = false;
          //this.shiftworkInfo=new HrShiftworkModel();
          this.shiftWorkTable = [];
          return;
        }
        this.isCreated = true;
        this.shiftworkInfo = data.data.shiftwork_info;
        this.shiftWorkTable = data.data.shiftwork_table;
        this.totalRows=data.data.shiftwork_table.length;
        //this.notification.showMessage("success", 'Shift-work table was created successfully', { titmeOut: 1000 });
        //console.log(this.shiftworkInfo, this.shiftWorkTable)
      });
    }
    else {
      this.notification.showInfo('Please select factory & department', { titmeOut: 1000 });
    }
  }
  createShiftwork() {
    console.log(this.shiftworkInfo)
    if (this.shiftworkInfo.depart_id > 0 && this.shiftworkInfo.month > 0 && this.shiftworkInfo.year > 0) {
      this.notification.confirmDialog('Shift-work table create confirmation', `Do you want to create shift-work table for ${this.shiftworkInfo.month_year}?`, (x) => {
        if (x) {
          this.shiftworkService.saveShiftworkInfo(this.shiftworkInfo).then(data => {
            if (data.error) {
              this.notification.showMessage("error", data.error.message);
              return;
            }
            this.searchData();
          })
        }
      })
    }
    else {
      this.notification.showMessage("error", 'Data invalid');
    }
  }
  onMonthChanged(my) {
    var m = my.split('-')[1];
    var y = my.split('-')[0];
    this.shiftworkInfo.month = m;
    this.shiftworkInfo.year = y;
    this.daysInMonth = Date.getDaysInMonth(y, m);
  }

  editADay(el, hrId, day, val) {
     var cellElement = $(el.target)
    if (cellElement.hasClass('cell-selected') || cellElement.parent().hasClass('cell-selected')) {
      return;
    }
     $("#tblShiftwork td.cell-editable.cell-selected").removeClass('cell-selected')
    // var oldSelectedCell = $("#tblShiftwork td.cell-editable").find('input');
    // if (oldSelectedCell.length > 0) {
    //   let ov = $(oldSelectedCell[0]).val();
    //   $(oldSelectedCell[0]).parent().html(ov)
    // }

    cellElement.addClass('cell-selected')
    this.isChangingDutyType = true;
    this.changingHrId = hrId;
    this.changingDay = day;
    this.currentDutyType = cellElement.text();

    this.selectedRowIndex = parseInt(cellElement.parent().find('td:first').text()) - 1;
    console.log('selectedRowIndex',this.selectedRowIndex)
  }
  resetCellsChanged() {
    this.notification.confirmDialog('Shift-work table clear changes confirmation', `Do you want to clear all changes?`, (x) => {
      if (x) {
        this.searchData();
        this.hasChanged = false;
        this.isChangingDutyType=false;
      }
    })
    // $("#tblShiftwork td.cell-editable.cell-selected").removeClass('cell-selected')
    // $("#tblShiftwork td.cell-editable.cell-changed").removeClass('cell-changed').text('')
    // var oldSelectedCell=$("#tblShiftwork td.cell-editable").find('input');
    // if(oldSelectedCell.length>0){
    //   let ov=$(oldSelectedCell[0]).val();
    //   $(oldSelectedCell[0]).parent().html(ov)
    // }
  }

  saveCellsChanged() {
    this.notification.confirmDialog('Shift-work table save changes confirmation', `Do you want to save all changes?`, (x) => {
      if (x) {
        console.log(this.shiftWorkTable)
        this.savingAllChanges = true;
        this.shiftworkService.updateShiftworkTable(this.shiftWorkTable).then(data => {
          if (data.error) {
            this.notification.showMessage("error", data.error.message);
            return;
          }
          this.searchData();
          this.hasChanged = false;
          this.savingAllChanges = false;
           this.isChangingDutyType=false;
        })
      }
    })
  }

  onDutyTypeChanged(e) {
   
    var id = $("select[name='number_value_1']").val();
    if (id != this.currentDutyType) {
      this.copyNewValue(id);
      this.currentDutyType=id;
    }
  }
  copyNewValue(id) {
    var val = id;
    var day = this.changingDay;
    for (var i = this.selectedRowIndex; i < this.shiftWorkTable.length; i++) {
      var monthData = this.shiftWorkTable[i].monthly_data;
      var nextDays = monthData.filter(x => x.day >= day);
      if (nextDays.length > 0) {
        $.each(nextDays, (i, item) => {
          item.duty_type_code = val;
          let dtype = this.dutyTypes.filter(x => x.number_value_1 == val);
          if (dtype.length > 0) {
            item.duty_type_gen_cd = dtype[0].gen_cd;
            item.duty_type_name = dtype[0].gen_nm;
          }
          else {
          }
        })
        var td
        let nextTr = $("#tblShiftwork").find(`tr[id=${i + 1}]`)
          td = nextTr.find(`td[data-day=${day}]`)
        td.html(val);
        td.addClass('cell-changed');
        td.removeClass('cell-selected')
        var allTd = td.nextAll();
        $.each(allTd, function (i, e) {
          $(e).addClass('cell-changed');
        })
        if (!this.hasChanged) this.hasChanged = true;
      }
    }
  }
}

