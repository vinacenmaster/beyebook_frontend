import {Directive, ElementRef, OnInit, EventEmitter, Output, OnChanges} from '@angular/core';


@Directive({
  selector: '[smartTimepicker]'
})
export class SmartTimepickerDirective implements OnInit, OnChanges{

  @Output() ngModelChange: EventEmitter<any> = new EventEmitter(false);
  constructor(private el: ElementRef) { }

  ngOnInit(){
    import('bootstrap-timepicker/js/bootstrap-timepicker.min.js').then(()=>{
      this.render()
    })
  }


  render(){
    $(this.el.nativeElement).timepicker();
    
    $(this.el.nativeElement).on("change",(e)=>{
      console.log('change',e)
      if(this.ngModelChange){
        console.log('ngModelChange')
        this.ngModelChange.emit(e.currentTarget.value);
      }
    })

    if(this.el.nativeElement.value)
    {
      $(this.el.nativeElement)
        .val(this.el.nativeElement.value)
        .trigger("change");
    }
  }

  ngOnChanges(changes) {
   console.log(` ngOnChanges: ${JSON.stringify(changes)}`)
    if (!changes.firstChange) {
      let val;
      if (changes.defaultVal != null && changes.defaultVal !== undefined) {
        val = changes.defaultVal.currentValue;
      }

      if (val == null || val === undefined) val = "";
      $(this.el.nativeElement)
        .val(val)
        .trigger("change");
    }
  }
}
