import { Component, OnInit } from '@angular/core';
import { CRMSolutionApiService, NotificationService } from '@app/core/services';
import { ChangePassModel } from '@app/core/models/user.model';

@Component({
  selector: 'sa-change-pass',
  templateUrl: './change-pass.component.html'
})
export class ChangePassComponent implements OnInit {
  model:ChangePassModel=new ChangePassModel();
  public resetValidationOptions:any= {
    rules: {
      password: {
        required: true
      },
      new_password: {
        required: true
      },
      confirm_password:{
        required: true,
        equalTo: "#newPass"
      }
    },
    messages: {
      password: {
        required: "Please enter current password"
      },
      new_password: {
        required: "Please enter new password"
      },
      confirm_password: {
        required: "Please confirm password",
        equalTo:"Confirm password is not match"
      },
    }
  };
  constructor(private api: CRMSolutionApiService,
    private notification: NotificationService){
    
  }
  
  ngOnInit(): void {
    
  }

  onSubmit() {
    this.api.post("user/change-pw", this.model).subscribe(data => {
      if (!data.success) {
        this.notification.showMessage("error", data.data.message);
      } else {
        this.notification.showMessage("success", data.data.message);
      }
    });
  }

}
