import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProgramService } from '@app/core/services/program.service';
import { ProgramModel } from '@app/core/models/program.model';
import { object } from '@amcharts/amcharts4/core';
import { isArray } from 'util';

declare var $: any;

@Component({
  selector: 'sa-footer',
  templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit, AfterViewInit {
  currentYear: number = new Date().getFullYear();
  openedPrograms: Array<ProgramModel> = [];
  private $menu: any;
  isFromIframe:boolean = false;
  constructor(private programService: ProgramService,
    private router: Router) { }

  ngOnInit() {
    this.$menu = $('#navLeftMenu');
    this.programService.getOpenedPrograms
      .subscribe(
        (programs: Array<ProgramModel>) => {
          //console.log('subscribe getOpenedPrograms',programs)
          this.openedPrograms = programs;
          // console.log("footer_programs",programs)
        }
      );
      window.addEventListener('message', this.listenFromIframe);
    this.programService.refreshOpenedPrograms();
  }
  listenFromIframe = (e) => {
    if (isArray(e.data)){
      this.openedPrograms = e.data;
      this.isFromIframe = true;
    }
  };
  ngAfterViewInit() {
    this.programService.addAnimation();
  }
  activeProg(id){
    // console.log("activeProg",this.openedPrograms)
    if (this.isFromIframe) {
      this.programService.ActiveMenuFromIframe(id,this.openedPrograms);
      this.isFromIframe = false;
    }else this.programService.ActiveMenu(id);

    this.$menu.find("li.active").removeClass("active")
    this.$menu.find("li[id="+id+"]").addClass("active")
  }
  closeProgram(id: number) {
    this.programService.closeOpenedPrograms(id);

    // if (this.openedPrograms.length > 0) {
    //   this.router.navigate([this.openedPrograms[0].url]);
    // } else {
    //   this.router.navigate(['/home']);
    // }
  }

  closeAllPrograms() {
    this.programService.closeAllOpenedPrograms();
  }

  onChange(event) {
    let scale = event.target.value;
    // console.log(event.target.value);
    // console.log($("#drlZomm option:selected" ).text());

    $("body").css("-moz-transform", `scale(${scale}, ${scale})`);
    $("body").css("zoom", `${scale}`);
    $("body").css("zoom", `${scale*100}%`);
  }

}
