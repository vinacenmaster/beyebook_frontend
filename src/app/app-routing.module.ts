import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthGuard } from "@app/core/guards/auth.guard";
import { MainLayoutComponent } from "@app/shared/layout/app-layouts/main-layout.component";
import { AuthLayoutComponent } from "@app/shared/layout/app-layouts/auth-layout.component";
import { EmptyLayoutComponent } from "@app/shared/layout/app-layouts/empty-layout.component";
import { CrmIntroLayoutComponent } from "./shared/layout/app-layouts/crm-intro-layout.component";

const routes: Routes = [
  {
    path: "",
    component: MainLayoutComponent,
    children:[
      {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
      },
      {
        path: 'home',
        loadChildren: 'app/features/home/home.module#HomeModule',
        data: { pageTitle: 'Home' },
        canActivate: [AuthGuard],
      },
    ]
  },
  {
    path: "",
    component: EmptyLayoutComponent,
    // canActivate: [AuthGuard],
    // data: { pageTitle: "Home" },
    children: [
      {
        path: "error",
        loadChildren: "app/features/error/error.module#ErrorModule",
        data: { pageTitle: "Error" }
      },

      // dashboard.
      {
        path: "dashboard",
        loadChildren: "app/features/dashboard/dashboard.module#DashboardModule",
        data: { pageTitle: "Dashboard" }
      },

      // register-system-menu.
      {
        path: 'register-system-menu',
        loadChildren: 'app/features/system/system-menu/system-menu.module#SystemMenuModule',
        data: { pageTitle: 'Register System menu' },
        canActivate: [AuthGuard],
      },

      // register-company.
      {
        path: 'register-company',
        loadChildren: 'app/features/system/company-master/company-master.module#CompanyMasterModule',
        data: { pageTitle: 'Register Company' },
        canActivate: [AuthGuard],
      },
      //group sharing master
      {
        path: 'group-sharing-setting',
        loadChildren: 'app/features/system/group-sharing-setting/group-sharing-setting.module#GroupSharingSettingModule',
        canActivate: [AuthGuard],
      },
      //security item setting
      {
        path: 'security-item-setting',
        loadChildren: 'app/features/system/security-item-setting/security-item-setting.module#SecurityItemSettingModule',
        canActivate: [AuthGuard],
      },
      // register-organization.
      {
        path: 'register-organization',
        loadChildren: 'app/features/system/organization-master/organization-master.module#OrganizationMasterModule',
        data: { pageTitle: 'Register Organization' },
        canActivate: [AuthGuard],
      },

      // register-position.
      {
        path: 'register-position',
        loadChildren: 'app/features/system/position-master/position-master.module#PositionMasterModule',
        data: { pageTitle: 'Register Position' },
        canActivate: [AuthGuard],
      },

      // register-user.
      {
        path: 'register-user',
        loadChildren: 'app/features/system/user-master/user-master.module#UserMasterModule',
        data: { pageTitle: 'Register User' },
        canActivate: [AuthGuard],
      },

      // register-authority-group.
      {
        path: 'register-authority-group',
        loadChildren: 'app/features/system/register-authority-group/register-authority-group.module#RegisterAuthorityGroupModule',
        data: { pageTitle: 'Register Authority Group' },
        canActivate: [AuthGuard],
      },

      // authority-group-menu-setting.
      {
        path: 'authority-group-menu-setting',
        loadChildren: 'app/features/system/authority-group-menu-setting/authority-group-menu-setting.module#AuthorityGroupMenuSettingModule',
        data: { pageTitle: 'Authority Group Menu Setting' },
        canActivate: [AuthGuard],
      },

      // user-authority-group-setting.
      {
        path: 'user-authority-group-setting',
        loadChildren: 'app/features/system/user-authority-group-setting/user-authority-group-setting.module#UserAuthorityGroupSettingModule',
        data: { pageTitle: 'User Authority Group Setting' },
        canActivate: [AuthGuard],
      },

      // general-master.
      {
        path: 'general-master',
        loadChildren: 'app/features/system/general-master/general-master.module#GeneralMasterModule',
        data: { pageTitle: 'General Master' },
        canActivate: [AuthGuard],
      },

      // language-setting.
      {
          path: 'language-setting',
          loadChildren: 'app/features/system/language-master/language-master.module#LanguageMasterModule',
          data: { pageTitle: 'Language Setting' },
          canActivate: [AuthGuard],
      },

      // geneal-sys-master
      {
        path: 'general-sys-master',
        loadChildren: 'app/features/system/general-sys-master/general-sys-master.module#GeneralSysMasterModule',
        data: { pageTitle: 'General Sys Master' },
        canActivate: [AuthGuard],
      },
      //packages
      {
        path: 'package-master',
        loadChildren: 'app/features/system/package-master/package-master.module#PackageMasterModule',
        data: { pageTitle: 'Packages Master' },
        canActivate: [AuthGuard],
      },
      {
        path: 'package-menu',
        loadChildren: 'app/features/system/package-menu/package-menu.module#PackageMenuModule',
        data: { pageTitle: 'Packages Menu' },
        canActivate: [AuthGuard],
      },
      // human-resource.
      
      {
        path: 'trader-master',
        loadChildren: 'app/features/master/trader-master/trader-master.module#TraderMasterModule',
        canActivate: [AuthGuard],
      },
      // crm
      {
        path: 'customer-master',
        loadChildren: 'app/features/crm/customer/customer-master/customer-master.module#CustomerMasterModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'customer-detail/:id',
        loadChildren: 'app/features/crm/customer/customer-detail/customer-detail.module#CustomerDetailModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'contactor-detail/:id',
        loadChildren: 'app/features/crm/contactor/contactor-detail/contactor-detail.module#ContactorDetailModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'crm-contactor',
        loadChildren: 'app/features/crm/contactor/contactor-master/contactor-master.module#ContactorMasterModule',
        canActivate: [AuthGuard],
      },

      {
        path: 'crm-payment',
        loadChildren: 'app/features/crm/order-magt/expenses-magt/expenses-magt.module#ExpensesMagtModule',
        canActivate: [AuthGuard],
      },

      {
        path: 'crm-service-item',
        loadChildren: 'app/features/crm/setting/service-item/service-item.module#ServiceItemModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'overtime-table',
        loadChildren: 'app/features/human-resources/overtime-table/overtime-table.module#HrOvertimeTableModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'work-calendar',
        loadChildren: 'app/features/human-resources/work-calendar/work-calendar.module#WorkCalendarModule',
        canActivate: [AuthGuard],
      },
      
      {
        path: 'news-master', 
        loadChildren: 'app/features/system/news-master/news-master.module#NewsMasterModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-sales-order', 
        loadChildren: 'app/features/crm/order-magt/sales-order/sales-order-master.module#SalesOrderMasterModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-sales-order-detail/:id', 
        loadChildren: 'app/features/crm/order-magt/sales-order/sales-order-detail/sales-order-detail.module#SalesOrderDetailModule', 
        canActivate: [AuthGuard],
      },

      { 
        path: 'crm-purch-order', 
        loadChildren: 'app/features/crm/order-magt/cost-order/cost-order-master.module#CostOrderMasterModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-purch-order-detail/:id', 
        loadChildren: 'app/features/crm/order-magt/cost-order/cost-order-detail/cost-order-detail.module#CostOrderDetailModule', 
        canActivate: [AuthGuard],
      },
      {
        path: 'crm-sales-opportunity', 
        loadChildren: 'app/features/crm/opportunity/opportunity-master/opportunity-master.module#OpportunityMasterModule', 
        canActivate: [AuthGuard],
      },
      {
        path: 'opportunity-detail/:id',
        loadChildren: 'app/features/crm/opportunity/opportunity-detail/opportunity-detail.module#OpportunityDetailModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'crm-project',
        loadChildren: 'app/features/crm/project/project-master/project-master.module#ProjectMasterModule',
        canActivate: [AuthGuard],
      },
      {
        path: 'project-detail/:id',
        loadChildren: 'app/features/crm/project/project-detail/project-detail.module#ProjectDetailModule',
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-contract', 
        loadChildren: 'app/features/crm/contract/contract-master/contract-master.module#ContractMasterModule',         
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-contract-detail/:id', 
        loadChildren: 'app/features/crm/contract/contract-detail/contract-detail.module#ContractDetailModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-target', 
        loadChildren: 'app/features/crm/sales-target/sales-target.module#SalesTargetModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-board', 
        loadChildren: 'app/features/crm/dashboard/dashboard.module#DashboardModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-activity-board', 
        loadChildren: 'app/features/crm/activity-board/activity-board.module#ActivityBoardModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-statistics-board', 
        loadChildren: 'app/features/crm/statistic-board/statistic-board.module#StatisticBoardModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-am-statistic', 
        loadChildren: 'app/features/crm/statistics/am-statistics/am-statistics.module#AmStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-sales-opportunity-statistic', 
        loadChildren: 'app/features/crm/statistics/sale-opportunity-statistics/sale-opportunity-statistics.module#SaleOpportunityStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-expected-sale-statistic', 
        loadChildren: 'app/features/crm/statistics/expected-sales-statistics/expected-sales-statistics.module#ExpectedSalesStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-fixed-financial-statistic', 
        loadChildren: 'app/features/crm/statistics/fixed-financial-statistics/fixed-financial-statistics.module#FixedFinancialStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-sales-by-product-statistic', 
        loadChildren: 'app/features/crm/statistics/sales-by-product-statistics/sales-by-product-statistics.module#SalesByProductStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-sales-by-customer-statistic', 
        loadChildren: 'app/features/crm/statistics/sales-by-customer-statistics/sales-by-customer-statistics.module#SalesByCustomerStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-cost-by-customer-statistic', 
        loadChildren: 'app/features/crm/statistics/cost-by-customer-statistics/cost-by-customer-statistics.module#CostByCustomerStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-sale-by-contactor-statistic', 
        loadChildren: 'app/features/crm/statistics/sales-by-contactor-statistics/sales-by-contactor-statistics.module#SalesByContactorStatisticsModule', 
        canActivate: [AuthGuard],
      },
      { 
        path: 'crm-home', 
        loadChildren: 'app/features/crm/home/home.module#CrmHomeModule', 
        canActivate: [AuthGuard],
      }
    ]
  },

  {
    path: "auth",
    component: AuthLayoutComponent,
    loadChildren: "app/features/auth/auth.module#AuthModule"
  },
  {
    path: "crm",
    component: CrmIntroLayoutComponent,
    loadChildren: "app/features/auth/crm-intro/crm-intro.module#CrmIntroModule",
  },


  { path: "**", redirectTo: "error/error404" }


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  // imports: [RouterModule.forRoot(routes, { useHash: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
