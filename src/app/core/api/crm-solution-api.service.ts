import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { Observable,throwError } from "rxjs";
import { map, delay, catchError } from 'rxjs/operators';
import { environment } from "@env/environment";
import { NotificationService } from '@app/core/services/notification.service';

@Injectable()
export class CRMSolutionApiService {

	constructor(private http: HttpClient) { }

	private getUrl(url) {
		if (url.indexOf('/') == 0) {
			url = url.substring(1);
		}
		return environment.SERVER_API_URL + url;
	}

	public get(url): Observable<any> {
		return this.http.get(this.getUrl(url)).pipe(
			delay(10),
			map((data: any) => (data)),
			catchError(this.handleError)
		);
	}

	public post(url: string, body: any): Observable<any> {
		let _url = this.getUrl(url);
		let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
		let data = JSON.stringify(body);
		// console.log(_url+'\n'+data);
		return this.http.post(_url, data, { headers: headers }).pipe(
			map((data: any) => (data)),
			catchError(this.handleError)
		);
	}

	private handleError(error: any) {
		// In a real world app, we might use a remote logging infrastructure
		// We'd also dig deeper into the error to get a better message
		console.log(error)
		let errMsg = (error.message) ? error.message :
			error.status ? `${error.status} - ${error.statusText}` : 'Server error';
		let errObj = { error: { code: -999, message: errMsg } };
		if(error.status!=401){
			let _notification=new NotificationService();
			_notification.showMessage('error',errMsg)
		}
		return throwError(errObj);
	}

}


