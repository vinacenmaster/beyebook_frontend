import 'datejs';

export class CommonFunction {
    public static b64DecodeUnicode(str) {
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }
    public static loadYearMonths() {
        let monthsYear: any = [];
        var d1 = new Date().addMonths(-12)
        var d2 = new Date().addMonths(12);
        while (d2 > d1) {
            d1 = d1.addMonths(+1);
            monthsYear.push({ text: d1.toString('yyyy-MM'), val: d1.toString('yyyy-MM-dd') });
        }
        return monthsYear;
    }
    public static generateYear(from,to) {
        let year: any = [];
        var y1 = new Date().addYears(from)
        var y2 = new Date().addYears(to);
        while (y2 > y1) {
            y1 = y1.addYears(+1);
            year.push({ text: y1.toString('yyyy'), val: y1.toString('yyyy') });
        }
        return year;
    }
    /**Eg: 2017-05-30 19:26:04->2017-05-30 */
    public static getDateFromDatetime(sdatetime){
        try{
            return sdatetime.split(" ")[0];
        }
        catch(e){
            return sdatetime;
        }
    } 
    public static formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }

    public static FormatMoney(number: any, decimal: number = 2) {
        try {
            var n = Number(number).toFixed(decimal);
            return Number(n).toLocaleString('us')
        }
        catch (e) {
            console.error(e);
            return number;
        }
    }
    public static ReloadDataTable(tableClassName: string) {
        try {
            $("." + tableClassName)
                .DataTable()
                .ajax.reload();
        }
        catch (e) {
            console.error(e);
        }
    }
   
    public static pad2(number) {
        return (number < 10 ? '0' : '') + number;
    }
}
