import { BaseModel, BaseModelWithSharingData } from "@app/core/models/base.model";

export class CostOrderModel extends BaseModelWithSharingData {
        cost_order_id: number = 0;
        company_id: number = 0;
        cost_order_nm: string = "";
        cost_order_atm: number =0;
        memo_text: string = "";
        salesopt_id: number = 0;
        customer_contactor_id: number = 0;
        cost_order_ymd: string = "";
        customer_contactor_nm: string = ''
        list_cost_order_detail: CostOrderDetailModel[] = []
        is_update_cost_order_detail: boolean
        order_type: boolean
        del_yn: boolean = false;
}

export class CostOrderDetailModel extends BaseModel {
        trans_seq: number = 1;
        company_id: number = 0;
        cost_order_id: number = 0;
        crm_item_id: number = 0;
        memo_text: string = "";
        price: number = 0;
        qty: number = 0;
        sub_total_amt: number = 0;
        item_unit_gen_nm: string = "";
        estimate_atm: number = 0;
        del_yn: boolean = false;
}