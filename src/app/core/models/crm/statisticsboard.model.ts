

export class CrmTnsStatisticBoardModel{
    profit_by_admin :number;
    sales_opportunity : number;
    expected_sales_amount :number;
    fixed_financial:number;
    sales_by_customer:number;
    cost_by_customer:number;
    sales_by_contactor:number;
}

export class CrmTnsAMStatisticsModel{
    contract_id :number;
    contract_nm : string;
    contract_amt :number;
    src_order_type:boolean;
    salesopt_id:number;
    salesopt_nm:string;
    sales_admin:string;
    salesopt_type_nm:string;
    contract_start_ymd:Date;
    contract_end_ymd:Date;
}