import { BaseModel } from "@app/core/models/base.model";
import { CrmActivityEmailModel } from "./activity-email.model";
import { CrmActivityTelModel } from "./activity-tel.model";
import { CrmActivityMeetingModel } from "./activity-meeting.model";
import { CrmActivityIssueModel } from "./activity-issue.model";
import { CrmSalesActivityModel } from "./sales-opportunity.model";
import { CrmProjectTaskModel } from "./activity-support.model";

export class CrmRecentyActivityModel extends BaseModel {
    list_emails : CrmActivityEmailModel[] = [];
    list_tels : CrmActivityTelModel[] = [];
    list_meetings : CrmActivityMeetingModel[] = [];
    list_issues : CrmActivityIssueModel[] = [];
    list_business : CrmSalesActivityModel[] = [];
    list_supports : CrmProjectTaskModel[] = [];
}
