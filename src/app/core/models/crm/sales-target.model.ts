import { BaseModelWithSharingData, BaseModel } from "@app/core/models/base.model";

export class CrmMasSalesTargetHeaderModel extends BaseModel {
    company_id: number = 0;
    crm_item_id: number = 0;
    year: number = 0;
    total_target_amt: number = 0;
    sales_target_details : CrmMasSalesTargetDetailModel[] = [];
    parent_nm:string = '';
    crm_service_cate_nm = '';
}
export class CrmMasSalesTargetDetailModel extends BaseModel {
    company_id: number = 0;
    crm_item_id: number = 0;
    year: number = 0;
    month: number = 0;
    monthly_target_amt: number = 0;
    
}
