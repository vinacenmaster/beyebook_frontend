import { BaseModel, BaseModelWithSharingData } from "@app/core/models/base.model";

export class GetOrderModel extends BaseModelWithSharingData {
        company_id: number = 0;
        order_id: number = 0;
        order_nm: string = "";
        order_ymd: string = "";
        total_atm: number = 0;
        salesopt_id: number = 0;
        customer_contactor_id: number = 0;
        customer_contactor_nm: string = ''
        list_order_detail: GetOrderDetailModel[] = []
        is_update_order_detail: boolean
        order_type: boolean

}
export class GetOrderDetailModel extends BaseModel {
        trans_seq: number = 1;
        company_id: number = 0;
        order_id: number = 0;
        crm_item_id: number = 0;
        memo_text: string = "";
        price: number = 0;
        qty: number = 0;
        sub_total_amt: number = 0;
        estimate_atm: number = 0;
        del_yn: boolean = false;
        item_unit_gen_nm: string = "";

}