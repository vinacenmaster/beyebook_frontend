import { BaseModelWithSharingData, BaseModel } from "../base.model";

export class CrmContractModel extends BaseModelWithSharingData {
        company_id : number = 0;
        contract_id : number = 0;
        contract_nm:string = '';
        contract_start_ymd:any;
        contract_end_ymd:any;
        contract_amt:number = 0;
        src_order_type:boolean = false;
        src_order_id:number = 0;
        content_text:string = '';
        del_yn:boolean = false;
        order_nm:string='';
        order_account:string = '';
        order_date:any;
        order_amt:number= 0;
        salesopt_type_nm:string= '';
        salesopt_id:number = 0;
        contract_details : CrmContractDetailModel[] = [];
        sales_admin:string='';
        sales_customer:string='';
}
export class CrmContractDetailModel extends BaseModel {
       company_id : number = 0;
       contract_id : number = 0;
       trans_seq : number = 0;
       issue_ymd:any;
       amt_eta_ymd:any;
       issue_yn:boolean = false;
       issue_amt : number = 0;
       amt_actual_ymd:any;
       del_yn:boolean = false;
       color:string = 'false';
       checkDepositDate:boolean=false;
       issue_month:number=0;
       salesopt_id:number=0;
}