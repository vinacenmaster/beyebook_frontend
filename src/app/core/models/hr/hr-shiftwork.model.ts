
import { BaseModel } from "@app/core/models/base.model";

export class HrShiftworkModel extends BaseModel {
  company_id :number = 0;
  depart_id : number = 0;
  shift_work_id : number = 0;
  month : number=0;
  year : number=0;
  month_year:string;
 }
