export class PayrollClosingModel  {
    salary_kind_type: string = "";
    year: number = 0;
    month: number = 0;
    last_created : number = 0;
    number_of_created : number = 0;
    target : string='';
    failed : boolean = false;
    created: string;
    description : string ='';
    index : number = 0;
  }