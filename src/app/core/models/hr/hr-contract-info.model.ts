import { BaseModel } from "@app/core/models/base.model";
export class HrContractInfoModel extends BaseModel {
    company_id: number = 0;
    hr_id: string = '';
    trans_seq: number = 0;
    from_ymd: Date;
    to_ymd?: Date;
    contract_no: number = 0;
    unlimitted?: boolean;
    attachment: string = '';
    
}