import { CRMSolutionApiService } from "@app/core/api/crm-solution-api.service";
import { NotificationService } from "../notification.service";
import { Injectable } from "@angular/core";
import { CrmContractModel, CrmContractDetailModel } from "@app/core/models/crm/contract.model";

@Injectable({
    providedIn: 'root'
  })
export class CrmContractService {
    private CrmContractInfo: CrmContractModel;

    constructor(
        private api: CRMSolutionApiService,
        private notificationService: NotificationService
    ) {
        this.CrmContractInfo = new CrmContractModel();
    }

    getModel(): CrmContractModel {
        return this.CrmContractInfo;
    }

    resetModel() {
        return this.CrmContractInfo = new CrmContractModel();
    }

    storeTemporaryModel(CrmContractInfo: CrmContractModel) {
        this.CrmContractInfo = CrmContractInfo;
    }

    public ListContract(company_id) {

        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`contract/list?companyid=${company_id}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public ShortList(companyId,srcOrderType,year:number=0) {

        if (companyId <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`contract/shortlist?companyId=${companyId}&srcOrderType=${srcOrderType}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data)
            })
        })
    }
    public ListFromAMStatistics(companyId,year,month,contractName) {

        if (companyId <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`contract/list-from-am-statistics?companyId=${companyId}&year=${year}&month=${month}&contractName=${contractName}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data)
            })
        })
    }
    public getAllShortList(company_id) {
        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`contract/get-all-short-list?companyid=${company_id}`).subscribe(data => {
                if (data.status < 0 ) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public getDetail(companyId,ContractId, isGetSharingData:boolean=null) {
        if(ContractId<=0){
          return new Promise<any>((resolve, reject) => {
            resolve([]);
          });
        }
          return new Promise<any>((resolve, reject) => {
            this.api.get(`contract/detail?companyId=${companyId}&ContractId=${ContractId}${isGetSharingData==true?'&getSharingData=true':''}`).subscribe(data => {
              if(!data){
                resolve([]);
              }
              resolve(data);
            });
          });
    }

    public InsertContract(model) {
        console.log("model",model)
        return new Promise<any>((resolve, reject) => {
            this.api.post("contract/save", model).subscribe(data => {
                resolve(data);
            });
        });
    }

    public DeleteContract(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("contract/delete", model).subscribe(data => {
                resolve(data);
            });
        });
    }
}

@Injectable({
    providedIn: 'root'
  })
export class CrmContractDetailService {
    private CrmContractDetailInfo: CrmContractDetailModel;

    constructor(
        private api: CRMSolutionApiService,
        private notificationService: NotificationService
    ) {
        this.CrmContractDetailInfo = new CrmContractDetailModel();
    }

    getModel(): CrmContractDetailModel {
        return this.CrmContractDetailInfo;
    }

    resetModel() {
        return this.CrmContractDetailInfo = new CrmContractDetailModel();
    }

    storeTemporaryModel(CrmContractDetailInfo: CrmContractDetailModel) {
        this.CrmContractDetailInfo = CrmContractDetailInfo;
    }

    public ListContractDetail(companyId,contractId) {

        if (companyId <= 0 || contractId <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`contract-detail/list?companyId=${companyId}&contractId=${contractId}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public ShortList(companyId,amtActualYear:number=0,contractId:number=0,issueYmd:number=0) {

        if (companyId <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`contract-detail/shortlist?companyId=${companyId}&amtActualYear=${amtActualYear}&contractId=${contractId}&issueYmd=${issueYmd}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data)
            })
        })
    }
 
    public InsertContractDetail(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("contract-detail/save", model).subscribe(data => {
                resolve(data);
            });
        });
    }

    public DeleteContractDetail(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("contract-detail/delete", model).subscribe(data => {
                resolve(data);
            });
        });
    }
}