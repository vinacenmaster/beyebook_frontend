import { CRMSolutionApiService } from "@app/core/api/crm-solution-api.service";
import { NotificationService } from "../notification.service";
import { Injectable } from "@angular/core";
import { CrmMasSalesTargetHeaderModel, CrmMasSalesTargetDetailModel } from "@app/core/models/crm/sales-target.model";

@Injectable({
    providedIn: 'root'
  })
export class CrmSalesTargetService {
    private CrmSalesTargetInfo: CrmMasSalesTargetHeaderModel;

    constructor(
        private api: CRMSolutionApiService,
        private notificationService: NotificationService
    ) {
        this.CrmSalesTargetInfo = new CrmMasSalesTargetHeaderModel();
    }

    getModel(): CrmMasSalesTargetHeaderModel {
        return this.CrmSalesTargetInfo;
    }

    resetModel() {
        return this.CrmSalesTargetInfo = new CrmMasSalesTargetHeaderModel();
    }

    storeTemporaryModel(CrmSalesTargetInfo: CrmMasSalesTargetHeaderModel) {
        this.CrmSalesTargetInfo = CrmSalesTargetInfo;
    }

    public ListSalesTarget(companyId, year) {
        if (companyId <= 0 || year <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`sales-target/list?companyId=${companyId}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }

    public InsertSalesTarget(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("sales-target/save", model).subscribe(data => {
                resolve(data);
            });
        });
    }
}

@Injectable({
    providedIn: 'root'
  })
export class CrmSalesTargetDetailService {
    private CrmSalesTargetDetailInfo: CrmMasSalesTargetDetailModel;

    constructor(
        private api: CRMSolutionApiService,
        private notificationService: NotificationService
    ) {
        this.CrmSalesTargetDetailInfo = new CrmMasSalesTargetDetailModel();
    }

    getModel(): CrmMasSalesTargetDetailModel {
        return this.CrmSalesTargetDetailInfo;
    }

    resetModel() {
        return this.CrmSalesTargetDetailInfo = new CrmMasSalesTargetDetailModel();
    }

    storeTemporaryModel(CrmSalesTargetDetailInfo: CrmMasSalesTargetDetailModel) {
        this.CrmSalesTargetDetailInfo = CrmSalesTargetDetailInfo;
    }

    public ListSalesTargetDetail(companyId, year) {
        if (companyId <= 0 || year <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`sales-target-detail/list?companyId=${companyId}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
}