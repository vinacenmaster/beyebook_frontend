import { Injectable } from '@angular/core';
import { CRMSolutionApiService } from '@app/core/api/crm-solution-api.service';
import { NotificationService } from '../notification.service';
import { CrmMasServiceCategoryModel, CrmMasServiceItemModel } from '@app/core/models/crm/setting-item.model';
import { GetOrderModel, GetOrderDetailModel } from '@app/core/models/crm/get-order.model';


@Injectable({
  providedIn: 'root'
})
export class SalesOrderService {
  getOrderModel: GetOrderModel

  constructor(private api: CRMSolutionApiService, private notificationService: NotificationService, ) {

  }
  public initModel(company_id): GetOrderModel {
    this.getOrderModel = new GetOrderModel();
    this.getOrderModel.company_id = company_id
    return this.getOrderModel
  }
  public getList(companyId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/crm-order-magt/sales-order/get-list?companyId=${companyId}`).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          resolve([]);
          return;
        }
        resolve(data.data);
      });
    });
  }
  public ShortList(company_id,keyword:string ='',take:number=0) {

    if (company_id <= 0) {
        return new Promise<any>((resolve, reject) => {
            resolve([]);
        });
    }
    return new Promise<any>((resolve, reject) => {
        this.api.get(`/crm-order-magt/sales-order/shortlist?companyid=${company_id}&keyword=${keyword}&take=${take}`).subscribe(data => {
            if (data.error) {
                this.notificationService.showMessage("error", data.error.message);
                resolve([]);
                return;
            }
            resolve(data)
        })
    })
}
  public getDetail(companyId, orderId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/crm-order-magt/sales-order/get-detail?companyId=${companyId}&orderId=${orderId}`).subscribe(data => {
        if (data == null) {
          this.notificationService.showMessage("error", data.error.message);
          resolve([]);
          return;
        }
        resolve(data);
      });
    });
  }
  public addOrUpdate(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/sales-order/save`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

  public delete(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/sales-order/delete`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

}

@Injectable({
  providedIn: 'root'
})
export class SalesOrderDetailService {
  getOrderDetailModel: GetOrderDetailModel

  constructor(private api: CRMSolutionApiService, private notificationService: NotificationService, ) {

  }

  public initModel(company_id): GetOrderDetailModel {
    this.getOrderDetailModel = new GetOrderDetailModel();
    this.getOrderDetailModel.company_id = company_id
    return this.getOrderDetailModel
  }
  public getListByOrderId(companyId, orderId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/crm-order-magt/sales-order-detail/get-list-by-orderid?companyId=${companyId}&orderId=${orderId}`).subscribe(data => {
        if (data == null) {
          this.notificationService.showMessage("error", data.error.message);
          resolve([]);
          return;
        }
        resolve(data.data);
      });
    });
  }

  public addOrUpdate(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/sales-order-detail/save`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

  public delete(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/sales-order-detail/delete`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          // resolve(data);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

}



