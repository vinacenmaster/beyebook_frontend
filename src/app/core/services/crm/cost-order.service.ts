import { Injectable } from '@angular/core';
import { CRMSolutionApiService } from '@app/core/api/crm-solution-api.service';
import { NotificationService } from '../notification.service';
import { CrmMasServiceCategoryModel, CrmMasServiceItemModel } from '@app/core/models/crm/setting-item.model';
import { GetOrderModel, GetOrderDetailModel } from '@app/core/models/crm/get-order.model';
import { CostOrderModel, CostOrderDetailModel } from '@app/core/models/crm/cost-order.model';


@Injectable({
  providedIn: 'root'
})
export class CostOrderService {
  costOrderModel: CostOrderModel

  constructor(private api: CRMSolutionApiService, private notificationService: NotificationService, ) {

  }
  public initModel(company_id): CostOrderModel {
    this.costOrderModel = new CostOrderModel();
    this.costOrderModel.company_id = company_id
    return this.costOrderModel
  }
  public getList(companyId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/crm-order-magt/cost-order/get-list?companyId=${companyId}`).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          resolve([]);
          return;
        }
        resolve(data.data);
      });
    });
  }
  public ShortList(company_id,keyword:string ='',take:number=0) {

    if (company_id <= 0) {
        return new Promise<any>((resolve, reject) => {
            resolve([]);
        });
    }
    return new Promise<any>((resolve, reject) => {
        this.api.get(`/crm-order-magt/cost-order/shortlist?companyid=${company_id}&keyword=${keyword}&take=${take}`).subscribe(data => {
            if (data.error) {
                this.notificationService.showMessage("error", data.error.message);
                resolve([]);
                return;
            }
            resolve(data)
        })
    })
}
  public getDetail(companyId, costOrderId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/crm-order-magt/cost-order/get-detail?companyId=${companyId}&costOrderId=${costOrderId}`).subscribe(data => {
        if (data == null) {
          this.notificationService.showMessage("error", data.error.message);
          resolve([]);
          return;
        }
        resolve(data);
      });
    });
  }
  public addOrUpdate(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/cost-order/save`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

  public delete(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/cost-order/delete`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

}

@Injectable({
  providedIn: 'root'
})
export class CostOrderDetailService {
  costOrderDetailModel: CostOrderDetailModel

  constructor(private api: CRMSolutionApiService, private notificationService: NotificationService, ) {

  }

  public initModel(company_id): CostOrderDetailModel {
    this.costOrderDetailModel = new CostOrderDetailModel();
    this.costOrderDetailModel.company_id = company_id
    return this.costOrderDetailModel
  }
  public getListByOrderId(companyId, costOrderId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/crm-order-magt/cost-order-detail/get-list?companyId=${companyId}&costOrderId=${costOrderId}`).subscribe(data => {
        if (data == null) {
          this.notificationService.showMessage("error", data.error.message);
          resolve([]);
          return;
        }
        resolve(data.data);
      });
    });
  }

  public addOrUpdate(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/cost-order-detail/save`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

  public delete(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post(`/crm-order-magt/cost-order-detail/delete`, model).subscribe(data => {
        if (data.error) {
          this.notificationService.showMessage("error", data.error.message);
          // resolve(data);
          reject();
          return;
        }
        resolve(data);
      });
    });
  }

}



