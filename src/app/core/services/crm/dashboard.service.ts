import { CRMSolutionApiService } from "@app/core/api/crm-solution-api.service";
import { NotificationService } from "../notification.service";
import { Injectable } from "@angular/core";
import { CrmRecentyActivityModel } from "@app/core/models/crm/dashboard.model";
import { resolve } from "dns";
import { reject } from "q";

@Injectable({
    providedIn: 'root'
})
export class CrmDashboardService {
    private CrmDashboardInfo: CrmRecentyActivityModel;

    constructor(
        private api: CRMSolutionApiService,
        private notificationService: NotificationService
    ) {
        this.CrmDashboardInfo = new CrmRecentyActivityModel();
    }

    getModel(): CrmRecentyActivityModel {
        return this.CrmDashboardInfo;
    }

    resetModel() {
        return this.CrmDashboardInfo = new CrmRecentyActivityModel();
    }

    storeTemporaryModel(CrmDashboardInfo: CrmRecentyActivityModel) {
        this.CrmDashboardInfo = CrmDashboardInfo;
    }

    public ListRecentlyActivity(company_id) {

        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`dashboard/list-recently-activity?companyid=${company_id}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }

    public ListActivityBoard(companyId, skip, take, search, creator, activityStatus, year, month) {
        if (companyId <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`activity-board/list?companyId=${companyId}&skip=${skip}&take=${take}&search=${search}
            &creator=${creator}&activityStatus=${activityStatus}&year=${year}&month=${month}`).subscribe(data => {
                    if (data.error) {
                        this.notificationService.showMessage("error", data.error.message);
                        resolve([]);
                        return;
                    }
                    resolve(data.data)
                })
        })
    }
    public ListSales(company_id,year) {

        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`dashboard/sale-by-quarter?companyid=${company_id}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public ListSalestarget(company_id,year) {

        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`dashboard/saletarget-by-quarter?companyid=${company_id}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public getFixedFinancial(company_id,year){
        if (company_id <= 0) {
            return new Promise<any>((resolve,reject) =>{
                resolve([]);
            })
        }
        return new Promise<any>((resolve,reject) =>{
            this.api.get(`dashboard/fixed-financial?companyid=${company_id}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public getSalesStatus(company_id,year,orderType){
        if (company_id <= 0) {
            return new Promise<any>((resolve,reject) =>{
                resolve([]);
            })
        }
        return new Promise<any>((resolve,reject) =>{
            this.api.get(`dashboard/sales-status?companyId=${company_id}&year=${year}&orderType=${orderType}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public getSalesTrend(company_id,year){
        if (company_id <= 0) {
            return new Promise<any>((resolve,reject) =>{
                resolve([]);
            })
        }
        return new Promise<any>((resolve,reject) =>{
            this.api.get(`dashboard/sales-trend?companyId=${company_id}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
    public getOrderStatistic(company_id,year){
        if (company_id <= 0) {
            return new Promise<any>((resolve,reject) =>{
                resolve([]);
            })
        }
        return new Promise<any>((resolve,reject) =>{
            this.api.get(`dashboard/order-statistic?companyId=${company_id}&year=${year}`).subscribe(data => {
                if (data.error) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
}

@Injectable({
    providedIn: 'root'
  })
export class CrmStatisticBoadService {
    constructor(
        private api: CRMSolutionApiService,
        private notificationService: NotificationService
    ) { }
    public getStatisticBoad(company_id,year) {
        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`statistics-board/list?companyId=${company_id}&year=${year}`).subscribe(data => {
                if (!data.success) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data[0])
            })
        })
    }
    public getAMStatistics(company_id,year) {
        if (company_id <= 0) {
            return new Promise<any>((resolve, reject) => {
                resolve([]);
            });
        }
        return new Promise<any>((resolve, reject) => {
            this.api.get(`statistics-board/am-statistics?companyId=${company_id}&year=${year}`).subscribe(data => {
                if (!data.success) {
                    this.notificationService.showMessage("error", data.error.message);
                    resolve([]);
                    return;
                }
                resolve(data.data)
            })
        })
    }
}
