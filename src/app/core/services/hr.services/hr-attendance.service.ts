import { Injectable } from "@angular/core";
import { CRMSolutionApiService } from "@app/core/api/crm-solution-api.service";

@Injectable()
export class AttendanceService {
    constructor(private api: CRMSolutionApiService) {
    }

    //Daily Attendance
    public getAttendanceEmployees(orgid, name, year, month, day, pagesize, page) {
        return new Promise<any>((resolve, reject) => {
            this.api.get("attendance/getpaging?orgid=" + orgid + "&name=" + name + "&year=" + year + "&month=" + month + "&day=" + day + "&pagesize=" + pagesize + "&page=" + page)
            .subscribe(data => {
                resolve(data);
            });
        });
    }

    public createEmployeeAttendance(orgid, name, year, month, day, pagesize) {
        return new Promise<any>((resolve, reject) => {
            this.api.get("attendance/create?orgid=" + orgid + "&name=" + name + "&year=" + year + "&month=" + month + "&day=" + day + "&pagesize=" + pagesize)
            .subscribe(data => {
                resolve(data);
            });
        });
    }

    public saveEmployeeAttendance(items) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("attendance/save", items).subscribe(data => {
                resolve(data);
            });
        });
    }

    public getAttendanceLog() {
        return new Promise<any>((resolve, reject) => {
            this.api.get("attendance/getattendlog")
            .subscribe(data => {
                resolve(data);
            });
        });
    }

    public saveSetting(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("attendance/setting/save", model).subscribe(data => {
                resolve(data);
            });
        });
    }

    public getSetting() {
        return new Promise<any>((resolve, reject) => {
            this.api.get("attendance/setting/get").subscribe(data => {
                resolve(data);
            });
        });
    }

    public deleteSetting(resetid) {
        return new Promise<any>((resolve, reject) => {
            this.api.get("attendance/setting/delete?resetid=" + resetid).subscribe(data => {
                resolve(data);
            });
        });
    }

    //Attendance By Person
    public getEmployees() {
        return new Promise<any>((resolve, reject) => {
            this.api.get("attendance/getemployee").subscribe(data => {
                resolve(data);
            });
        });
    }

    public getAttendanceByPerson(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("attendance/person", model).subscribe(data => {
                resolve(data);
            });
        });
    }
}