import { Injectable } from "@angular/core";
import { CRMSolutionApiService } from "@app/core/api/crm-solution-api.service";
import { NotificationService } from "../notification.service";

@Injectable({
    providedIn: 'root'
})
export class HrShiftworkService {
    constructor(private api: CRMSolutionApiService, 
                private notificationService: NotificationService) {
    }

    public getShiftWorkTable(companyId,departId, month, year) {
       
          return new Promise<any>((resolve, reject) => {
            this.api.get("hr/shiftwork/table?companyId="+companyId+'&departId='+departId+'&month='+month+'&year='+year).subscribe(data => {
              resolve(data);
            });
          });
      }
      public saveShiftworkInfo(model:any) {
        return new Promise<any>((resolve, reject) => {
          this.api.post("hr/shiftwork/add", model).subscribe(data => {
            resolve(data);
          });
        });
      }
      public updateShiftworkTable(model:any) {
        return new Promise<any>((resolve, reject) => {
          this.api.post("hr/shiftwork/table/update", model).subscribe(data => {
            resolve(data);
          });
        });
      }
}