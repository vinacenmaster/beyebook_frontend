import { Injectable } from "@angular/core";
import { CRMSolutionApiService } from "@app/core/api/crm-solution-api.service";

@Injectable()
export class PayrollMasterService {
    constructor(private api: CRMSolutionApiService) {
    }

    public GetPayrollMonths() {
        return new Promise<any>((resolve, reject) => {
            this.api.get("payroll/master/months").subscribe(data => {
                resolve(data);
                return;
            });
        });
    }

    public getPayrollMaster(orgid, name, hrid, year, month, pagesize, page) {
        return new Promise<any>((resolve, reject) => {
            this.api.get("payroll/master/list?orgid=" + orgid + "&name=" + name + "&hrid=" + hrid + "&year=" + year + "&month=" + month + "&pagesize=" + pagesize + "&page=" + page)
            .subscribe(data => {
                resolve(data);
                return;
            });
        });
    }

    public createPayrollMaster(year, month, pagesize) {
        return new Promise<any>((resolve, reject) => {
            this.api.get("payroll/master/create?year=" + year + "&month=" + month + "&pagesize=" + pagesize).subscribe(data => {
                resolve(data);
            });
        });
    }

    public savePayrollMaster(model) {
        return new Promise<any>((resolve, reject) => {
            this.api.post("payroll/master/save", model).subscribe(data => {
                resolve(data);
            });
        });
    }

    public copyPayrollMaster(mfrom, yfrom, mto, yto, pagesize) {
        return new Promise<any>((resolve, reject) => {
            this.api.get("payroll/master/copy?mfrom=" + mfrom + "&yfrom=" + yfrom + "&mto=" + mto + "&yto=" + yto + "&pagesize=" + pagesize).subscribe(data => {
                resolve(data);
            });
        });
    }
}