import { Injectable } from '@angular/core';
import { CRMSolutionApiService } from '@app/core/api/crm-solution-api.service';
import { HrOvertimeTableModel, HrMasOvertimeModels, HrMasOvertimeTableModel, HrOvertimeTableDetailModel,  } from '@app/core/models/hr/hr-overtime-table.model';
import { NotificationService } from '../notification.service';
import { FactoryModel } from '@app/core/models/factory.model';

@Injectable({
  providedIn: 'root'
})
export class HrOvertimeTableService {
  private otModelOld: HrMasOvertimeModels;
  private overtimeTableNewModel: HrMasOvertimeTableModel;
  private overtimeDetailTableModel: HrOvertimeTableDetailModel;

  constructor(
    private api: CRMSolutionApiService,
    private notificationService: NotificationService
  ) {
    this.otModelOld = new HrMasOvertimeModels();
  }
  getModel(): HrMasOvertimeModels {
    return this.otModelOld;
  }

  getOvertimeTableModel(): HrMasOvertimeTableModel {
    return this.overtimeTableNewModel;
  }
  storeTemporaryModel(systemMenuInfo: HrMasOvertimeModels) {
    this.otModelOld = systemMenuInfo;
  }

  resetModel() {
    this.otModelOld = new HrMasOvertimeModels();
  }

  resetOverTimeTableModel() {
    this.overtimeTableNewModel = new HrMasOvertimeTableModel();
  }

  public insertHrOvertimeTable(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/save", model).subscribe(data => {
        resolve(data);
      });
    });
  }

  public DeleteHrOvertimeTable(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/delete", model).subscribe(data => {
        resolve(data);
      });
    });
  }

  public listOvertime(company_id, factory_id, ot_table_id) {
    if (company_id <= 0) {
      return new Promise<any>((resolve, reject) => {
        resolve([]);
      });
    }
    return new Promise<any>((resolve, reject) => {
      this.api.get("overtime/list?companyId=" + company_id + '&factoryId=' + factory_id + '&otTableId=' + ot_table_id).subscribe(data => {
        if (!data.total && !data.data) {
          resolve([]);
        }
        resolve(data.data);
      });
    });
  }

  public listFactory(companyId) {
    if (companyId <= 0) {
      return new Promise<any>((resolve, reject) => {
        resolve([]);
      });
    }
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/factory/list?companyId=${companyId}`).subscribe(data => {
        if (!data.total && !data.data) {
          resolve([]);
        }
        resolve(data.data);
      });
    });
  }

  public copyOvertime(model) {
    console.log("model", model)
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/copy", model).subscribe(data => {
        resolve(data);
      });
    });
  }

  public getMaxTranseq(companyId, factoryId, otTableId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`overtime/getmaxtranseq?companyId=${companyId}&factoryId=${factoryId}&otTableId=${otTableId}`).subscribe(data => {
        resolve(data);
      });
    });
  }

   // Over time detail 

   public GetMaxTranseqNew(companyId, otTableId) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`overtime/gettranseqnew?companyId=${companyId}&otTableId=${otTableId}`).subscribe(data => {
        resolve(data);
      });
    });
  }

  public ListOvertimeNew(company_id,ot_table_id,) {
    if (company_id <= 0) {
      return new Promise<any>((resolve, reject) => {
        resolve([]);
      });
    }
    return new Promise<any>((resolve, reject) => {
      this.api.get("overtime/list-overtime?companyId="+company_id+'&otTableId='+ot_table_id).subscribe(data => {
        if (!data.total && !data.data) {
          resolve([]);
        }
        resolve(data.data);
      });
    });
  }

  public InsertOvertimeTableDetail(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/save-overtime", model).subscribe(data => {
        resolve(data);
      });
    });
  }

  

  public DeleteOvertimeTableDetail(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/delete-overtime", model).subscribe(data => {
        resolve(data);
      });
    });
  }

   // Over time table
   public InsertOvertimeTable(model) {
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/save-overtime-table", model).subscribe(data => {
        resolve(data);
      });
    });
  }

  public GetOvertimeTable(company_id, ot_table_id) {
    if(ot_table_id<=0){
      return new Promise<any>((resolve, reject) => {
        resolve({});
      });
    }
    return new Promise<any>((resolve, reject) => {
      this.api.get(`overtime/detail-overtime-table?companyid=${company_id}&otTableId=${ot_table_id}`).subscribe(data => {
        if(!data){
          resolve({});
        }
        resolve(data);
      });
    });
  }

  //Copy 
  public copyOvertimeTable(oldOtID, newOtModel) {
    return new Promise<any>((resolve, reject) => {
      this.api.post("overtime/copy-overtime?oldOtID="+oldOtID, newOtModel).subscribe(data => {
        resolve(data);
      });
    });
  }

// Get number max sum hour
  public getMaxSumhour(company_id, ot_table_id,workday) {
    return new Promise<any>((resolve, reject) => {
      this.api.get(`overtime/getmax-sum-hour?companyid=${company_id}&otTableId=${ot_table_id}&workday=${workday}`).subscribe(data => {
        resolve(data);
      });
    });
  }

  public listOverTimeTable(companyId) {
    if (companyId <= 0) {
      return new Promise<any>((resolve, reject) => {
        resolve([]);
      });
    }
    return new Promise<any>((resolve, reject) => {
      this.api.get(`/overtime/list-overtime-table?companyId=${companyId}`).subscribe(data => {
        if (!data.total && !data.data) {
          resolve([]);
        }
        resolve(data.data);
      });
    });
  }

  getModelOvertimeDetailModel(): HrOvertimeTableDetailModel {
    return this.overtimeDetailTableModel;
  }
}