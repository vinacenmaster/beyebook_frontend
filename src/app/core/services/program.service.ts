import { EventEmitter, Injectable, Output } from "@angular/core";
import { ProgramModel } from "@app/core/models/program.model";
import { config } from "@app/core/smartadmin.config";
import { NotificationService } from "@app/core/services/notification.service";
import { Router } from "@angular/router";

declare var $: any;

@Injectable()
export class ProgramService {
    openedProgramsKey: string = "opened.programs";
    public openedPrograms: Array<ProgramModel> = [];

    getOpenedPrograms = new EventEmitter<Array<ProgramModel>>();

    programsClicked = new EventEmitter<any>();

    constructor(private notificationService: NotificationService,
        private router: Router) {

    }

    ActiveMenu(id) {
        // console.log("ActiveMenu", this.openedPrograms)
        this.openedPrograms.forEach(element => {
            element.active = false;
            if (element.id == id) {
                element.active = true;
            }
        });

        this.setOpenedProgramsLocalStorage();

        this.programsClicked.emit(id)

        if (window.parent.length) {
            $(window.parent.document).find("div[id='page']").find("iframe").hide();
            $(window.parent.document).find("div[id='page']").find("iframe[id=" + id + "]").show();
        }
        else {
            $("#page").find("iframe").hide();
            $("#page").find("iframe[id=" + id + "]").show();
        }
        $('.center-loading').hide();
    }
    ActiveMenuFromIframe(id,openedProg:Array<ProgramModel>) {
        this.openedPrograms = openedProg;
        this.openedPrograms.forEach(element => {
            element.active = false;
            if (element.id == id) {
                element.active = true;
            }
        });

        this.setOpenedProgramsLocalStorage();
        this.programsClicked.emit(id)
        if (window.parent.length) {
            $(window.parent.document).find("div[id='page']").find("iframe").hide();
            $(window.parent.document).find("div[id='page']").find("iframe[id=" + id + "]").show();
        }
        else {
            $("#page").find("iframe").hide();
            $("#page").find("iframe[id=" + id + "]").show();
        }
        $('.center-loading').hide();
    }

    OpenMenu(menuInfo: ProgramModel, child: boolean = false) {
        //console.log('OpenMenu',menuInfo) 
        // var ie=$("#page").find("iframe[id=" + menuInfo.id + "]");
        // console.log("OpenMenu")
        var iframeList;
        if (child) {
            iframeList = $(window.parent.document).find("div[id='page']").find("iframe[id=" + menuInfo.id + "]");
            if (iframeList.length) {
                this.ActiveMenu(menuInfo.id);
            }
            else {
                $(window.parent.document).find("div[id='page']").find("iframe").hide();
                $('<iframe>',
                    {
                        src: "/#" + menuInfo.url,
                        id: menuInfo.id,
                        frameborder: 0,
                        scrolling: 'auto',
                        height: '100vh',
                        width: '100%'
                    })
                    .appendTo(window.parent.document.getElementById("page"));
                setTimeout(() => {
                    $('.center-loading').hide();
                }, 1000);
                this.programsClicked.emit(menuInfo.id)
            }
        }
        else {
            iframeList = $("#page").find("iframe[id=" + menuInfo.id + "]")
            if (iframeList.length) {
                this.ActiveMenu(menuInfo.id);
            }
            else {
                $("#page").find("iframe").hide();
                $('<iframe>',
                    {
                        src: "/#" + menuInfo.url,
                        id: menuInfo.id,
                        frameborder: 0,
                        scrolling: 'auto',
                        height: '100vh',
                        width: '100%'
                    })
                    .appendTo('#page');
                setTimeout(() => {
                    $('.center-loading').hide();
                }, 1000);
                this.programsClicked.emit(menuInfo.id)
            }
        }
    }

    getOpenedProgramsLocalStorage(child: boolean = false) {
        let openedProgramsData = localStorage.getItem(this.openedProgramsKey);
        // var iframeList:number = $("#page").find("iframe").length;
        // console.log("openedProgramsData",openedProgramsData)
        var iframeList: number;

        if (openedProgramsData) {
            this.openedPrograms = JSON.parse(openedProgramsData);
            if (child) {
                iframeList = $(window.parent.document).find("div[id='page']").find("iframe").length;
                if (!iframeList) {
                    // console.log('reopen ' + this.openedPrograms.length)
                    var activeId = 0;
                    $('.center-loading').show();
                    this.openedPrograms.forEach(element => {

                        if (element.active) {
                            activeId = element.id;
                        }
                        $('<iframe>',
                            {
                                src: "/#" + element.url,
                                id: element.id,
                                frameborder: 0,
                                scrolling: 'auto',
                                height: '100vh',
                                width: '100%',
                            })
                            .appendTo(window.parent.document.getElementById("page"));
                    });
                    this.ActiveMenu(activeId)
                    setTimeout(() => {
                        $('.center-loading').hide();
                        //
                    }, 700 * this.openedPrograms.length);
                }
            }
            else {
                iframeList = $("#page").find("iframe").length;
                if (!iframeList) {
                    // console.log('reopen ' + this.openedPrograms.length)
                    var activeId = 0;
                    $('.center-loading').show();
                    this.openedPrograms.forEach(element => {

                        if (element.active) {
                            activeId = element.id;
                        }
                        $('<iframe>',
                            {
                                src: "/#" + element.url,
                                id: element.id,
                                frameborder: 0,
                                scrolling: 'auto',
                                height: '100vh',
                                width: '100%',
                            })
                            .appendTo('#page');
                    });
                    this.ActiveMenu(activeId)
                    setTimeout(() => {
                        $('.center-loading').hide();
                        //
                    }, 700 * this.openedPrograms.length);
                }
            }
        }

    }

    setOpenedProgramsLocalStorage() {
        // console.log("setOpenedProgramsLocalStorage", this.openedPrograms)
        // store array to local storage.
        localStorage.setItem(this.openedProgramsKey, JSON.stringify(this.openedPrograms));
    }
    resetActiveByUrl(url) {
        // console.log("resetActiveByUrl")
        this.openedPrograms.forEach(element => {
            element.active = false;
            if (element.url === url) {
                element.active = true;
            }
        });
    }
    resetActiveById(id) {
        // console.log("resetActiveById")
        this.openedPrograms.forEach(element => {
            element.active = false;
            if (element.id === id) {
                element.active = true;
            }
        });
    }
    addOpenedPrograms(program: ProgramModel, child: boolean = false): boolean {
        // console.log(this.openedPrograms)
    
        this.getOpenedProgramsLocalStorage(child);

        if (!this.openedPrograms.filter(x => x.id == program.id).length) {
            this.openedPrograms.push(program);
        }else{
            if (!this.openedPrograms.filter(x => x.url == program.url).length) {
                this.getOpenedProgramsLocalStorage();
                let p = this.openedPrograms.filter(p => p.id == program.id);
                if (p && p.length > 0) {
                    this.closeOpenedPrograms(p[0].id)
                }
                this.openedPrograms.push(program);
            }
        }
        //program.active = true;
        //this.addAnimation();


        this.openedPrograms.forEach(element => {
            element.active = false;
            if (element.id == program.id) {
                element.active = true;
            }
        });

        this.OpenMenu(program, child)

        this.setOpenedProgramsLocalStorage();
        this.getOpenedPrograms.emit(this.openedPrograms);
        if (child) window.parent.postMessage(this.openedPrograms, '*');
        // console.log(this.openedPrograms);
        return true;
    }

    clearActivePrograms() {
        // console.log("clearActivePrograms")
        this.getOpenedProgramsLocalStorage();

        this.openedPrograms.forEach(element => {
            element.active = false;
        });

        this.setOpenedProgramsLocalStorage();
        this.addAnimation();
        this.getOpenedPrograms.emit(this.openedPrograms);
    }

    refreshOpenedPrograms() {
        // console.log("refreshOpenedPrograms")
        this.getOpenedProgramsLocalStorage()
        this.getOpenedPrograms.emit(this.openedPrograms);
    }

    closeCurrentProgram() {
        // console.log("closeCurrentProgram")
        this.getOpenedProgramsLocalStorage();
        let p = this.openedPrograms.filter(p => p.active == true);
        if (p && p.length > 0) {
            this.closeOpenedPrograms(p[0].id)
        }
    }

    closeOpenedPrograms(id: number) {
        // console.log("closeOpenedPrograms")
        this.openedPrograms = this.openedPrograms.filter(p => p.id != id);
        //console.log('closeOpenedPrograms',this.openedPrograms)
        //this.addAnimation();

        this.setOpenedProgramsLocalStorage();
        this.getOpenedPrograms.emit(this.openedPrograms);

        if (window.parent.length) {
            //let t=$(window.parent.document).find("#tabs>li[id=" + id + "]>a>i")
            $(window.parent.document).find("#tabs>li[id=" + id + "]>a>i").trigger("click")
            $(window.parent.document).find("iframe[id=" + id + "]").remove();
        }
        else {
            $("#page").find("iframe[id=" + id + "]").remove();
        }
        if (this.openedPrograms.length) {
            //this.openedPrograms[this.openedPrograms.length-1].active=true;
            this.ActiveMenu(this.openedPrograms[this.openedPrograms.length - 1].id)
        }



        // if (this.openedPrograms.length > 0) {
        //     this.router.navigate([this.openedPrograms[this.openedPrograms.length-1].url]);
        //   } else {
        //     this.router.navigate(['/home']);
        //   }
    }

    closeAllOpenedPrograms() {
        // console.log("closeAllOpenedPrograms")
        this.openedPrograms.forEach(element => {
            $("#page").find("iframe[id=" + element.id + "]").remove();
        });

        this.openedPrograms = [];
        this.addAnimation();
        this.setOpenedProgramsLocalStorage();
        this.getOpenedPrograms.emit(this.openedPrograms);
        //this.router.navigate(['/home']);
    }

    showPopup() {
        // console.log("showPopup")
        this.notificationService.smartMessageBox({
            title: "Notification",
            content: `Maximum ${config.maxOpenedPrograms} programs can be opened!`,
            buttons: '[OK]'
        });
    }

    addAnimation() {
        // $('.dropdown-toggle>b').addClass('animated bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
        // function() {
        //     $(this).removeClass('animated bounce');
        // });
    }

    getCurrentProgram(): ProgramModel {
        // console.log("getCurrentProgram")
        let p = this.openedPrograms.filter(p => p.active == true);
        if (p && p.length > 0) {
            return p[0];
        }
        return null;
    }
}